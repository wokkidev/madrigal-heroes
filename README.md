# Madrigal Heroes APP and API
A react app frontend and express backend api by Wokki. If you enjoy this, please consider donating. I spent way longer on this project than I should have, so donations are greatly appreciated.

# Requirements
- NodeJS v12+
- MSSQL database
- FlyFF database (API does not include migrations to create tables for other than web)
- nginx for reverse proxy
- Windows Build Tools `npm install --global windows-build-tools`
- Python 3 for Let's Encrypt SSL certificate (Not required if you already have SSL)

# Features
- Authentication
- Registration
- Ranking
- Change password
- Change email
- Voting with sockets (gtop100 and xtremetop100 implemented)
- Donations with sockets (PayPal IPN)
- Account cash and vote points editing via Admin Panel
- News articles
- Site settings
- Donation and Vote Shop with instant delivery using billing socket (Requires [source edits](https://www.elitepvpers.com/forum/flyff-pserver-guides-releases/3446616-web-shop.html))
- Logging of vote, donation, purchases and admin actions
- Online users in game
- World status monitor
- Active users on site count and possiblity to send messages to specific clients

# Planned features
- Forgot password system

These both have to be ran in order for the API to function properly. `create-macban.sql` is for mac ban functionality. Even if your server does not support this, this is still required.
The `ITEM_DBF.sql` is required for our web shop. This database is filled with all item data once server launches, assuming you have implemented the [source edits for item mall](ITEM_MALL_SOURCE_EDITS.md).

# Setup
1. Clone the repository `git clone git@gitlab.com:wokkidev/madrigal-heroes.git`
2. Create `WEB_DBF` database
3. Create a file called `.env` inside `/madrigal-heroes/api` and paste and edit the following
```
SALT=YOUR_CLIENT_SALT
DB_USER=
DB_PASS=
DB_HOST=
SECRET=JWT_SECRET
PORT=3300
PAYPAL_MODE=live
NODE_ENV=production
VOTE_POINTS_PER_VOTE=5
WORLD_IP=WORLD_SERVER_IP
WORLD_PORT=WORLD_SERVER_PORT
PING_INTERVAL=300000
UPDATES_WEBHOOK=DISCORD_WEBHOOK
ARTICLE_PATH=http://YOUR_IP_OR_DOMAIN/article/
```
4. Create a file called `.env.production` inside `/madrigal-heroes/app` and paste and edit the following
```
REACT_APP_API_ENDPOINT=/api/
REACT_APP_CAPTCHA_KEY=YOUR_RECAPTCHA_V2_KEY
REACT_APP_SOCKET=https://YOUR_DOMAIN.COM
REACT_APP_TIMEZONE=Europe/Madrid
REACT_APP_EXP_RATE=60
REACT_APP_GS_TIME=12 PM
```
5. Inside `/madrigal-heroes/api` folder, run `npm run sequelize:web:migrate`
6. Inside `/madrigal-heroes/api` folder, run `npm run sequelize:web:seed:all`
7. Inside `/madrigal-heroes/api` folder, finally run `npm run sequelize:account:migrate`
8. Run the `create-macban.sql` script to create macban table. Even if your source wouldn't support this, we still need this for the API.
9. Run the `ITEM_DBF.sql` script to create ITEM_DBF and the items table. This is required for Item Mall to function properly.
10. Run the API by using `npm start`
11. Inside `/madrigal-heroes/app` folder, build our application by running `npm run build`
12. Serve our application by running `npm run serve`
13. API should now be running on port 3300 and our front-end on port 5000

The Item Mall assumes that you have source changes in place for sending items through the adbill socket. If you do not, [please implement them](ITEM_MALL_SOURCE_EDITS.md) or optionally disable the Item Mall and use your own.

# Creating nginx reverse proxy
Our API and APP should now be running, but we don't want to tell our users to access them from domain.com:5000. Instead, we want to reverse proxy our applications with nginx to port 80.

1. Setup nginx
2. Open and edit nginx.conf file and change your locations to the following
```
server {
  listen 80;
  listen 443 ssl;
  server_name localhost;

  ssl_certificate      C:/ssl/fullchain.pem;
  ssl_certificate_key  C:/ssl/privkey.pem;

  location / {
    proxy_pass "http://127.0.0.1:5000";
  }

  location /api {
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $http_host;
    rewrite ^/api(.*) $1 break;
    proxy_pass "http://127.0.0.1:3300";
  }

  location /socket.io/ {
    proxy_pass "http://127.0.0.1:3300";
    proxy_redirect off;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection 'upgrade';
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
  }

  location /sockjs-node {
    proxy_set_header X-Real-IP  $remote_addr;
    proxy_set_header X-Forwarded-For $remote_addr;
    proxy_set_header Host $host;

    proxy_pass "http://127.0.0.1:3300"; 

    proxy_redirect off;

    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
  }

  location /NeuroSpace {
    autoindex on;
    rewrite ^/NeuroSpace(.*) $1 break;
    root   html/NeuroSpace;
  }
}
```
3. Start nginx, or if you already had nginx running, reload `nginx -t reload`

The line `location /NeuroSpace` is used for your patcher files. If you don't need this, then don't add it.

# Generating Let's Encrypt certificate
If you already have an SSL (Not self signed as that will not work) you can skip generating the SSL with Let's Encrypt and use your existing SSL instead.

In order for the API to send POST requests to PayPal, we need to setup SSL. For this we'll be using Let's Encrypt since they offer free SSL certificates.
- Install [Python 3](https://www.python.org/)
- Open command line and type `pip install certbot`
- Now we can generate our SSL with `certbot certonly --standalone -d example.com -d www.example.com`

Now place these certificates inside C:\ssl folder or alternatively just change the ssl certificate lines in nginx.conf that we placed previously
```
  ssl_certificate      C:/ssl/fullchain.pem;
  ssl_certificate_key  C:/ssl/privkey.pem;
```

After this we need to restart nginx, `nginx -s reload`.

Everything should now be good to go!

# If you enjoy this, please consider buying me a coffee
<a href="https://www.buymeacoffee.com/Wokki" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/default-orange.png" alt="Buy Me A Coffee" style="height: 51px !important;width: 217px !important;" ></a>