import React from 'react';
import { NavLink } from "react-router-dom";

import AuthService from '../services/auth.service';

export default class Navigation extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentUser: {}
    };
  }

  async componentDidMount() {
    let currentUser = await AuthService.getCurrentUser();
    this.setState({
      currentUser: currentUser
    });
  }

  render() {
    const { currentUser } = this.state;
    
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-blue">
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto ml-auto">
            <li className="nav-item">
              <NavLink exact to="/" className="nav-link">Home</NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="/download" className="nav-link">Download</NavLink>
            </li>
            {!currentUser && 
              <li className="nav-item">
                <NavLink to="/register" className="nav-link">Register</NavLink>
              </li>
            }
            <li className="nav-item">
              <NavLink to="/ranking" className="nav-link">Ranking</NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="/donate" className="nav-link">Donate</NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="/shop" className="nav-link">Shop</NavLink>
            </li>
          </ul>
        </div>
      </nav>
    )
  }
}