import React from 'react';
import moment from 'moment-timezone';

import SocketContext from '../socketContext';

class NavigationFooter extends React.Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      onlineCount: 0,
      peakCount: 0,
      worldStatus: true,
      serverTime: moment().tz(process.env.REACT_APP_TIMEZONE).format("h.mm a")
    }
  }

  componentDidMount() {
    this._isMounted = true;

    setInterval(() => {
      this.setState({
        serverTime: moment().tz(process.env.REACT_APP_TIMEZONE).format("h.mm a")
      })
    }, 10000);

    /*
    axios.get(`${process.env.REACT_APP_API_ENDPOINT}online`).then(
      response => {
        this.setState({
          onlineCount: response.data.data
        });
      },
      error => {
        console.error(error);
      }
    );

    axios.get(`${process.env.REACT_APP_API_ENDPOINT}peak`).then(
      response => {
        this.setState({
          peakCount: response.data.data
        });
      },
      error => {
        console.error(error);
      }
    );
    */

    this.props.socket.on('worldStatus', status => {
      if (status) this.setState({ worldStatus: true });
      else this.setState({ worldStatus: false });
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    const { serverTime, worldStatus } = this.state;

    return (
      <div className="container-fluid d-flex justify-content-center navbar-footer">
        <ul className="list-group list-group-horizontal">
          <li className="list-group-item mh-list-group-item d-flex justify-content-between align-items-center">
            { worldStatus ?
              <span>Server status <span className="badge badge-madrigal py-2 px-2 ml-3 badge-madrigal-online"><i className="fas fa-arrow-up"></i></span></span> :
              <span>Server status <span className="badge badge-madrigal py-2 px-2 ml-3 badge-madrigal-offline"><i className="fas fa-arrow-down"></i></span></span>
            }
          </li>
          <li className="list-group-item mh-list-group-item d-flex justify-content-between align-items-center">
            <span>Exp <span className="badge badge-madrigal py-2 px-2 ml-3">{process.env.REACT_APP_EXP_RATE}x</span></span>
          </li>
          <li className="list-group-item mh-list-group-item d-flex justify-content-between align-items-center">
            <span>Guild Siege <span className="badge badge-madrigal py-2 px-2 ml-3">{process.env.REACT_APP_GS_TIME}</span></span>
          </li>
          {/*
          <li className="list-group-item mh-list-group-item d-flex justify-content-between align-items-center">
            Players online <span className="badge badge-madrigal py-2 px-2 ml-3">{ onlineCount }</span>
          </li>
          <li className="list-group-item mh-list-group-item d-flex justify-content-between align-items-center">
            Peak <span className="badge badge-madrigal py-2 px-2 ml-3">{ peakCount }</span>
          </li>
          */}
          <li className="list-group-item mh-list-group-item d-flex justify-content-between align-items-center">
            Server time <span className="badge badge-madrigal py-2 px-2 ml-3">{ serverTime }</span>
          </li>
        </ul>
      </div>
    )
  }
}

const NavigationFooterWithSocket = props => (
  <SocketContext.Consumer>
    {socket => <NavigationFooter {...props} socket={socket} />}
  </SocketContext.Consumer>
);

export default NavigationFooterWithSocket;