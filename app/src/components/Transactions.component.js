import React, { Component } from "react";
import moment from 'moment';
import { withRouter } from 'react-router-dom';

import AuthService from "../services/auth.service";
import UserService from "../services/user.service";

import TablePagination from "rsuite/lib/Table/TablePagination";

import { Table } from 'rsuite';

const { Column, HeaderCell, Cell } = Table;

class Transactions extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      currentUser: {},
      displayError: false,
      errorMessage: '',
      donationLogs: [],
      purchaseLogs: [],
      logs: [],
      displayLength: 10,
      loading: true,
      page: 1
    };

    this.handleChangePage = this.handleChangePage.bind(this);
    this.handleChangeLength = this.handleChangeLength.bind(this);
  }

  async componentDidMount() {
    this._isMounted = true;

    let currentUser = await AuthService.getCurrentUser();
    this.setState({
      currentUser: currentUser
    });

    if (!this.state.currentUser) {
      this.props.history.push('/');
      window.location.reload();
    }

    UserService.getUserData().then(
      response => {
        if (response.status === 200 && this._isMounted) {
          UserService.getLogs('donation').then(
            res => {
              res.data.data.forEach(log => { log.createdAt = moment(log.createdAt).format("MM/DD/YYYY HH:mm"); return log; });
              if (this._isMounted) {
                this.setState({
                  donationLogs: res.data.data,
                  loading: false
                });
              }
            },
            err => {
              console.error(err);
            }
          );
          UserService.getLogs('purchase').then(
            res => {
              res.data.data.forEach(log => { log.createdAt = moment(log.createdAt).format("MM/DD/YYYY HH:mm"); return log; });
              if (this._isMounted) {
                this.setState({
                  purchaseLogs: res.data.data,
                  loading: false
                });
                this.setState({
                  logs: [...this.state.donationLogs, ...this.state.purchaseLogs].sort((a, b) => b.id - a.id)
                });
              }
            },
            err => {
              console.error(err);
            }
          );
        }
        else {
          this.props.history.push('/');
          window.location.reload();
        }
      },
      error => {
        console.error(error);
        this.props.history.push('/');
        window.location.reload();
      }
    );
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  handleChangePage(dataKey) {
    if (this._isMounted) {
      this.setState({
        page: dataKey
      });
    }
  }
  handleChangeLength(dataKey) {
    if (this._isMounted) {
      this.setState({
        page: 1,
        displayLength: dataKey
      });
    }
  }
  getData() {
    const { displayLength, page, logs } = this.state;

    return logs.filter((v, i) => {
      const start = displayLength * (page - 1);
      const end = start + displayLength;
      return i >= start && i < end;
    });
  }

  render() {
    const data = this.getData();
    const { logs, loading, page, displayLength } = this.state;

    return (
      <div className="Transactions">
        <Table height={displayLength === 10 ? 500 : 1000} data={data} loading={loading} bordered={false}>
          <Column width={150}>
            <HeaderCell>Date</HeaderCell>
            <Cell dataKey="createdAt" />
          </Column>

          <Column width={500}>
            <HeaderCell>Message</HeaderCell>
            <Cell dataKey="message" />
          </Column>
        </Table>

        <TablePagination
          lengthMenu={[
            {
              value: 10,
              label: 10
            },
            {
              value: 20,
              label: 20
            }
          ]}
          activePage={page}
          displayLength={displayLength}
          total={logs.length}
          onChangePage={this.handleChangePage}
          onChangeLength={this.handleChangeLength}
        />
      </div>
    )
  }
}

export default withRouter (Transactions);