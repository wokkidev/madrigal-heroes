import React, { Component } from 'react';
import moment from 'moment';
import ReactMarkdown from 'react-markdown/with-html';
import { Link } from "react-router-dom";
import { Icon, Tooltip, Whisper } from 'rsuite';

export default class Article extends Component {
  render() {
    const { title, message, author, createdAt, uuid } = this.props.data; // updatedAt

    return (
      <div className="article">
        <Link to={`/article/${uuid}`} className="article-link">
          <div className="mh-header">
            <h3 className="px-3 py-3">{title}</h3>
          </div>
        </Link>
        <div className="mh-text-container px-3 py-3 mb-3">
          <ReactMarkdown source={message} escapeHtml={false} />
          <p className="text-muted text-right">
            <Whisper placement="top" trigger="hover" speaker={<Tooltip>Article created at</Tooltip>}>
              <span className="mr-2"><Icon icon="calendar" /> { moment(createdAt).format("MM/DD/YYYY HH:mm") }</span>
            </Whisper>
            {/* updatedAt !== createdAt && <span><Icon icon="edit2" /> { moment(updatedAt).format("MM/DD/YYYY HH:mm") }&nbsp;</span> */}
            <Whisper placement="top" trigger="hover" speaker={<Tooltip>Article written by</Tooltip>}>
              <span><Icon icon="edit2"/> { author.slice(0, 1).toUpperCase() + author.slice(1, author.length) }</span>
            </Whisper>
          </p>
        </div>
      </div>
    )
  }
}