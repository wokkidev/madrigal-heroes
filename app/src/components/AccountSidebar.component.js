import React from 'react';
import { useRouteMatch, Link } from "react-router-dom";
import { Sidenav, Dropdown, Nav, Icon } from 'rsuite';

import Login from '../pages/Login';

import AuthService from '../services/auth.service';
import UserService from '../services/user.service';

export default class AccountSidebar extends React.Component {
  constructor(props) {
    super(props);

    this.logout = this.logout.bind(this);

    this.state = {
      currentUser: {},
      showGMData: false,
      showAdminData: false,
      account: 'Account'
    }
  }
  async componentDidMount() {
    const currentUser = await AuthService.getCurrentUser();
    this.setState({
      currentUser: currentUser
    });


    if (this.state.currentUser) {
      UserService.getUserData().then(
        response => {
          if (response.data.auth === 'Z') {
            this.setState({
              showGMData: false,
              showAdminData: true,
              account: response.data.account
            });
          }
          else if (response.data.auth === 'L') {
            this.setState({
              showGMData: false,
              account: response.data.account
            });
          }
          else {
            this.setState({
              account: response.data.account
            });
          }
        },
        error => {
          console.error(error);
        }
      )
    }
  }

  logout() {
    AuthService.logout();
    window.location.reload();
  }

  render() {
    const { currentUser, account, showAdminData, showGMData } = this.state;

    return (
      <div>
        {currentUser ?
          <div className="currentuser">
            {this.props.expanded &&
              <h3 className="py-3 px-3 mb-0 mh-header">Account</h3>
            }
            <div className="mh-text-container">
              <Sidenav appearance="subtle" defaultOpenKeys={['1', '2', '3']} activeKey="1" style={{ backgroundColor: 'transparent' }} expanded={this.props.expanded}>
                <Sidenav.Body>
                  <Nav>
                    <SingleMenu eventKey="0" label="Vote" icon={<Icon icon="ticket" />} to="/vote" />
                    <Dropdown eventKey="1" title="My account" icon={<Icon icon="user" />}>
                      <Menu label={account.slice(0, 1).toUpperCase() + account.slice(1, account.length)} to='/user' activeOnlyWhenExact={true} />
                      <Menu label='My characters' to='/user/characters' />
                      <Menu label='Change password' to='/user/changepassword' />
                      <Menu label='Change email' to='/user/changeemail' />
                      <Menu label='Vote logs' to='/user/votelogs' />
                      <Menu label='Transactions' to='/user/transactions' />
                    </Dropdown>
                    {showGMData &&
                    <Dropdown eventKey="2" title="GM" icon={<Icon icon="shield" />}>
                      <Menu label='Home' to='/gm' />
                    </Dropdown>
                    }
                    {showAdminData &&
                    <Dropdown eventKey="3" title="Admin" icon={<Icon icon="user-secret" />}>
                      <Menu label='Accounts' to='/admin/accounts' activeOnlyWhenExact />
                      <Menu label='Banned accounts' to='/admin/accounts/banned' />
                      <Menu label='Articles' to='/admin/articles' />
                      <Menu label='Settings' to='/admin/setting' />
                      <Menu label='Shop' to='/admin/shop' />
                      <Menu label='Logs' to='/admin/logs' />
                      <Menu label='Sockets' to='/admin/clients' />
                    </Dropdown>
                    }
                    <Nav.Item eventKey="4" icon={<Icon icon="sign-out" />} onClick={this.logout}>
                      Logout
                    </Nav.Item>
                    <Nav.Item icon={<Icon icon={this.props.expanded ? 'angle-left' : 'angle-right'} />} onClick={this.props.handleToggle}>
                      Minimize
                    </Nav.Item>
                  </Nav>
                </Sidenav.Body>
              </Sidenav>
            </div>
          </div> :
          <div className="login">
            <h3 className="py-3 px-3 mb-0 mh-header">Login</h3>
            <div className="mh-text-container px-3 py-3">
              <Login />
            </div>
          </div>
        }
      </div>
    )
  }
}

function SingleMenu({ label, to, activeOnlyWhenExact, icon }) {
  let match = useRouteMatch({
    path: to,
    exact: activeOnlyWhenExact
  });

  return (
    <li className={match ? "rs-nav-item rs-nav-item-active" : "rs-nav-item"}>
      <Link className="rs-nav-item-content" to={to} style={{ backgroundColor: 'transparent' }}>{icon} {label}</Link>
    </li>
  );
}

function Menu({ label, to, activeOnlyWhenExact }) {
  let match = useRouteMatch({
    path: to,
    exact: activeOnlyWhenExact
  });

  return (
    <li className={match ? "rs-dropdown-item rs-dropdown-item-active" : "rs-dropdown-item"}>
      <Link className="rs-dropdown-item-content" to={to} style={{ backgroundColor: 'transparent' }}>{label}</Link>
    </li>
  );
}