import React from 'react';
import { Redirect } from 'react-router-dom';
import UserService from '../services/user.service';

export class AuthRequired extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      auth: false,
      isTokenValidated: false
    }
  }

  componentDidMount() {
    UserService.getAdminData().then(
      response => {
        if (response.status === 200) {
          this.setState({
            auth: true,
            isTokenValidated: true
          });
        }
        else {
          this.setState({
            isTokenValidated: true
          });
        }
      },
      error => {
        console.error(error);
        this.setState({
          isTokenValidated: true
        });
      }
    );
  }

  render() {
    const { isTokenValidated, auth } = this.state;

    if (!isTokenValidated) {
      return <div />; // For now we just render a empty div. This could be a loader if we wanted it to be.
    }

    if (isTokenValidated && auth) {
      return (this.props.componentToRender)
    }
    else {
      return (<Redirect to="/" />)
    }
  }
}

export default AuthRequired;