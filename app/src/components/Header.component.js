import React from 'react';

import logo from '../assets/logo.png';

export default class Header extends React.Component {
  render() {
    return (
      <div className="container-fluid text-center header-image py-4">
        <img src={logo} alt="Madrigal Heroes" height="200px" style={{ paddingTop: '20px' }} />
      </div>
    )
  }
}