import axios from 'axios';
import Cookies from "js-cookie";

class AuthService {
  login(username, password) {
    return axios
      .post(`${process.env.REACT_APP_API_ENDPOINT}auth/login`, {
        username,
        password
      })
      .then(response => {
        return response.data.data;
      })
  }

  gameLogin(user_id, m_idPlayer, server_index, md5, check) {
    return axios.post(`${process.env.REACT_APP_API_ENDPOINT}auth/gamelogin`, {
      user_id,
      m_idPlayer,
      server_index,
      md5,
      check
    }).then(
      response => {
        if (response.data.data.accessToken) {
          Cookies.set('user', JSON.stringify(response.data.data));
        }

        return response.data.data;
      }
    )
  }

  logout() {
    return axios
      .post(`${process.env.REACT_APP_API_ENDPOINT}auth/logout`)
      .then(response => {
        if (response.status === 200) {
          window.location.reload();
        }
      });
  }

  register(username, email, password) {
    return axios.post(`${process.env.REACT_APP_API_ENDPOINT}auth/signup`, {
      username,
      email,
      password
    });
  }

  /*
   * This function has to be implemented to the backend if we wish to use it
   
  forgotPassword(accountOrEmail) {
    return axios.post(`${process.env.REACT_APP_API_ENDPOINT}auth/forgotpassword`, {
      accountOrEmail
    });
  }*/

  getCurrentUser() {
    return new Promise((resolve, reject) => {
      axios.get(`${process.env.REACT_APP_API_ENDPOINT}auth/account`)
        .then(response => {
          if (response.status === 200) {
            resolve(response.data.data);
          }
          else {
            resolve(false);
          }
        })
        .catch(error => {
          resolve(false);
        })
    })
  }
}

export default new AuthService();