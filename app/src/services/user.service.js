import axios from 'axios';

class UserService {
  getUserData() {
    return axios.get(`${process.env.REACT_APP_API_ENDPOINT}test/user`);
  }

  getGMData() {
    return axios.get(`${process.env.REACT_APP_API_ENDPOINT}test/gm`);
  }

  getAdminData() {
    return axios.get(`${process.env.REACT_APP_API_ENDPOINT}test/admin`);
  }

  getAccountData() {
    return axios.get(`${process.env.REACT_APP_API_ENDPOINT}auth/account`);
  }

  changePassword(password) {
    return axios
      .post(`${process.env.REACT_APP_API_ENDPOINT}auth/changepassword` , {
        password
      })
      .then(response => {
        return response.data;
      })
  }

  changeEmail(email) {
    return axios
      .post(`${process.env.REACT_APP_API_ENDPOINT}auth/changeemail`, {
        email
      })
      .then(response => {
        return response.data;
      });
  }

  getLogs(type) {
    return axios.get(`${process.env.REACT_APP_API_ENDPOINT}logs/${type}`);
  }

  getCharacters(account) {
    return axios.get(`${process.env.REACT_APP_API_ENDPOINT}characters/${account}`);
  }

  getCharacter(name) {
    return axios.get(`${process.env.REACT_APP_API_ENDPOINT}character/${name}`);
  }
}

export default new UserService();