import axios from 'axios';

class SettingService {
  getSettings() {
    return axios.get(`${process.env.REACT_APP_API_ENDPOINT}setting`);
  }
  
  getSetting(name) {
    return axios.get(`${process.env.REACT_APP_API_ENDPOINT}setting/${name}`);
  }
}

export default new SettingService();