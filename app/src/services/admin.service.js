import axios from 'axios';

class AdminService {
  /**
   * Mac ban account
   * @param {*} account 
   */
  macBanAccount(account) {
    return axios.post(`${process.env.REACT_APP_API_ENDPOINT}macban/add`, {
      account
    });
  }

  /**
   * Remove account mac ban
   * @param {*} account 
   */
  macUnbanAccount(account) {
    return axios.post(`${process.env.REACT_APP_API_ENDPOINT}macban/remove`, {
      account
    });
  }

  /**
   * Get all banned accounts
   */
  getBannedAccounts() {
    return axios.get(`${process.env.REACT_APP_API_ENDPOINT}macban`);
  }

  /**
   * Get a single account
   */
  getAccounts() {
    return axios.get(`${process.env.REACT_APP_API_ENDPOINT}accounts`);
  }

  /**
   * Get account by name
   * @param {*} account 
   */
  getAccount(account) {
    return axios.get(`${process.env.REACT_APP_API_ENDPOINT}accounts/${account}`);
  }

  /**
   * Update account
   * @param {*} account 
   * @param {*} values 
   */
  updateAccount(account, values) {
    return axios.put(`${process.env.REACT_APP_API_ENDPOINT}accounts`, {
      account,
      values
    });
  }

  /**
   * Add article
   * @param {*} title 
   * @param {*} message 
   */
  addArticle(title, message) {
    return axios.post(`${process.env.REACT_APP_API_ENDPOINT}news`, {
        title,
        message
      })
      .then(response => {
        return response.data;
      })
  }

  /**
   * Update article
   * @param {*} uuid 
   * @param {*} title 
   * @param {*} message 
   */
  updateArticle(uuid, title, message) {
    return axios.put(`${process.env.REACT_APP_API_ENDPOINT}news`, {
        uuid,
        title,
        message
      })
      .then(response => {
        return response.data;
      });
  }

  /**
   * Delete article
   * @param {*} uuid 
   */
  deleteArticle(uuid) {
    return axios.delete(`${process.env.REACT_APP_API_ENDPOINT}news`, { data: { uuid } })
      .then(response => {
        return response;
      });
  }

  /**
   * Update setting
   * @param {*} uuid 
   * @param {*} value 
   */
  updateSetting(uuid, value) {
    return axios.put(`${process.env.REACT_APP_API_ENDPOINT}setting`, {
      uuid: uuid,
      value: value
    });
  }

  /**
   * Add item to the store
   * @param {*} name 
   * @param {*} image 
   * @param {*} price 
   * @param {*} salePrice 
   * @param {*} isSale 
   * @param {*} isVote 
   * @param {*} itemId 
   * @param {*} description 
   * @param {*} categoryId 
   */
  addItem(name, image, price, salePrice, isSale, isVote, itemId, description, categoryId, quantity) {
    return axios.post(`${process.env.REACT_APP_API_ENDPOINT}store/items`, {
      name,
      image,
      price,
      salePrice,
      isSale,
      isVote,
      itemId,
      description,
      categoryId,
      quantity
    });
  }

  /**
   * Update store item
   * @param {*} id 
   * @param {*} name 
   * @param {*} image 
   * @param {*} price 
   * @param {*} salePrice 
   * @param {*} isSale 
   * @param {*} isVote 
   * @param {*} itemId 
   * @param {*} description 
   * @param {*} categoryId 
   */
  updateItem(id, name, image, price, salePrice, isSale, isVote, itemId, description, categoryId, quantity) {
    return axios.put(`${process.env.REACT_APP_API_ENDPOINT}store/items`, {
      id,
      name,
      image,
      price,
      salePrice,
      isSale,
      isVote,
      itemId,
      description,
      categoryId,
      quantity
    });
  }

  /**
   * Delete store item
   * @param {*} id 
   */
  deleteItem(id) {
    return axios.delete(`${process.env.REACT_APP_API_ENDPOINT}store/items`, { data: { id } });
  }

  /**
   * Add a new category
   * @param {*} name 
   */
  addCategory(name) {
    return axios.post(`${process.env.REACT_APP_API_ENDPOINT}store/categories`, {
      name
    });
  }

  /**
   * Update category
   * @param {*} id 
   * @param {*} name 
   */
  updateCategory(id, name) {
    return axios.put(`${process.env.REACT_APP_API_ENDPOINT}store/categories`, {
      id,
      name
    });
  }

  /**
   * Delete category
   * @param {*} id 
   */
  deleteCategory(id) {
    return axios.delete(`${process.env.REACT_APP_API_ENDPOINT}store/categories`, { data: { id } });
  }

  /**
   * Get all logs of type
   * @param {*} type 
   */
  getAllLogs(type) {
    return axios.get(`${process.env.REACT_APP_API_ENDPOINT}alllogs/${type}`);
  }
}

export default new AdminService();