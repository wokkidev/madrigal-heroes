import axios from 'axios';

class ArticleService {
  getArticles() {
    return axios.get(`${process.env.REACT_APP_API_ENDPOINT}news`).then(
      response => {
        return response.data;
      }
    );
  }

  getArticle(uuid) {
    return axios.get(`${process.env.REACT_APP_API_ENDPOINT}news/${uuid}`).then(
      response => {
        return response.data;
      }
    );
  }
}

export default new ArticleService();