import axios from 'axios';

class StoreService {
  getItems() {
    return axios.get(`${process.env.REACT_APP_API_ENDPOINT}store/items`);
  }

  getItem(id) {
    return axios.get(`${process.env.REACT_APP_API_ENDPOINT}store/item/${id}`);
  }

  getCategories() {
    return axios.get(`${process.env.REACT_APP_API_ENDPOINT}store/categories`);
  }

  getItemsByCategory(id) {
    return axios.get(`${process.env.REACT_APP_API_ENDPOINT}store/categories/${id}`);
  }

  purchaseItem(id, character, amount) {
    return axios.post(`${process.env.REACT_APP_API_ENDPOINT}store/purchase`, {
      itemId: id,
      characterId: character,
      amount: amount
    });
  }

  getItemsData() {
    return axios.get(`${process.env.REACT_APP_API_ENDPOINT}store/itemsdata`);
  }
}

export default new StoreService();