import React from 'react';
import { Link } from "react-router-dom";
import { Route } from "react-router-dom";

import Shop from '../pages/store/Store';

export default class ShopLayout extends React.Component {
  render() {
    return (
      <div className="ShopLayout">
        <div className="container my-5">
          <Route path="/shop/full" component={Shop} />
          <Link to="/shop" type="button" className="btn btn-primary mt-3">Back</Link>
        </div>
      </div>
    )
  }
}