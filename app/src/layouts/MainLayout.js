import React from 'react';
import { Route, Switch, NavLink } from "react-router-dom";

import Header from '../components/Header.component';
import Navigation from '../components/Navigation.component';
import NavigationFooter from '../components/NavigationFooter.component';
import AccountSidebar from '../components/AccountSidebar.component';

import Home from '../pages/Home'
import Article from '../pages/Article';
import Login from '../pages/Login';
import Register from '../pages/Register';
import Ranking from '../pages/Ranking';
import Download from '../pages/Download';
import Donate from '../pages/Donate';
import DonateComplete from '../pages/DonateComplete';
import DonateCancel from '../pages/DonateCancel';
import Vote from '../pages/Vote';
//import ForgotPassword from '../pages/ForgotPassword';
import Discord from '../pages/Discord';
import Team from '../pages/team';
import Terms from '../pages/terms';
import Privacy from '../pages/privacy';

import Admin from '../pages/admin/AdminHome';
import AdminArticles from '../pages/admin/Articles';
import CreateArticle from '../pages/admin/CreateArticle';
import EditArticle from '../pages/admin/EditArticle';
import Setting from '../pages/admin/Settings';
import EditStore from '../pages/admin/Shop';
import AdminLogs from '../pages/admin/Logs';
import Clients from '../pages/admin/Clients';
import AdminAccounts from '../pages/admin/Accounts';
import ViewCharacter from '../pages/admin/Character';
import BannedAccounts from '../pages/admin/BannedAccounts';

import User from '../pages/user/UserHome';
import ChangePassword from '../pages/user/ChangePassword';
import ChangeEmail from '../pages/user/ChangeEmail';
import MyCharacters from '../pages/user/MyCharacters';
import UserVoteLogs from '../pages/user/VoteLogs';
import UserDonateLogs from '../pages/user/DonateLogs';

import Shop from '../pages/store/Store';

import NotFoundPage from '../pages/404';

import AdminRoute from '../components/AdminRoute';
import UserRoute from '../components/UserRoute';

export default class MainLayout extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      expanded: true
    }

    this.handleExpand = this.handleExpand.bind(this);
  }

  handleExpand() {
    this.setState({
      expanded: !this.state.expanded
    });
  }

  render() {
    return (
      <div className="MainLayout">
        <Header />
        <Navigation />
        <NavigationFooter />
        <div className="container my-5">
          <div className="row flex-column-reverse flex-md-row">
            <div className={'mb-3' + (this.state.expanded ? ' col-md-3' : ' col-md-1')}>
              <AccountSidebar expanded={this.state.expanded} handleToggle={this.handleExpand} />
              {this.state.expanded &&
                <iframe className="mt-3" src="https://discordapp.com/widget?id=704969866960175164&theme=dark" width="100%" height="400" allowtransparency="true" frameBorder="0" title="discord"></iframe>
              }
            </div>
            <div className={'mb-3' + (this.state.expanded ? ' col-md-9' : ' col-md-11')}>
              <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/article/:uuid" component={Article} />
                <Route path="/download" component={Download} />
                <Route path="/register" component={Register} />
                <Route path="/ranking" component={Ranking} />
                <Route path="/login" component={Login} />
                <Route exact path="/donate" component={Donate} />
                <Route path="/donate/complete" component={DonateComplete} />
                <Route path="/donate/cancel" component={DonateCancel} />
                <Route path="/vote" component={Vote} />
                {/*<Route path="/forgotpassword" component={ForgotPassword} />*/}
                <Route path="/discord" component={Discord} />
                <Route path="/team" component={Team} />
                <Route path="/terms" component={Terms} />
                <Route path="/privacy" component={Privacy} />

                <Route path="/shop" component={Shop} />

                <Route exact path="/user" render={() => (<UserRoute componentToRender={<User />} />)}  />
                <Route exact path="/user/changeemail" render={() => (<UserRoute componentToRender={<ChangeEmail />} />)} />
                <Route exact path="/user/changepassword" render={() => (<UserRoute componentToRender={<ChangePassword />} />)} />
                <Route exact path="/user/characters" render={() => (<UserRoute componentToRender={<MyCharacters />} />)} />
                <Route exact path="/user/votelogs" render={() => (<UserRoute componentToRender={<UserVoteLogs />} />)} />
                <Route exact path="/user/transactions" render={() => (<UserRoute componentToRender={<UserDonateLogs />} />)} />

                <Route exact path="/admin" render={() => (<AdminRoute componentToRender={<Admin />} />)} />
                <Route exact path="/admin/articles" render={() => (<AdminRoute componentToRender={<AdminArticles />} />)} />
                <Route exact path="/admin/articles/new"  render={() => (<AdminRoute componentToRender={<CreateArticle />} />)} />
                <Route exact path="/admin/articles/edit/:uuid" render={() => (<AdminRoute componentToRender={<EditArticle />} />)} />
                <Route exact path="/admin/setting" render={() => (<AdminRoute componentToRender={<Setting />} />)} />
                <Route exact path="/admin/shop" render={() => (<AdminRoute componentToRender={<EditStore />} />)} />
                <Route exact path="/admin/logs" render={() => (<AdminRoute componentToRender={<AdminLogs />} />)} />
                <Route exact path="/admin/accounts" render={() => (<AdminRoute componentToRender={<AdminAccounts />} />)} />
                <Route exact path="/admin/character/:name" render={() => (<AdminRoute componentToRender={<ViewCharacter />} />)} />
                <Route exact path="/admin/accounts/banned" render={() => (<AdminRoute componentToRender={<BannedAccounts />} />)} />
                <Route exact path="/admin/clients" render={() => (<AdminRoute componentToRender={<Clients />} />)} />

                <Route component={NotFoundPage} />
              </Switch>
            </div>
          </div>
        </div>

        <footer id="footer" className="py-4">
          <div className="container">
            <div className="row">
              <div className="col-md-3 text-center">
                <img src={process.env.PUBLIC_URL + '/logo192.png'} alt="Madrigal Heroes" className="mb-2" />
              </div>
              <div className="col-md-2">
                <h5 className="mx-3 mb-2" style={{ textShadow: '1px 1px 3px black' }}>Navigation</h5>
                <ul className="list-group list-group-flush">
                  <li className="list-group-item py-1" style={{ backgroundColor: 'transparent', borderBottom: '0px' }}><NavLink exact to="/" className="text-light" style={{ textShadow: '1px 1px 3px black' }} onClick={() => { document.documentElement.scrollTop = 0; document.body.scrollTop = 0; }}>Home</NavLink></li>
                  <li className="list-group-item py-1" style={{ backgroundColor: 'transparent', borderBottom: '0px' }}><NavLink to="/download" className="text-light" style={{ textShadow: '1px 1px 3px black' }} onClick={() => { document.documentElement.scrollTop = 0; document.body.scrollTop = 0; }}>Download</NavLink></li>
                  <li className="list-group-item py-1" style={{ backgroundColor: 'transparent', borderBottom: '0px' }}><NavLink to="/register" className="text-light" style={{ textShadow: '1px 1px 3px black' }} onClick={() => { document.documentElement.scrollTop = 0; document.body.scrollTop = 0; }}>Register</NavLink></li>
                  <li className="list-group-item py-1" style={{ backgroundColor: 'transparent', borderBottom: '0px' }}><NavLink to="/ranking" className="text-light" style={{ textShadow: '1px 1px 3px black' }} onClick={() => { document.documentElement.scrollTop = 0; document.body.scrollTop = 0; }}>Ranking</NavLink></li>
                  <li className="list-group-item py-1" style={{ backgroundColor: 'transparent', borderBottom: '0px' }}><NavLink to="/donate" className="text-light" style={{ textShadow: '1px 1px 3px black' }} onClick={() => { document.documentElement.scrollTop = 0; document.body.scrollTop = 0; }}>Donate</NavLink></li>
                  <li className="list-group-item py-1" style={{ backgroundColor: 'transparent', borderBottom: '0px' }}><NavLink to="/shop" className="text-light" style={{ textShadow: '1px 1px 3px black' }} onClick={() => { document.documentElement.scrollTop = 0; document.body.scrollTop = 0; }}>Shop</NavLink></li>
                </ul>
              </div>
              <div className="col-md-2">
                <h5 className="mx-3 mb-2" style={{ textShadow: '1px 1px 3px black' }}>About</h5>
                <ul className="list-group list-group-flush">
                  <li className="list-group-item py-1" style={{ backgroundColor: 'transparent', borderBottom: '0px' }}><NavLink exact to="/terms" className="text-light" style={{ textShadow: '1px 1px 3px black' }} onClick={() => { document.documentElement.scrollTop = 0; document.body.scrollTop = 0; }}>Terms of Service</NavLink></li>
                  <li className="list-group-item py-1" style={{ backgroundColor: 'transparent', borderBottom: '0px' }}><NavLink exact to="/privacy" className="text-light" style={{ textShadow: '1px 1px 3px black' }} onClick={() => { document.documentElement.scrollTop = 0; document.body.scrollTop = 0; }}>Privacy Policy</NavLink></li>
                  <li className="list-group-item py-1" style={{ backgroundColor: 'transparent', borderBottom: '0px' }}><NavLink exact to="/contact" className="text-light" style={{ textShadow: '1px 1px 3px black' }} onClick={() => { document.documentElement.scrollTop = 0; document.body.scrollTop = 0; }}>Contact us</NavLink></li>
                  <li className="list-group-item py-1" style={{ backgroundColor: 'transparent', borderBottom: '0px' }}><NavLink exact to="/team" className="text-light" style={{ textShadow: '1px 1px 3px black' }} onClick={() => { document.documentElement.scrollTop = 0; document.body.scrollTop = 0; }}>Team</NavLink></li>
                </ul>
              </div>
            </div>

            <p className="mt-3 text-center" style={{ textShadow: '1px 1px 3px black' }}>
              &copy; Madrigal Heroes 2020 ─ All images and other content related to FlyFF are owned by Webzen.
            </p>
          </div>
        </footer>
      </div>
    )
  }
}