import React from 'react';
import { Route } from "react-router-dom";

import Patcher from '../pages/Patcher';

export default class PatcherLayout extends React.Component {
  render() {
    return (
      <div className="PatcherLayout">
        <div className="container-fluid mx-0 px-0">
          <Route path="/patcher" component={Patcher} />
        </div>
      </div>
    )
  }
}