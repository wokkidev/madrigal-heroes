import React from 'react';
import Cookies from "js-cookie";
import 'bootstrap/dist/css/bootstrap.css';
import 'jquery/dist/jquery.slim';
import 'bootstrap/dist/js/bootstrap.bundle';
import 'rsuite/dist/styles/rsuite-default.css';
import {
  BrowserRouter as Router,
  Route
} from "react-router-dom";
import './App.css';

import MainLayout from './layouts/MainLayout';
import ShopLayout from './layouts/ShopLayout';
import PatcherLayout from './layouts/PatcherLayout';

import io from "socket.io-client";
import SocketContext from './socketContext';

import { Notification } from 'rsuite';

const layoutAssignment = {
  '/shop/full': ShopLayout,
  '/patcher': PatcherLayout
};

const layoutPicker = function(props) {
  let Layout = layoutAssignment[props.location.pathname];
  return Layout ? <Layout/> : <MainLayout/>;
};

const socket = io(process.env.REACT_APP_SOCKET, { secure: true }); // , path: '/api/'
socket
  .on('requestIdentity', () => {
    socket.emit('identify', { user: Cookies.get('user') ? JSON.parse(Cookies.get('user')) : undefined });
  })
  .on('notice', (data) => {
    Notification['info']({
      title: 'Notice',
      placement: 'topStart',
      description: data.message
    });
  })
  .on('voteSuccess', (vote) => {
    let storedUser = JSON.parse(Cookies.get('user'));
    storedUser.vote = parseInt(storedUser.vote) + vote.amount;
    Cookies.set('user', JSON.stringify(storedUser));
  });

class App extends React.Component {
  render() {
    return (
      <SocketContext.Provider value={socket}>
        <div className="App">
          <Router>
            <Route path="*" render={layoutPicker} />
          </Router>
        </div>
      </SocketContext.Provider>
    );
  }
}

export default App;