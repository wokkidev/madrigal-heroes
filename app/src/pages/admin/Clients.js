import React, { Component } from "react";

import AuthService from "../../services/auth.service";

import SocketContext from '../../socketContext';

class Clients extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      currentUser: {},
      clients: []
    }
  }

  async componentDidMount() {
    this._isMounted = true;

    let currentUser = await AuthService.getCurrentUser();
    this.setState({
      currentUser: currentUser
    });

    this.props.socket.emit('getClients');

    this.props.socket
      .on('clients', clients => {
        if (this._isMounted) {
          this.setState({
            clients: clients
          });
        }
      });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  sendMessage(socket) {
    let value = window.prompt('Enter message');
    this.props.socket.emit('sendMessage', { to: socket, message: value });
  }

  render() {
    const { clients } = this.state;

    return (
      <div className="Clients">
        <h3 className="py-3 px-3 mb-0 mh-header">Connected clients</h3>
        <div className="mh-text-container px-3 py-3">
          <p>This page lists all sockets connected to the API. Current clients count: {clients.length}</p>
          <br/>
          <ul>
            {clients.map((client, index) => (
              <li key={index}>{client[0] || 'Unknown user'} - {client[1]} <button className="btn btn-sm btn-info" onClick={() => this.sendMessage(client[1])}>Send message</button></li>
            ))}
          </ul>
        </div>
      </div>
    )
  }
}

const ClientsWithSocket = props => (
  <SocketContext.Consumer>
    {socket => <Clients {...props} socket={socket} />}
  </SocketContext.Consumer>
);

export default ClientsWithSocket;