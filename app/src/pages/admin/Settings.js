import React from "react";
import update from 'immutability-helper';

import AuthService from "../../services/auth.service";
import AdminService from '../../services/admin.service';
import SettingService from '../../services/setting.service';

import { Alert } from 'rsuite';

class Setting extends React.Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      currentUser: {},
      loading: false,
      settings: [],
      editingSetting: 0,
      newValue: ''
    }

    this.settingValueChange = this.settingValueChange.bind(this);
  }

  async componentDidMount() {
    this._isMounted = true;

    let currentUser = await AuthService.getCurrentUser();
    this.setState({
      currentUser: currentUser
    });

    SettingService.getSettings().then(
      settings => {
        if (this._isMounted) {
          this.setState({
            settings: settings.data.data,
            loading: false
          });
        }
      },
      error => {
        console.error(error);
      }
    )
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  toggleEditSetting(setting) {
    if (this._isMounted) {
      this.setState({
        editingSetting: setting.uuid,
        newValue: setting.value
      });
    }
  }

  saveSetting(setting) {
    const settingIndex = this.state.settings.findIndex(x => x.uuid === setting.uuid);

    AdminService.updateSetting(setting.uuid, this.state.newValue).then(
      () => {
        Alert.success('Setting updated', 5000);

        this.setState(update(this.state, { settings: { [settingIndex]: { value: { $set: this.state.newValue } } } }));
        this.setState({
          newValue: '',
          editingSetting: 0
        });
      },
      error => {
        Alert.error('Setting update error. Info logged to console.', 5000);
        console.error(error);
      }
    )
  }

  settingValueChange(e) {
    this.setState({
      newValue: e.target.value
    });
  }

  render() {
    const { loading, settings, editingSetting } = this.state;

    return (
      <div className="Setting">
      <h3 className="py-3 px-3 mb-0 mh-header">Settings</h3>
      <div className="mh-text-container px-3 py-3">
        <p><strong>NOTE:</strong> The way download links work was changed. Instead of just adding the download link, you'll have to do the following format. <code>https://download.link|Link text;https://download.link/2|Another link text</code>.</p>
        <div className="table-responsive">
          {loading &&
            <div className="spinner-border text-primary" role="status"><span className="sr-only">Loading...</span></div>
          }
          <table className="table">
            <thead>
              <tr>
                <th>Name</th>
                <th>Value</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {settings.map((setting, index) => (
                <tr key={index}>
                  <td>
                    {setting.name}
                  </td>
                  <td>
                    {editingSetting === setting.uuid ?
                     <input type="text" name="value" value={this.state.newValue} className="form-control" onChange={this.settingValueChange} /> :
                     setting.value
                    }
                  </td>
                  <td>
                    {editingSetting === setting.uuid ?
                      <div className="btn-group" role="group" aria-label="Edit settings">
                        <button type="button" className="btn btn-sm btn-danger" onClick={() => this.toggleEditSetting({ uuid: 0, value: '' })}>Cancel</button>
                        <button type="button" className="btn btn-sm btn-success" onClick={() => this.saveSetting(setting)}>Save</button>
                      </div> :
                      <button type="button" className="btn btn-sm btn-success" onClick={() => this.toggleEditSetting(setting)}>Edit</button>
                    }
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
    )
  }
}

export default Setting;