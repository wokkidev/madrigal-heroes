import React, { Component } from "react";
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import ReactMarkdown from 'react-markdown/with-html';

import AuthService from "../../services/auth.service";
import AdminService from '../../services/admin.service';

const formRef = React.createRef();

export default class CreateArticle extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      currentUser: {},
      displaySuccess: false,
      successMessage: '',
      preview: false,
      previewTitle: '',
      previewMessage: ''
    };

    this.updatePreview = this.updatePreview.bind(this);
    this.closePreview = this.closePreview.bind(this);
  }

  async componentDidMount() {
    this._isMounted = true;

    let currentUser = await AuthService.getCurrentUser();
    this.setState({
      currentUser: currentUser
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  updatePreview() {
    const values = formRef.current.getFieldProps();
    const { title, message } = values.value;

    if (this._isMounted) {
      this.setState({
        preview: true,
        previewTitle: title,
        previewMessage: message
      });
    }
  }

  closePreview() {
    if (this._isMounted) {
      this.setState({
        preview: false
      });
    }
  }

  render() {
    const { preview } = this.state;

    return (
      <div className="CreateArticle">
        <h3 className="py-3 px-3 mb-0 mh-header">Write an article</h3>
        <div className="mh-text-container px-3 py-3">
          {this.state.displaySuccess &&
            <div className={'alert alert-success'} dangerouslySetInnerHTML={{ __html:  this.state.successMessage }}></div>
          }
          <Formik
            innerRef={formRef}
            initialValues={{
              title: '',
              message: ''
            }}
            validationSchema={Yup.object().shape({
              title: Yup.string().required('Title is required'),
              message: Yup.string().required('Message is required')
            })}
            onSubmit={({ title, message }, { setStatus, setSubmitting }) => {
              setStatus();
              AdminService.addArticle(title, message).then(
                (article) => {
                  if (this._isMounted) {
                    this.setState({
                      successMessage: `Article added! <a href="/article/${article.data.uuid}" target="_blank">Link to article</a>`,
                      displaySuccess: true
                    });
                  }
                  setSubmitting(false);
                },
                error => {
                  const resMessage =
                    (error.response &&
                      error.response.data &&
                      error.response.data.message) ||
                    error.message ||
                    error.toString();
                  setStatus(resMessage);
                  setSubmitting(false);
                }
              )
            }}
          >
            {({ errors, status, touched, isSubmitting }) => (
              <Form>
                {status &&
                  <div className={'alert alert-danger'}>{ status }</div>
                }
                <div className="form-group">
                  <label htmlFor="title">Title</label>
                  <Field name="title" type="text" className={'form-control' + (errors.title && touched.title ? ' is-invalid' : '')} />
                  <ErrorMessage name="title" component="div" className="invalid-feedback" />
                </div>
                <div className="form-group">
                  <label htmlFor="message">Message</label>
                  <Field name="message" as="textarea" rows="20" className={'form-control' + (errors.message && touched.message ? ' is-invalid' : '')} />
                  <ErrorMessage name="message" component="div" className="invalid-feedback" />
                </div>
                <div className="form-group">
                  <button type="submit" className="btn btn-primary" disabled={isSubmitting}>Add article</button>
                  <button type="button" className="btn btn-primary ml-2" onClick={this.updatePreview}>Update preview</button>
                  {isSubmitting &&
                    <div className="spinner-border text-primary" role="status"><span className="sr-only">Loading...</span></div>
                  }
                </div>
              </Form>
            )}
          </Formik>
        </div>

        {preview &&
          <div>
            <div className="mh-header mt-3">
              <h3 className="px-3 py-3">
                {this.state.previewTitle}
              </h3>
            </div>
            <div className="mh-text-container px-3 py-3">
              <ReactMarkdown source={this.state.previewMessage} escapeHtml={false} />
              <button className="btn btn-primary mt-3" onClick={this.closePreview}>Close preview</button>
            </div>
          </div>
        }
      </div>
    )
  }
}