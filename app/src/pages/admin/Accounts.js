import React from 'react';
import update from 'immutability-helper';

import { Alert } from 'rsuite';

import AuthService from "../../services/auth.service";
import AdminService from '../../services/admin.service';

export default class AdminAccounts extends React.Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      currentUser: {},
      account: {},
      accountName: '',
      loading: false
    }

    this.handleChange = this.handleChange.bind(this);
    this.getAccount = this.getAccount.bind(this);
  }

  async componentDidMount() {
    this._isMounted = true;

    let currentUser = await AuthService.getCurrentUser();
    this.setState({
      currentUser: currentUser
    });    
  }

  componentWillMount() {
    this._isMounted = false;
  }

  getAccount() {
    if (this._isMounted) {
      this.setState({
        loading: true
      });

      AdminService.getAccount(this.state.accountName.toLowerCase()).then(
        account => {
          if (account.data.data) {
            if (this._isMounted) {
              this.setState({
                account: account.data.data,
                loading: false
              });
            }
            Alert.success('Account fetched!', 5000);
          }
          else {
            Alert.error('No account found with given name', 5000);
            if (this._isMounted) {
              this.setState({
                loading: false
              });
            }
          }
        },
        error => {
          Alert.error('Could not fetch account, error logged to console', 5000);
          console.error(error);
          if (this._isMounted) {
            this.setState({
              loading: false
            });
          }
        }
      )
    }
  }

  handleChange(e) {
    if (this._isMounted) {
      this.setState({
        accountName: e.target.value
      });
    }
  }

  changeMP(amount) {
    if (!Number.isInteger(parseInt(amount))) {
      return Alert.error(`Only numbers are allowed`, 5000);
    }

    AdminService.updateAccount(this.state.accountName, { cash: amount }).then(
      account => {
        if (account.status === 200) {
          if (this._isMounted) {
            this.setState(update(this.state, { account: { cash: { $set: amount } } }));
          }
          Alert.success(`Madrigal Points changed to ${amount}`, 5000);
        }
        else {
          console.error(account);
          Alert.error(`Could not edit points. Error logged to console.`, 5000);
        }
      },
      error => {
        console.error(error);
        Alert.error(`Could not edit points. Error logged to console.`, 5000);
      }
    );
    
  }

  changeVP(amount) {
    if (!Number.isInteger(parseInt(amount))) {
      return Alert.error(`Only numbers are allowed`, 5000);
    }

    AdminService.updateAccount(this.state.accountName, { vote: amount }).then(
      account => {
        if (account.status === 200) {
          if (this._isMounted) {
            this.setState(update(this.state, { account: { vote: { $set: amount } } }));
          }
          Alert.success(`Vote Points changed to ${amount}`, 5000);
        }
        else {
          console.error(account);
          Alert.error(`Could not edit points. Error logged to console.`, 5000);
        }
      },
      error => {
        console.error(error);
        Alert.error(`Could not edit points. Error logged to console.`, 5000);
      }
    );
  }

  macBan(account) {
    AdminService.macBanAccount(account).then(
      data => {
        console.log(data);
        Alert.success(`Account banned!`, 5000);
      },
      error => {
        console.error(error);
        Alert.error(`Could not ban account. Error logged to console.`, 5000);
      }
    )
  }

  macUnban(account) {
    AdminService.macUnbanAccount(account).then(
      data => {
        console.log(data);
        Alert.success(`Account unbanned!`, 5000);
      },
      error => {
        console.error(error);
        Alert.error(`Could not unban account. Error logged to console.`, 5000);
      }
    )
  }

  render() {
    const { loading, account } = this.state;

    return (
      <div className="Accounts">
        <h3 className="py-3 px-3 mb-0 mh-header">Accounts</h3>
        <div className="mh-text-container px-3 py-3">
          <div className="form-group">
            <div className="input-group">
              <input type="text" className="form-control" id="account" name="account" placeholder="Account name" onChange={this.handleChange} />
              <div className="input-group-append">
                <button className="btn btn-primary" onClick={this.getAccount} disabled={loading}>Get account</button>
              </div>
            </div>
          </div>

          {Object.keys(account).length > 0 &&
            <table className="table">
              <thead>
                <tr>
                  <th>Account</th>
                  <th>Madrigal Points</th>
                  <th>Vote Points</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>{account.account}</td>
                  <td>{account.cash} <button className="btn btn-sm btn-link" onClick={() => { const amount = prompt('Amount to set to'); if(amount) { this.changeMP(amount) } }}>Change</button></td>
                  <td>{account.vote} <button className="btn btn-sm btn-link" onClick={() => { const amount = prompt('Amount to set to'); if(amount) { this.changeVP(amount) } }}>Change</button></td>
                  <td>
                    {account.detail.ban ?
                      <button className="btn btn-sm btn-danger" onClick={() => { if(window.confirm('Are you sure you want to mac unban this account?')) this.macUnban(account.account); }}>Mac Unban</button> :
                      <button className="btn btn-sm btn-danger" onClick={() => { if(window.confirm('Are you sure you want to mac ban this account?')) this.macBan(account.account); }}>Mac Ban</button>
                    }
                  </td>
                </tr>
              </tbody>
            </table>
          }
        </div>
      </div>
    )
  }
}