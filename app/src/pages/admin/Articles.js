import React, { Component } from "react";
import moment from 'moment';
import { Link } from "react-router-dom";

import { Message } from 'rsuite';

import AuthService from "../../services/auth.service";
import ArticleService from '../../services/article.service';
import AdminService from '../../services/admin.service';

export default class CreateArticle extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      currentUser: {},
      displayError: false,
      errorMessage: '',
      displaySuccess: false,
      successMessage: '',
      articles: [],
      loading: true
    };
  }

  async componentDidMount() {
    this._isMounted = true;

    const user = AuthService.getCurrentUser();

    let currentUser = await AuthService.getCurrentUser();
    this.setState({
      currentUser: currentUser
    });

    ArticleService.getArticles().then(
      articles => {
        if (this._isMounted) {
          this.setState({
            articles: articles.data,
            loading: false
          });
        }
      },
      error => {
        console.error(error);
        if (this._isMounted) {
          this.setState({
            displayError: true,
            errorMessage: 'An error occurred while attempting to load news articles. Try again later.'
          });
        }
      }
    )
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  deleteArticle(uuid) {
    AdminService.deleteArticle(uuid).then(
      response => {
        if (response.status === 200 && this._isMounted) {
          this.setState({
            articles: this.state.articles.filter(article => article.uuid !== uuid),
            successMessage: 'Article deleted',
            displaySuccess: true
          });
        }
        else {
          if (this._isMounted) {
            this.setState({
              errorMessage: 'An error occurred while attempting to delete an article. Try again later.',
              displayError: true
            });
          }
        }
      },
      error => {
        if (this._isMounted) {
          this.setState({
            errorMessage: error,
            displayError: true
          });
        }
      }
    )
  }

  render() {
    const { articles } = this.state;

    return (
      <div className="Articles">
        <h3 className="py-3 px-3 mb-0 mh-header">Articles</h3>
        <div className="mh-text-container px-3 py-3">
          <Link to="/admin/articles/new" type="button" className="btn btn-primary mb-2">New article</Link>
          {this.state.displayError &&
            <Message type="error" description={this.state.errorMessage} />
          }
          {this.state.displaySuccess &&
            <Message type="success" description={this.state.successMessage} />
          }
          {articles.length === 0 && !this.state.loading &&
            <Message type="info" description="No articles to fetch" />
          }
          {this.state.loading &&
            <div className="text-center"><div className="spinner-border text-primary" role="status"><span className="sr-only">Loading...</span></div></div>
          }
          {!this.state.loading &&
            <div className="table-responsive">
              <table className="table table-striped">
                <thead>
                  <tr>
                    <th>Title</th>
                    <th>Author</th>
                    <th>Created at</th>
                    <th>Updated at</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {articles.map((article, index) => {
                    return (
                      <tr key={index}>
                        <td>{ article.title }</td>
                        <td>{ article.author }</td>
                        <td>{ moment(article.createdAt).format("DD.MM.YYYY HH:mm") }</td>
                        <td>{ moment(article.updatedAt).format("DD.MM.YYYY HH:mm") }</td>
                        <td>
                          <Link to={`/admin/articles/edit/${article.uuid}`} type="button" className="btn btn-sm btn-primary mx-1">Edit</Link>
                          <button type="button" className="btn btn-sm btn-danger mx-1" onClick={() => { if (window.confirm("Are you sure you want to delete this article? Once it's deleted it's gone forever.")) this.deleteArticle(article.uuid) } }>Delete</button>
                        </td>
                      </tr>
                    )
                  })
                  }
                </tbody>
              </table>
            </div>
          }
        </div>
      </div>
    )
  }
}