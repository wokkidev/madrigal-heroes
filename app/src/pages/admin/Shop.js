import React, { Component } from "react";
import update from 'immutability-helper';

import { Message, Nav, Modal, Button, Alert } from 'rsuite';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { isLink } from '../../Helpers';

import AuthService from "../../services/auth.service";
import StoreService from "../../services/store.service";
import AdminService from '../../services/admin.service';

export default class AdminShop extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      currentUser: {},
      categories: [],
      items: [],
      errors: [],
      selectedCategory: 0,
      active: '0',
      editItem: {},
      itemsData: []
    }

    this.selectCategory = this.selectCategory.bind(this);
    this.selectItem = this.selectItem.bind(this);
    this.addItem = this.addItem.bind(this);
    this.close = this.close.bind(this);
    this.addCategory = this.addCategory.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
  }

  async componentDidMount() {
    this._isMounted = true;

    let currentUser = await AuthService.getCurrentUser();
    this.setState({
      currentUser: currentUser
    });

    StoreService.getCategories().then(
      categories => {
        if (this._isMounted) {
          this.setState({
            categories: categories.data.data
          });
        }
      },
      error => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        if (this._isMounted) this.setState(update(this.state, { errors: { $unshift: [resMessage] } }));
      }
    )

    StoreService.getItems().then(
      items => {
        if (this._isMounted) {
          this.setState({
            items: items.data.data,
            active: 0
          });
        }
      },
      error => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        if (this._isMounted) this.setState(update(this.state, { errors: { $unshift: [resMessage] } }));
      }
    )

    StoreService.getItemsData().then(
      items => {
        if (this._isMounted) {
          this.setState({
            itemsData: items.data.data
          })
        }
      },
      error => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        console.error(resMessage);
        if (this._isMounted) this.setState(update(this.state, { errors: { $unshift: [resMessage] } }));
      }
    )
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  selectCategory(id) {
    if (id === undefined) return;
    if (this._isMounted) {
      this.setState({
        selectedCategory: id,
        active: id
      });
    }
  }

  selectItem(id) {
    if (this._isMounted) {
      this.setState({
        editItemModal: true,
        editItem: this.state.items.find(item => item.id === id)
      });
    }
  }

  addItem() {
    if (this._isMounted) {
      this.setState({
        addItemModal: true
      });
    }
  }

  addCategory() {
    if (this._isMounted) {
      this.setState({
        addCategoryModal: true
      });
    }
  }

  deleteCategory(id) {
    AdminService.deleteCategory(id).then(
      category => {
        if (this._isMounted) {
          this.setState({
            categories: this.state.categories.filter(c => c.id !== category.data.data.id),
            active: 0
          });
        }
      },
      error => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        this.setState(update(this.state, { errors: { $unshift: [resMessage] } }));
      }
    )
  }

  deleteItem() {
    AdminService.deleteItem(this.state.editItem.id).then(
      item => {
        if (this._isMounted) {
          this.setState({
            items: this.state.items.filter(c => c.id !== item.data.data.id),
            editItemModal: false
          });
          Alert.success('Item deleted', 5000);
        }
      },
      error => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        if (this._isMounted) this.setState(update(this.state, { errors: { $unshift: [resMessage] } }));
      }
    )
  }

  close() {
    if (this._isMounted) {
      this.setState({
        addItemModal: false,
        editItemModal: false,
        displaySuccess: false,
        addCategoryModal: false,
        successMessage: ''
      });
    }
  }

  autofillItem(values, setFieldValue) {
    let { name } = values;
    const items = this.state.itemsData;
    let item;

    if (!isNaN(parseInt(name))) {
      name = parseInt(name);
      item = items.find(item => item.index === name);
    }
    else {
      item = items.find(item => item.szName.toLowerCase() === name.toLowerCase());
    }
    
    if (item) {
      setFieldValue('name', item.szName, false);
      setFieldValue('image', item.szIcon.replace('.dds', '.png'), false);
      setFieldValue('itemId', item.index, false);
      setFieldValue('description', item.szCommand, false);
      Alert.success('Item found. Fields set', 5000);
    }
    else {
      Alert.error('No item found with given name', 5000);
    }
  }

  render() {
    const { categories, items, errors, active, selectedCategory } = this.state;

    const CustomNav = ({ active, onSelect, ...props }) => {
      return (
        <Nav {...props} vertical activeKey={parseInt(active)} onSelect={onSelect}>
          <Nav.Item eventKey={0} onClick={() => this.selectCategory(0)}>All</Nav.Item>
          {categories.map((category, index) => (
            <Nav.Item key={index} eventKey={category.id}>{ category.name } <i className="fas fa-trash" onClick={() => { if (window.confirm("Are you sure you want to delete this category? If this category has items in it, it cannot be deleted.")) this.deleteCategory(category.id) }}></i></Nav.Item>
          ))}
          <Nav.Item onClick={this.addCategory}><i className="fas fa-plus"></i> Add new</Nav.Item>
        </Nav>
      );
    };

    return (
      <div className="AdminShop">
        <h3 className="py-3 px-3 mb-0 mh-header">Edit shop</h3>
        <div className="mh-text-container px-3 py-3">
          {errors.map((error, index) => (
            <Message key={index} type="error" description={error} className="mb-2" closable />
          ))}

          <div className="row">
            <div className="col-md-3">
              <CustomNav appearance="subtle" reversed active={active} onSelect={this.selectCategory} />
            </div>
            <div className="col-md-9">
              {selectedCategory !== 0 &&
                items.filter(item => item.categoryId === selectedCategory).map((item, index) => (
                  <div className="store-item" key={index} onClick={() => this.selectItem(item.id)} style={{ cursor: 'pointer' }}>
                    <img src={ isLink(item.image) } alt={ item.name } style={{ width: '100%', height: '100%' }} /><br/>
                    { item.name }
                  </div>
                ))
              }
              {selectedCategory !== 0 &&
                <div className="store-item" onClick={this.addItem} style={{ cursor: 'pointer' }}>
                  <i className="fas fa-plus" style={{ fontSize: '60px' }}></i>
                </div>
              }
              {selectedCategory === 0 &&
                items.map((item, index) => (
                  <div className="store-item" key={index} onClick={() => this.selectItem(item.id)} style={{ cursor: 'pointer' }}>
                    <img src={ isLink(item.image) } alt={ item.name } style={{ width: '100%', height: '100%' }} /><br/>
                    { item.name }
                  </div>
                ))
              }
            </div>
          </div>
        </div>

        <Modal show={this.state.addCategoryModal} onHide={this.close}>
          <Modal.Header>
            <Modal.Title>Add category</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.displaySuccess &&
              <Message type="success" description={this.state.successMessage} />
            }
            <Formik
              initialValues={{
                name: ''
              }}
              validationSchema={Yup.object().shape({
                name: Yup.string().required('Name is required')
              })}
              onSubmit={({ name }, { setStatus, setSubmitting }) => {
                setStatus();
                AdminService.addCategory(name).then(
                  category => {
                    if (category) {
                      if (this._isMounted) {
                        this.setState({
                          displaySuccess: true,
                          successMessage: 'Category added'
                        });
                        this.setState(update(this.state, { categories: { $push: [category.data.data] } }));
                      }
                    }
                    setSubmitting(false);
                  },
                  error => {
                    const resMessage =
                      (error.response &&
                        error.response.data &&
                        error.response.data.message) ||
                      error.message ||
                      error.toString();
                    setStatus(resMessage);
                    setSubmitting(false);
                  }
                )
              }}
            >
              {({ errors, status, touched, isSubmitting }) => (
                <Form>
                  {status &&
                    <Message type="error" description={status} />
                  }
                  <div className="form-group">
                    <label htmlFor="name">Name</label>
                    <Field name="name" type="text" className={'form-control' + (errors.name && touched.name ? ' is-invalid' : '')} />
                    <ErrorMessage name="name" component="div" className="invalid-feedback" />
                  </div>
                  <div className="form-group">
                    <button type="submit" className="btn btn-primary" disabled={isSubmitting}>Add category</button>
                    {isSubmitting &&
                      <div className="spinner-border text-primary" role="status"><span className="sr-only">Loading...</span></div>
                    }
                  </div>
                </Form>
              )}
            </Formik>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.close} appearance="subtle">Cancel</Button>
          </Modal.Footer>
        </Modal>

        <Modal show={this.state.editItemModal} onHide={this.close}>
          <Modal.Header>
            <Modal.Title>{ this.state.editItem.name }</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.displaySuccess &&
              <Message type="success" description={this.state.successMessage} />
            }
            <Formik
              initialValues={{
                name: this.state.editItem.name,
                image: this.state.editItem.image,
                price: this.state.editItem.price,
                salePrice: this.state.editItem.salePrice,
                isSale: this.state.editItem.isSale,
                isVote: this.state.editItem.isVote,
                itemId: this.state.editItem.itemId,
                quantity: this.state.editItem.quantity,
                description: this.state.editItem.description,
                categoryId: this.state.editItem.categoryId
              }}
              validationSchema={Yup.object().shape({
                name: Yup.string().required('Name is required'),
                image: Yup.string().required('Image is required'),
                price: Yup.number().positive().required('Price is required'),
                salePrice: Yup.number().positive(),
                itemId: Yup.string().required('Item ID is required'),
                quantity: Yup.number().positive().required('Quantity is required'),
                description: Yup.string().required('Description is required')
              })}
              onSubmit={({ name, image, price, salePrice, isSale, isVote, itemId, description, categoryId, quantity }, { setStatus, setSubmitting }) => {
                setStatus();
                AdminService.updateItem(this.state.editItem.id, name, image, price, salePrice, isSale, isVote, itemId, description, parseInt(categoryId), quantity).then(
                  item => {
                    if (this._isMounted) {
                      this.setState({
                        displaySuccess: true,
                        successMessage: 'Item updated'
                      });
                      const itemIndex = this.state.items.findIndex(x => x.id === this.state.editItem.id);
                      this.setState(update(this.state, { items: { [itemIndex]: { $set: item.data.data } } }));
                    }
                    setSubmitting(false);
                  },
                  error => {
                    const resMessage =
                      (error.response &&
                        error.response.data &&
                        error.response.data.message) ||
                      error.message ||
                      error.toString();
                    setStatus(resMessage);
                    setSubmitting(false);
                  }
                )
              }}
            >
              {({ errors, status, touched, isSubmitting, values, setFieldValue }) => (
                <Form>
                  {status &&
                    <Message type="error" description={status} />
                  }
                  <div className="form-group">
                    <label htmlFor="name">Name</label>
                    <div className="input-group">
                      <Field name="name" type="text" className={'form-control' + (errors.name && touched.name ? ' is-invalid' : '')} />
                      <div className="input-group-append">
                        <button type="button" className="btn btn-primary" onClick={() => this.autofillItem(values, setFieldValue)}>Autofill</button>
                      </div>
                    </div>
                    <ErrorMessage name="name" component="div" className="invalid-feedback" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="image">Image</label>
                    <Field name="image" type="text" className={'form-control' + (errors.image && touched.image ? ' is-invalid' : '')} />
                    <ErrorMessage name="image" component="div" className="invalid-feedback" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="price">Price</label>
                    <Field name="price" type="number" className={'form-control' + (errors.price && touched.price ? ' is-invalid' : '')} />
                    <ErrorMessage name="price" component="div" className="invalid-feedback" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="salePrice">Sale price</label>
                    <Field name="salePrice" type="number" className={'form-control' + (errors.salePrice && touched.salePrice ? ' is-invalid' : '')} />
                    <ErrorMessage name="salePrice" component="div" className="invalid-feedback" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="isSale">Is on sale?</label><br/>
                    <Field id="isSale" name="isSale" className="form-control">{({field, form, meta}) => <input type="checkbox" checked={field.value} {...field} />}</Field>
                    <ErrorMessage name="isSale" component="div" className="invalid-feedback" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="isVote">Is vote item?</label><br/>
                    <Field id="isVote" name="isVote" className="form-control">{({field, form, meta}) => <input type="checkbox" checked={field.value} {...field} />}</Field>
                    <ErrorMessage name="isVote" component="div" className="invalid-feedback" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="itemId">Item ID (Can be multiple separated by commas)</label>
                    <Field name="itemId" type="text" className={'form-control' + (errors.itemId && touched.itemId ? ' is-invalid' : '')} />
                    <ErrorMessage name="itemId" component="div" className="invalid-feedback" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="quantity">Quantity</label>
                    <Field name="quantity" type="number" className={'form-control' + (errors.quantity && touched.quantity ? ' is-invalid' : '')} />
                    <ErrorMessage name="quantity" component="div" className="invalid-feedback" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="description">Description</label>
                    <Field name="description" as="textarea" rows="5" className={'form-control' + (errors.description && touched.description ? ' is-invalid' : '')} />
                    <ErrorMessage name="description" component="div" className="invalid-feedback" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="categoryId">Category</label>
                    <Field name="categoryId">
                      {({field, form, meta}) => <select value={field.value} {...field} className="form-control">
                        {categories.map((category, index) => (
                          <option value={category.id} key={index}>{category.name}</option>
                        ))}
                      </select>}
                    </Field>
                  </div>
                  <div className="form-group">
                    <button type="submit" className="btn btn-primary" disabled={isSubmitting}>Edit item</button>
                    <button type="button" className="btn btn-danger ml-2" onClick={() => { if(window.confirm('Are you sure you want to delete this item?')) return this.deleteItem() }}>Delete item</button>
                    {isSubmitting &&
                      <div className="spinner-border text-primary" role="status"><span className="sr-only">Loading...</span></div>
                    }
                  </div>
                </Form>
              )}
            </Formik>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.close} appearance="subtle">Cancel</Button>
          </Modal.Footer>
        </Modal>

        <Modal show={this.state.addItemModal} onHide={this.close}>
          <Modal.Header>
            <Modal.Title>Add item</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.displaySuccess &&
              <Message type="success" description={this.state.successMessage} />
            }
            <Formik
              initialValues={{
                name: '',
                image: '',
                price: 1,
                salePrice: 1,
                isSale: false,
                isVote: false,
                itemId: '0',
                quantity: 1,
                description: ''
              }}
              validationSchema={Yup.object().shape({
                name: Yup.string().required('Name is required'),
                image: Yup.string().required('Image is required'),
                price: Yup.number().positive().required('Price is required'),
                salePrice: Yup.number().positive(),
                itemId: Yup.string().required('Item ID is required'),
                quantity: Yup.number().positive().required('Quantity is required'),
                description: Yup.string().required('Description is required')
              })}
              onSubmit={({ name, image, price, salePrice, isSale, isVote, itemId, description, quantity }, { setStatus, setSubmitting }) => {
                setStatus();
                AdminService.addItem(name, image, price, salePrice, isSale, isVote, itemId, description, selectedCategory, quantity).then(
                  item => {
                    if (item) {
                      if (this._isMounted) {
                        this.setState({
                          displaySuccess: true,
                          successMessage: 'Item added'
                        });
                        this.setState(update(this.state, { items: { $push: [item.data.data] } }));
                      }
                    }
                    setSubmitting(false);
                  },
                  error => {
                    const resMessage =
                      (error.response &&
                        error.response.data &&
                        error.response.data.message) ||
                      error.message ||
                      error.toString();
                    setStatus(resMessage);
                    setSubmitting(false);
                  }
                )
              }}
            >
              {({ errors, status, touched, isSubmitting, values, setFieldValue }) => (
                <Form>
                  {status &&
                    <Message type="error" description={status} />
                  }
                  <div className="form-group">
                    <label htmlFor="name">Name</label>
                    <div className="input-group">
                      <Field name="name" type="text" className={'form-control' + (errors.name && touched.name ? ' is-invalid' : '')} />
                      <div className="input-group-append">
                        <button type="button" className="btn btn-primary" onClick={() => this.autofillItem(values, setFieldValue)}>Autofill</button>
                      </div>
                    </div>
                    <ErrorMessage name="name" component="div" className="invalid-feedback" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="image">Image</label>
                    <Field name="image" type="text" className={'form-control' + (errors.image && touched.image ? ' is-invalid' : '')} />
                    <ErrorMessage name="image" component="div" className="invalid-feedback" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="price">Price</label>
                    <Field name="price" type="number" className={'form-control' + (errors.price && touched.price ? ' is-invalid' : '')} />
                    <ErrorMessage name="price" component="div" className="invalid-feedback" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="salePrice">Sale price</label>
                    <Field name="salePrice" type="number" className={'form-control' + (errors.salePrice && touched.salePrice ? ' is-invalid' : '')} />
                    <ErrorMessage name="salePrice" component="div" className="invalid-feedback" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="isSale">Is on sale?</label><br/>
                    <Field id="isSale" name="isSale" className="form-control">{({field, form, meta}) => <input type="checkbox" checked={field.value} {...field} />}</Field>
                    <ErrorMessage name="isSale" component="div" className="invalid-feedback" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="isVote">Is vote item?</label><br/>
                    <Field id="isVote" name="isVote" className="form-control">{({field, form, meta}) => <input type="checkbox" checked={field.value} {...field} />}</Field>
                    <ErrorMessage name="isVote" component="div" className="invalid-feedback" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="itemId">Item ID (Can be multiple separated by commas)</label>
                    <Field name="itemId" type="text" className={'form-control' + (errors.itemId && touched.itemId ? ' is-invalid' : '')} />
                    <ErrorMessage name="itemId" component="div" className="invalid-feedback" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="quantity">Quantity</label>
                    <Field name="quantity" type="number" className={'form-control' + (errors.quantity && touched.quantity ? ' is-invalid' : '')} />
                    <ErrorMessage name="quantity" component="div" className="invalid-feedback" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="description">Description</label>
                    <Field name="description" as="textarea" rows="5" className={'form-control' + (errors.description && touched.description ? ' is-invalid' : '')} />
                    <ErrorMessage name="description" component="div" className="invalid-feedback" />
                  </div>
                  <div className="form-group">
                    <button type="submit" className="btn btn-primary" disabled={isSubmitting}>Add item</button>
                    {isSubmitting &&
                      <div className="spinner-border text-primary" role="status"><span className="sr-only">Loading...</span></div>
                    }
                  </div>
                </Form>
              )}
            </Formik>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.close} appearance="subtle">Cancel</Button>
          </Modal.Footer>
        </Modal>
      </div>
    )
  }
}