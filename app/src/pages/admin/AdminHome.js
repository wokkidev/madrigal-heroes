import React, { Component } from "react";
import { Link } from "react-router-dom";

import AuthService from "../../services/auth.service";

export default class Admin extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      currentUser: {},
      displaySuccess: false,
      successMessage: '',
      mounted: false
    };
  }

  async componentDidMount() {
    this._isMounted = true;

    let currentUser = await AuthService.getCurrentUser();
    this.setState({
      currentUser: currentUser
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    return (
      <div className="Admin">
        <h3 className="py-3 px-3 mb-0 mh-header">Admin</h3>
        <div className="mh-text-container px-3 py-3">
          <nav className="nav nav-pills flex-column">
            <Link to="/admin/articles" className="nav-link">Articles</Link>
            <Link to="/admin/article/new" className="nav-link">New article</Link>
            <Link to="/admin/setting" className="nav-link">Settings</Link>
            <Link to="/admin/shop" className="nav-link">Shop</Link>
          </nav>
        </div>
      </div>
    )
  }
}