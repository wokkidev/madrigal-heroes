import React, { Component } from "react";
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import AuthService from "../../services/auth.service";
import AdminService from '../../services/admin.service';
import ArticleService from '../../services/article.service';

export default class CreateArticle extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      currentUser: {},
      displaySuccess: false,
      successMessage: '',
      article: {},
      loading: true
    };
  }

  async componentDidMount() {
    this._isMounted = true;

    let currentUser = await AuthService.getCurrentUser();
    this.setState({
      currentUser: currentUser
    });

    const uuid = this.props.match.params.uuid;

    if (!uuid && this._isMounted) {
      this.setState({
        errorMessage: 'Invalid uuid. Make sure your link is valid.',
        displayError: true
      })
    }
    else {
      ArticleService.getArticle(uuid).then(
        article => {
          if (this._isMounted) {
            this.setState({
              article: article.data,
              loading: false
            });
          }
        },
        error => {
          console.error(error);
          if (this._isMounted) {
            this.setState({
              displayError: true,
              errorMessage: 'An error occurred while attempting to load news articles. Try again later.'
            });
          }
        }
      )
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    const article = this.state.article;

    return (
      <div className="CreateArticle">
        <h3 className="py-3 px-3 mb-0 mh-header">Edit article</h3>
        <div className="mh-text-container px-3 py-3">
          {this.state.displaySuccess &&
            <div className={'alert alert-success'} dangerouslySetInnerHTML={{ __html:  this.state.successMessage }}></div>
          }
          {this.state.loading &&
            <div className="text-center"><div className="spinner-border text-primary" role="status"><span className="sr-only">Loading...</span></div></div>
          }
          {!this.state.loading &&
          <Formik
            initialValues={{
              title: article.title,
              message: article.message
            }}
            validationSchema={Yup.object().shape({
              title: Yup.string().required('Title is required'),
              message: Yup.string().required('Message is required')
            })}
            onSubmit={({ title, message }, { setStatus, setSubmitting }) => {
              setStatus();
              AdminService.updateArticle(this.props.match.params.uuid, title, message).then(
                (article) => {
                  this.setState({
                    successMessage: `Article edited! <a href="/article/${article.data.uuid}" target="_blank">Link to article</a>`,
                    displaySuccess: true
                  });
                  setSubmitting(false);
                },
                error => {
                  const resMessage =
                    (error.response &&
                      error.response.data &&
                      error.response.data.message) ||
                    error.message ||
                    error.toString();
                  setStatus(resMessage);
                  setSubmitting(false);
                }
              )
            }}
          >
            {({ errors, status, touched, isSubmitting }) => (
              <Form>
                {status &&
                  <div className={'alert alert-danger'}>{ status }</div>
                }
                <div className="form-group">
                  <label htmlFor="title">Title</label>
                  <Field name="title" type="text" className={'form-control' + (errors.title && touched.title ? ' is-invalid' : '')} />
                  <ErrorMessage name="title" component="div" className="invalid-feedback" />
                </div>
                <div className="form-group">
                  <label htmlFor="message">Message</label>
                  <Field name="message" as="textarea" rows="20" className={'form-control' + (errors.message && touched.message ? ' is-invalid' : '')} />
                  <ErrorMessage name="message" component="div" className="invalid-feedback" />
                </div>
                <div className="form-group">
                <button type="submit" className="btn btn-primary" disabled={isSubmitting}>Edit article</button>
                  {isSubmitting &&
                    <div className="spinner-border text-primary" role="status"><span className="sr-only">Loading...</span></div>
                  }
                </div>
              </Form>
            )}
          </Formik>
          }
        </div>
      </div>
    )
  }
}