import React from 'react';
import update from 'immutability-helper';
import moment from 'moment';

import AdminService from '../../services/admin.service';

import TablePagination from "rsuite/lib/Table/TablePagination";

import { Table } from 'rsuite';

const { Column, HeaderCell, Cell } = Table;

export default class AdminLogs extends React.Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      logs: [],
      errors: [],
      displayLength: 10,
      loading: true,
      page: 1,
      type: 'donation',
      data: []
    }

    this.handleChangePage = this.handleChangePage.bind(this);
    this.handleChangeLength = this.handleChangeLength.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;

    this.getLogs();
  }

  changeLogType(type) {
    if (type === this.state.type) {
      console.log('Same type');
      return;
    }

    this.setState({
      type: type,
      loading: true
    });
    this.getLogs(type);
  }

  getLogs(type) {
    type = type ? type : this.state.type;

    AdminService.getAllLogs(type).then(
      logs => {
        logs.data.data.forEach(log => { log.createdAt = moment(log.createdAt).format("MM/DD/YYYY HH:mm"); return log; });
        if (this._isMounted) {
          this.setState({
            logs: logs.data.data,
            loading: false
          });
          this.getData();
        }
      },
      error => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        if (this._isMounted) {
          this.setState(update(this.state, { errors: { $unshift: [resMessage] } }));
          this.setState({
            loading: false
          });
        }
      }
    )
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  handleChangePage(dataKey) {
    if (this._isMounted) {
      this.setState({
        page: dataKey
      });
    }
  }

  handleChangeLength(dataKey) {
    if (this._isMounted) {
      this.setState({
        page: 1,
        displayLength: dataKey
      });
    }
  }

  getData() {
    const { displayLength, page, logs } = this.state;

    return logs.filter((v, i) => {
      const start = displayLength * (page - 1);
      const end = start + displayLength;
      return i >= start && i < end;
    });
  }

  render() {
    const data = this.getData();
    const { logs, loading, page, displayLength } = this.state;

    return (
      <div className="AdminLogs">
        <h3 className="py-3 px-3 mb-0 mh-header">Admin Logs</h3>
        <div className="mh-text-container px-3 py-3">
          <button type="button" className="btn btn-sm btn-primary mb-3 mr-1" onClick={() => this.changeLogType('donation')}>Donation logs</button>
          <button type="button" className="btn btn-sm btn-primary mb-3 mr-1" onClick={() => this.changeLogType('vote')}>Vote logs</button>
          <button type="button" className="btn btn-sm btn-primary mb-3 mr-1" onClick={() => this.changeLogType('purchase')}>Purchases</button>
          <button type="button" className="btn btn-sm btn-primary mb-3" onClick={() => this.changeLogType('admin')}>Admin</button>

          <Table height={displayLength === 10 ? 500 : 1000} data={data} loading={loading} bordered={false}>
            <Column width={150}>
              <HeaderCell>Date</HeaderCell>
              <Cell dataKey="createdAt" />
            </Column>

            <Column width={125}>
              <HeaderCell>Account</HeaderCell>
              <Cell dataKey="account" />
            </Column>

            <Column width={500}>
              <HeaderCell>Message</HeaderCell>
              <Cell dataKey="message" />
            </Column>
          </Table>

          <TablePagination
            lengthMenu={[
              {
                value: 10,
                label: 10
              },
              {
                value: 20,
                label: 20
              }
            ]}
            activePage={page}
            displayLength={displayLength}
            total={logs.length}
            onChangePage={this.handleChangePage}
            onChangeLength={this.handleChangeLength}
          />
        </div>
      </div>
    )
  }
}