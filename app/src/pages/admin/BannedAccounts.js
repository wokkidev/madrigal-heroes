import React, { Component } from "react";
import { Alert, Message } from 'rsuite';

import AuthService from "../../services/auth.service";
import AdminService from '../../services/admin.service';

export default class BannedAccounts extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      currentUser: {},
      mounted: false,
      accounts: []
    };
  }

  async componentDidMount() {
    this._isMounted = true;

    let currentUser = await AuthService.getCurrentUser();
    this.setState({
      currentUser: currentUser
    });

    AdminService.getBannedAccounts().then(
      response => {
        if (this._isMounted) {
          this.setState({
            accounts: response.data.data
          });
        }
      },
      error => {
        console.error(error);
        Alert.success('Error when trying to fetch banned accounts. Error logged to console.', 5000);
      }
    )
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  macUnban(account) {
    AdminService.macUnbanAccount(account).then(
      data => {
        console.log(data);
        Alert.success(`Account unbanned!`, 5000);
      },
      error => {
        console.error(error);
        Alert.error(`Could not unban account. Error logged to console.`, 5000);
      }
    )
  }

  render() {
    const { accounts } = this.state;

    return (
      <div className="Banned-Accounts">
        <h3 className="py-3 px-3 mb-0 mh-header">Banned accounts</h3>
        <div className="mh-text-container px-3 py-3">
          {accounts.length === 0 &&
            <Message type="warning" description="No banned accounts found" />
          }
          <div className="table-responsive">
						<table className="table mt-3">
							<thead>
								<tr>
                  <th>Account</th>
                  <th>Mac address</th>
                  <th>Actions</th>
								</tr>
							</thead>
							<tbody>
                {accounts.map((account, index) => (
                  <tr key={index}>
                    <td>{ account.account }</td>
                    <td>{ account.macaddr }</td>
                    <td>
                      <button className="btn btn-sm btn-danger" onClick={() => { if(window.confirm('Are you sure you want to mac unban this account?')) this.macUnban(account.account); }}>Mac Unban</button>
                    </td>
                  </tr>
                ))}
							</tbody>
						</table>
					</div>
        </div>
      </div>
    )
  }
}