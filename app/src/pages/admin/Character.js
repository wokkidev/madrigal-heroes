import React from 'react';

import AuthService from "../../services/auth.service";
import UserService from "../../services/user.service";
import StoreService from "../../services/store.service";

import { Tooltip, Whisper, Message } from 'rsuite';

import { getElement } from '../../Helpers';

export default class Character extends React.Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      currentUser: {},
      character: {},
      inventory: [],
      items: [],
      search: ''
    }

    this.handleChange = this.handleChange.bind(this);
  }

  async componentDidMount() {
    this._isMounted = true;

    let currentUser = await AuthService.getCurrentUser();
    this.setState({
      currentUser: currentUser
    });

    const name = this.props.match.params.name;

    UserService.getCharacter(name).then(
      character => {
        if (this._isMounted) {
          let inv = character.data.data.inventory.mInventory.split('/').slice(0, -1);
          for (let i = 0; i < inv.length; i++) {
            inv[i] = inv[i].split(',');
          }

          this.setState({
            character: character.data.data,
            inventory: inv
          });
        }
      },
      error => {
        console.error(error);
        /*if (this._isMounted) {
          this.setState({
            error: 'Failed to load character',
            displayError: true
          });
        }*/
      }
    )

    StoreService.getItemsData().then(
      items => {
        if (this._isMounted) {
          this.setState({
            items: items.data.data
          });
        }
      },
      error => {
        console.error(error);
      }
    )
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  handleChange(e) {
    if (this._isMounted) {
      this.setState({
        search: e.target.value
      })
    }
  }
  render() {
    const { character, inventory, items, search } = this.state;

    return (
      <div className="character">
        <h3 className="py-3 px-3 mb-0 mh-header">{ character.mSzName || '' }'s inventory</h3>
        <div className="mh-text-container px-3 py-3">
          { character.multiServer === 1 &&
            <Message className="mb-2" type="warning" description="This character is currently online. The inventory displayed is what the character had in their inventory when they last logged out." />
          }
          <p className="mb-2">Penya: { character.mDwGold ? character.mDwGold.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : '' }</p>

          <input type="text" className="form-control mb-2" id="mh-search" name="mh-search" onChange={this.handleChange} placeholder="Search for item..." />

          {inventory.map((item, index) => {
            // 1 = id
            // 5 = quantity
            // 7 = pos
            // 12 = upgrade +
            // 13 = element
            // 14 = element value
            //item = item.split(',');
            const itemData = items.find(itm => itm.index === parseInt(item[1]));
            return (
              <div key={index} className="d-inline mx-1 my-1" onClick={() => console.log(item)}>
                {itemData && itemData.szName.includes(search) &&
                  <Whisper placement="top" trigger="hover" speaker={(<Tooltip>{ itemData.szName } { item[12] > 0 && '+' + item[12] } { parseInt(item[13]) !== 0 && (<span><br/>{ getElement(parseInt(item[13])) + '+' + item[14] }</span>) }</Tooltip>)}>
                    <div className="d-inline position-relative">
                      <img src={process.env.PUBLIC_URL + '/items/' + itemData.szIcon.replace('.dds', '.png')} alt={itemData.szName} className="img-thumbnail" />
                      {item[5] > 1 &&
                        <span className="position-absolute" style={{ bottom: '-10px', right: '5px', color: '#0099ff', fontWeight: 'bold', textShadow: '-1px 1px 2px #000, 1px 1px 2px #000, 1px -1px 2px #000, -1px -1px 2px #000' }}>{ item[5] }</span>
                      }
                    </div>
                  </Whisper>
                }
              </div>
            )
          })}
        </div>
      </div>
    )
  }
}