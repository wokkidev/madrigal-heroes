import React from "react";

import SocketContext from '../socketContext';

import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { Steps, Modal, Button, Message } from 'rsuite';
import AuthService from "../services/auth.service";

class Donate extends React.Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      amount: 0,
      account: '',
      displayCustom: true,
      currentUser: {},
      step: 0,
      showTermsModal: false,
      displayError: '',
      displaySuccess: ''
    }

    this.handleCancel = this.handleCancel.bind(this);
    this.beginDonate = this.beginDonate.bind(this);
    this.close = this.close.bind(this);
    this.open = this.open.bind(this);
  }

  async componentDidMount() {
    this._isMounted = true;

    let currentUser = await AuthService.getCurrentUser();
    this.setState({
      currentUser: currentUser
    });

    this.props.socket
      .on('donateSuccess', (donate) => {
        if (this._isMounted) {
          this.setState({
            step: 3,
            displaySuccess: donate.message
          });
        }
      })
      .on('donateError', (error) => {
        console.error(error);
        this.setState({
          step: 2,
          displayError: error.message
        })
      });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  handleCancel() {
    if (this._isMounted) {
      this.setState({
        step: 0,
      });
    }
  }

  beginDonate() {
    let url = 'https://www.paypal.com/cgi-bin/webscr?cmd=_donations';
    const values = {
      business: 'heroesmadrigal@gmail.com',
      item: 'Madrigal Heroes',
      currency_code: 'EUR',
      lc: 'US',
      country: 'US',
      amount: this.state.amount,
      custom: this.state.account
    };

    Object.keys(values).map(key => {
      url = `${url}&${key}=${values[key]}`;
      return url;
    });

    console.log(url);

    window.open(
      url,
      'Donate',
      'scrollbars=no,resizable=yes,status=no,location=no,toolbar=no,menubar=no,width=469,height=775,left=100,top=100'
    )

    if (this._isMounted) {
      this.setState({
        step: 2
      });
    }
  }

  close() {
    this.setState({ show: false });
  }

  open() {
    this.setState({ show: true });
  }

  displayPoints = (amount) => {
    let points = 0;
    if (amount >= 25 && amount < 49) {
      points = (amount * 10) * 1.1;
    }
    else if (amount >= 50 && amount < 99) {
      points = (amount * 10) * 1.15;
    }
    else if (amount >= 100) {
      points = (amount * 10) * 1.2;
    }
    else {
      points = amount * 10;
    }

    return parseInt(points);
  }

  render() {
    const { step, displaySuccess, displayError } = this.state;

    return (
      <div className="Donate">
      <h3 className="py-3 px-3 mb-0 mh-header">Donate</h3>
      <div className="mh-text-container px-3 py-3">
        <Steps current={this.state.step} currentStatus={displayError ? 'error' : 'process'}>
          <Steps.Item title="Details" />
          <Steps.Item title="Donate" />
          <Steps.Item title="Complete" />
        </Steps>
        <hr />

        {displayError &&
          <Message type="error" description={displayError} />
        }

        {step === 0 &&
          <Formik
            initialValues={{
              account: this.state.currentUser ? this.state.currentUser.account : '',
              amount: 5
            }}
            validationSchema={Yup.object().shape({
              account: Yup.string().min(4).max(12).required('Account name is required'),
              amount: Yup.number().required('Amount is required')
            })}
            onSubmit={({ account, amount }, { setStatus, setSubmitting }) => {
              setStatus();
              setSubmitting(false);
              if (this._isMounted) {
                this.setState({
                  account: account,
                  amount: amount,
                  step: 1
                });
              }
            }}
          >
            {({ errors, status, touched, isSubmitting }) => (
              <Form>
                {status &&
                  <div className={'alert alert-danger'}>{ status }</div>
                }
                <div className="form-group">
                  <label htmlFor="account">Account</label>
                  <Field name="account" type="text" className={'form-control' + (errors.account && touched.account ? ' is-invalid' : '')} />
                  <ErrorMessage name="account" component="div" className="invalid-feedback" />
                </div>
                <div className="form-group">
                  <label htmlFor="amount">Amount</label>
                  <Field as="select" size="6" name="amount" className={'form-control' + (errors.amount && touched.amount ? ' is-invalid' : '')}>
                    <option value="5">$5 - 50 Madrigal Points</option>
                    <option value="10">$10 - 100 Madrigal Points</option>
                    <option value="25">$25 - 275 Madrigal Points (10% increase)</option>
                    <option value="50">$50 - 575 Madrigal Points (15% increase)</option>
                    <option value="100">$100 - 1200 Madrigal Points (20% increase)</option>
                  </Field>
                  <ErrorMessage name="amount" component="div" className="invalid-feedback" />
                </div>
                <div className="form-group">
                  <button type="submit" className="btn btn-primary" disabled={isSubmitting}>Continue</button>
                  <button type="button" className="btn btn-link float-right" onClick={this.open}>By donating you agree to our Terms and Conditions</button>
                  {isSubmitting &&
                    <div className="spinner-border text-primary" role="status"><span className="sr-only">Loading...</span></div>
                  }
                </div>
              </Form>
            )}
          </Formik>
        }

        {step === 1 &&
          <div>
            <div className="row">
              <div className="col-md-3">
                <strong>Account:</strong>
              </div>
              <div className="col-md-9">
                {this.state.account}
              </div>
            </div>
            <div className="row">
              <div className="col-md-3">
                <strong>Amount:</strong>
              </div>
              <div className="col-md-9">
                ${this.state.amount}
              </div>
            </div>
            <div className="row">
              <div className="col-md-3">
                <strong>Points:</strong>
              </div>
              <div className="col-md-9">
                {this.displayPoints(this.state.amount)} Madrigal Points
              </div>
            </div>
            <img src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" alt="Donate" onClick={this.beginDonate} style={{ cursor: 'pointer' }} />
            <br/>
            <button className="btn btn-primary mt-3" onClick={this.handleCancel}>Cancel</button>
            <button type="button" className="btn btn-link float-right" onClick={this.open}>By donating you agree to our Terms and Conditions</button>
          </div>
        }

        {step === 2 &&
          <div>
            <Message type="success" description={<div><div className="spinner-border spinner-border-sm" role="status"><span className="sr-only">Loading...</span></div> Waiting for PayPal response. This usually takes from few seconds up to 2 minutes.</div>} />
            <button className="btn btn-primary mt-3" onClick={this.handleCancel}>Cancel</button>
            <button type="button" className="btn btn-link float-right" onClick={this.open}>By donating you agree to our Terms and Conditions</button>
          </div>
        }

        {step === 3 &&
          <div>
            <Message type="success" description={displaySuccess} />
          </div>
        }
      </div>

      <Modal show={this.state.show} onHide={this.close}>
        <Modal.Header>
          <Modal.Title>Terms and Conditions</Modal.Title>
        </Modal.Header>
        <Modal.Body>
					<ol>
						<li>These terms and conditions govern your use at Madrigal Heroes, by using any of the services Madrigal Heroes is providing, you agree with these terms completely. By disagreeing with these Terms you may not use any of the services stated below.</li>
						<li>Refund policy. Madrigal Heroes will not provide refunds under any circumstances. If you unexpectedly did not receive your donation award, write a new message to our support channel in discord for us to review the case.</li>
						<li>When you donate to Madrigal Heroes, it is understood that you're actually "donating" at your own risk. Donation items are exclusively rewards for your consideration into donating to Madrigal Heroes. You're not paying for any service, you're donating to keep our servers up.</li>
						<li>Any received service(s) and/or goods cannot be exchanged for other services and/or goods, as well as money. If you are not satisfied with our services and/or goods, you can report this to our discord.</li>
						<li>By partaking in any of Madrigal Heroes services, you agree to indemnify and hold us harmless, as well as any parties that are related to Madrigal Heroes. Madrigal Heroes will not be liable for any addition matters relating to its services, and/or goods.</li>
						<li>If you have read, understood, and agreed to these terms and conditions, you may precede with the services and/or goods the Madrigal Heroes offers. If not, you will be asked to leave immediately, and terminate all affiliations with Madrigal Heroes and it’s partners. Not that all purchases and actions made relating Madrigal Heroes are out of free will. Madrigal Heroes is not responsible for any actions done under your discretion.</li>
					</ol>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.close} appearance="primary">
            Continue
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
    )
  }
}

const DonateWithSocket = props => (
  <SocketContext.Consumer>
    {socket => <Donate {...props} socket={socket} />}
  </SocketContext.Consumer>
);

export default DonateWithSocket;