import React from 'react';
import { Alert, Message } from 'rsuite';

import SettingService from '../services/setting.service';

export default class Discord extends React.Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      discordLink: '',
      loading: true,
      displayError: false,
      errorMessage: ''
    }
  }

  componentDidMount() {
    this._isMounted = true;
		SettingService.getSettings().then(
      settings => {
				if (this._isMounted) {
					this.setState({
						loading: false
          });
          settings.data.data.find(setting => setting.name === 'discord') ?
            window.location = settings.data.data.find(setting => setting.name === 'discord').value :
            this.setState({ displayError: true, errorMessage: 'Sorry, we were unable to redirect you to our discord invite link at this time.' })
				}
      },
      error => {
				Alert.error(error, 5000);
      }
    )
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    const { loading, displayError, errorMessage } = this.state;

    return (
      <div className="Discord">
        <h3 className="py-3 px-3 mb-0 mh-header">Discord</h3>
        <div className="mh-text-container px-3 py-3">
          {loading &&
            <Message type="success" description={<div><div className="spinner-border spinner-border-sm text-primary mr-2" role="status"><span className="sr-only">Loading...</span></div> We're redirecting you to our discord invite link</div>} />
          }
          {displayError &&
            <Message type="error" description={errorMessage} />
          }
        </div>
      </div>
    )
  }
}