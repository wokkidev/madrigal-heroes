import React from "react";
import update from 'immutability-helper';
import queryString from 'query-string';
import Cookies from "js-cookie";

import { isLink } from '../../Helpers';

import Item from './Item';

import { Message, Nav, Alert, Panel, Row, Col, Tag } from 'rsuite';

import Transactions from '../../components/Transactions.component';
import Login from '../Login';

import AuthService from "../../services/auth.service";
import StoreService from "../../services/store.service";
import UserService from "../../services/user.service";

export default class Store extends React.Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      currentUser: {},
      categories: [],
      items: [],
      errors: [],
      messages: [],
      selectedCategory: 0,
      viewItem: {},
      characters: [],
      character: '',
      loading: true,
      active: '0',
      amount: 1,
      queryValues: {},
      purchasing: false,
      keyword: '',
      authenticating: false,
      popular: []
    }

    this.resetItem = this.resetItem.bind(this);
    this.purchaseItem = this.purchaseItem.bind(this);
    this.changeCharacter = this.changeCharacter.bind(this);
    this.selectCategory = this.selectCategory.bind(this);
    this.changeAmount = this.changeAmount.bind(this);
    this.changeSearch = this.changeSearch.bind(this);
  }

  async componentDidMount() {
    this._isMounted = true;
    let currentUser = await AuthService.getCurrentUser();
    this.setState({
      currentUser: currentUser
    });
    // Check if query includes user data. Use that data to authenticate user.
    const values = queryString.parse(this.props.location.search);
    if (this._isMounted) {
      this.setState({
        queryValues: values
      });
    }
    // We will not use in game browser, so having these is useless
    /*if (!this.state.currentUser) {
      const { user_id, m_idPlayer, server_index, md5, check } = values;
      if (user_id && m_idPlayer && server_index && md5 && check) {
        this.setState({ authenticating: true });
        console.log('In game login');
        const md5Compare = crypto.createHash('md5').update(user_id + m_idPlayer + server_index).digest("hex");
        if (md5Compare === md5) {
          console.log('Match');
          AuthService.gameLogin(user_id, m_idPlayer, server_index, md5, check).then(
            () => {
              if (this._isMounted) {
                this.setState({
                  currentUser: AuthService.getCurrentUser()
                });
                window.location.reload();
              }
            },
            error => {
                const resMessage =
                (error.response &&
                  error.response.data &&
                  error.response.data.message) ||
                error.message ||
                error.toString();
              if (this._isMounted) this.setState(update(this.state, { errors: { $unshift: [resMessage] } }));
            }
          )
        }
      }
    }*/

    if (this.state.currentUser) {
      UserService.getCharacters(this.state.currentUser.account).then(
        characters => {
          if (characters.data.data.length > 0 && this._isMounted) {
            this.setState({
              characters: characters.data.data.sort((a, b) => a.mIdPlayer - b.mIdPlayer),
              character: values.m_idPlayer ? values.m_idPlayer : characters.data.data[0].mIdPlayer,
              loadingCharacters: false,
            });
          }
          else {
            if (this._isMounted) {
              this.setState({
                characters: [],
              });
            }
          }
        },
        error => {
          console.error(error);
          if (this._isMounted) {
            this.setState({
              error: 'Failed to load characters'
            });
          }
        }
      )
  
      StoreService.getCategories().then(
        categories => {
          if (this._isMounted) {
            this.setState({
              categories: categories.data.data
            });
          }
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();
          if (this._isMounted) this.setState(update(this.state, { errors: { $unshift: [resMessage] } }));
        }
      )
  
      StoreService.getItems().then(
        items => {
          if (this._isMounted) {
            this.setState({
              items: items.data.data,
              popular: items.data.data.sort((a,b) => a.purchases - b.purchases).slice(0, 4),
              loading: false
            });
          }
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();
          if (this._isMounted) this.setState(update(this.state, { errors: { $unshift: [resMessage] } }));
        }
      )
    }
    else {
      if (this._isMounted) {
        this.setState({
          loading: false
        });
      }
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  selectCategory(id) {
    if (this._isMounted) {
      this.setState({
        selectedCategory: id,
        viewItem: {},
        active: id
      });
    }
  }

  selectItem = (id) => {
    const { items } = this.state;

    if (this._isMounted) {
      this.setState({
        viewItem: items.find(item => item.id === id)
      });
    }
  }

  resetItem() {
    if (this._isMounted) {
      this.setState({
        viewItem: {},
        amount: 1
      });
    }
  }

  purchaseItem() {
    const item = this.state.viewItem;
    const amount = this.state.amount;
    const character = this.state.characters.find(character => character.mIdPlayer === this.state.character);

    if (!character) {
      return Alert.error('Cannot send item for a non-existing character', 5000);
    }

    if (this._isMounted) {
      this.setState({
        purchasing: true
      });
    }

    StoreService.purchaseItem(item.id, character.mIdPlayer, amount).then(
      response => {
        const { account, item, amount } = response.data.data;
        //this.setState(update(this.state, { messages: { $unshift: [`${item.name}x${amount} sent to character ${character.mSzName}. If you were logged in, please relog to receive your item.`] } }));
        let storedUser = JSON.parse(Cookies.get('user'));
        storedUser.cash = account.cash;
        storedUser.vote = account.vote;
        Cookies.set('user', JSON.stringify(storedUser));
        if (this._isMounted) {
          this.setState({
            purchasing: false,
            currentUser: storedUser
          });
        }
        Alert.success(`${item.name} x ${amount} sent to character ${character.mSzName}`, 5000)
      },
      error => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        console.error(resMessage);
        if (this._isMounted) {
          this.setState({
            purchasing: false
          });
        }
        Alert.error(resMessage, 5000);
      }
    )
  }

  changeCharacter(event) {
    if (this._isMounted) {
      this.setState({
        character: event.target.value
      });
    }
  }

  changeAmount(event) {
    if (this._isMounted) {
      this.setState({
        amount: event.target.value
      });
    }
  }

  changeSearch(event) {
    if (this._isMounted) {
      this.setState({
        keyword: event.target.value
      });
    }
  }

  getItems() {
    let items = this.state.items;
    const { selectedCategory, keyword } = this.state;
    if (selectedCategory !== 0) items = items.filter(item => item.categoryId === selectedCategory);
    if (keyword) items = items.filter(item => item.name.toLowerCase().includes(keyword.toLowerCase()));

    return items;
  }

  render() {
    const { popular, currentUser, categories, errors, messages, viewItem, characters, character, loading, active, amount, purchasing, queryValues, selectedCategory, authenticating } = this.state;
    const items = this.getItems();
    const CustomNav = ({ active, onSelect, ...props }) => {
      return (
        <Nav {...props} vertical activeKey={parseInt(active)} onSelect={onSelect}>
          <Nav.Item eventKey={0} onClick={() => this.selectCategory(0)}>All items</Nav.Item>
          {categories.map((category, index) => (
            <Nav.Item key={index} eventKey={category.id}>{ category.name }</Nav.Item>
          ))}
          <Nav.Item eventKey={998} onClick={() => this.selectCategory(998)}>On sale</Nav.Item>
          <Nav.Item eventKey={999} onClick={() => this.selectCategory(999)}>Transactions</Nav.Item>
        </Nav>
      );
    };

    return (
      <div className="Store">
        <h3 className="py-3 px-3 mb-0 mh-header">Madrigal Shop</h3>
        <div className="mh-text-container px-3 py-3">
          {Object.keys(queryValues).length > 0 &&
            <p><strong>Debug:</strong> { Object.keys(queryValues).map((string, index) => ( <span key={index}>{string} = {queryValues[string]}, </span> )) }</p>
          }

          {loading &&
            <div className="text-center"><div className="spinner-border text-primary" role="status"><span className="sr-only">Loading...</span></div></div>
          }

          {errors.map((error, index) => (
            <Message key={index} type="error" description={error} className="mb-2" closable />
          ))}

          {messages.map((message, index) => (
            <Message key={index} type="success" description={message} className="mb-2" closable />
          ))}

          {!currentUser && !authenticating &&
            <div>
              <Message className="mb-2" type="warning" description="Please login to use the store" />
              <Login />
            </div>
          }

          {!currentUser && authenticating &&
            <Message className="mb-2" type="success" description={<div><div className="spinner-border spinner-border-sm text-primary" role="status"><span className="sr-only">Loading...</span></div>&nbsp;&nbsp;&nbsp;We're logging you in. Buckle up!</div>} />
          }

          {currentUser &&
          <div className="row">
            <div className="col-md-2">
              <CustomNav appearance="subtle" reversed active={active} onSelect={this.selectCategory} />
            </div>
            <div className="col-md-10">
              <div className="container-fluid">
                <Row>
                  {Object.keys(viewItem).length === 0 && selectedCategory !== 999 &&
                    <Col md={6} sm={6}>
                      <input type="text" placeholder="Search" className="form-control" value={this.state.keyword} onChange={this.changeSearch} />
                    </Col>
                  }
                  <Col md={12} sm={12}>
                    <Tag color="blue" className="mt-2"><strong>Madrigal Points:</strong> {currentUser.cash}</Tag>
                    <Tag color="orange" className="mt-2"><strong>Vote Points:</strong> {currentUser.vote}</Tag>
                  </Col>
                </Row>
              </div>
              {Object.keys(viewItem).length > 0 ?
                <div className="StoreItem">
                  <Item 
                    item={viewItem}
                    characters={characters}
                    character={character}
                    purchasing={purchasing}
                    amount={amount}
                    resetItem={this.resetItem}
                    changeCharacter={this.changeCharacter}
                    changeAmount={this.changeAmount}
                    purchaseItem={this.purchaseItem} />
                </div> :
                <div className="StoreItemList">
                  {selectedCategory === 0 &&
                    <div className="PopularItems mt-4">
                      <Row>
                        {popular.map((item, index) => (
                          <Col md={6} sm={12} key={index} onClick={() => this.selectItem(item.id)} style={{ cursor: 'pointer', position: 'relative' }}>
                            <Panel className="text-center">
                              <span className="badge badge-pill badge-primary" style={{ position: 'absolute', top: '5px', left: '30px', transform: 'rotate(-15deg)' }}>Most Popular</span>
                              <img src={ isLink(item.image) } alt={ item.name } style={{ width: '100%', height: '100%' }} /><br/>
                              { item.name }
                            </Panel>
                          </Col>
                        ))}
                      </Row>
                    </div>
                  }
                  <div className="mt-3">
                    <Row>
                      {items.map((item, index) => (
                        <Col md={4} sm={12} key={index} onClick={() => this.selectItem(item.id)} style={{ cursor: 'pointer', position: 'relative' }}>
                          <Panel className="text-center">
                            { item.isSale && <span className="badge badge-danger" style={{ position: 'absolute', top: '5px', left: '30px', transform: 'rotate(-15deg)' }}>On sale!</span> }
                            <img src={ isLink(item.image) } alt={ item.name } style={{ width: '100%', height: '100%' }} /><br/>
                              { item.name }<br/>
                              {item.isVote ?
                                <Tag color="orange" className="mt-2">{item.price} VP</Tag> :
                                <Tag color="blue" className="mt-2">{item.price} MP</Tag>
                              }
                          </Panel>
                        </Col>
                      ))}
                      {items.length === 0 && !loading && errors.length === 0 && selectedCategory !== 999 && selectedCategory !== 998 &&
                        <Message className="mt-3" type="error" description="No items found. Try different category or search phrase." />
                      }
                    </Row>

                    {selectedCategory === 998 &&
                        <Row>
                          {this.state.items.filter(item => item.isSale).map((item, index) => (
                            <Col md={4} sm={12} key={index} onClick={() => this.selectItem(item.id)} style={{ cursor: 'pointer', position: 'relative' }}>
                              <Panel className="text-center">
                                { item.isSale && <span className="badge badge-danger" style={{ position: 'absolute', top: '5px', left: '30px', transform: 'rotate(-15deg)' }}>On sale!</span> }
                                <img src={ isLink(item.image) } alt={ item.name } /><br/>
                                { item.name }
                              </Panel>
                            </Col>
                          ))}
                        </Row>
                    }
                    {this.state.items.filter(item => item.isSale).length === 0 && selectedCategory === 998 &&
                      <Message className="mt-3" type="error" description="No items on sale currently. Check back later! " />
                    }
                  </div>
                </div>
              }

              {selectedCategory === 999 &&
                <div className="mt-3">
                  <Transactions />
                </div>
              }
            </div>
          </div>
          }
        </div>
      </div>
    )
  }
}