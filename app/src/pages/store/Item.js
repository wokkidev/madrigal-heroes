import React from "react";
import ReactMarkdown from 'react-markdown/with-html';

import { isLink } from '../../Helpers';

export default class Item extends React.Component {
  render() {
    const { item, characters, character, amount, purchasing, resetItem, changeCharacter, changeAmount, purchaseItem } = this.props;

    return (
      <div className="Item mt-3">
        <img src={ isLink(item.image) } alt={ item.name } className="img-thumbnail float-left mr-2" width={'50px'} />
        <p><strong>Description:</strong></p> <ReactMarkdown source={item.description} escapeHtml={false} />
        { item.isSale ?
          <p><strong>Price:</strong> <del>{ item.price }</del> { item.salePrice } { item.isVote ? 'Vote Points' : 'Madrigal Points' }</p> :
          <p><strong>Price:</strong> { item.price } { item.isVote ? 'Vote Points' : 'Madrigal Points' }</p>
        }
        <div className="row mt-5">
          <div className="col-md-4">
            <button className="btn btn-primary" onClick={resetItem}>Return</button>
          </div>
          <div className="col-md-8">
            <div className="form-group">
              <div className="input-group mb-3">
                <select className="form-control" value={character} onChange={changeCharacter}>
                  {characters.map((c, index) => (
                    <option key={index} value={c.mIdPlayer}>{ c.mSzName }</option>
                  ))}
                </select>
                <div className="input-group-append">
                  <select className="form-control" value={amount} onChange={changeAmount}>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                  </select>
                </div>
                <div className="input-group-append">
                  <button className="btn btn-primary" onClick={purchaseItem} disabled={purchasing}>
                    {purchasing &&
                      <div className="spinner-border spinner-border-sm" role="status"><span className="sr-only">Loading...</span></div>
                    }
                    Buy for {item.price * amount} {item.isVote ? 'VP' : 'MP'}
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}