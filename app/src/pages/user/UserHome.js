import React, { Component } from "react";
import { Link } from "react-router-dom";

import AuthService from "../../services/auth.service";

export default class User extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      currentUser: {},
      accountData: {}
    };
  }

  async componentDidMount() {
    this._isMounted = true;

    const currentUser = await AuthService.getCurrentUser();
    this.setState({
      currentUser: currentUser
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    const { account, cash, vote, email } = this.state.currentUser || {};

    return (
      <div className="Profile">
        <h3 className="py-3 px-3 mb-0 mh-header">My account</h3>
        <div className="mh-text-container px-3 py-3">
          <p>Welcome, { account }!</p>

          <br/>

          <div className="row">
            <div className="col-md-2">
              <strong>Madrigal Points:</strong>
            </div>
            <div className="col-md-10">
              { cash || 0 }
              &nbsp;
              (<Link to="/donate">
                Add points
              </Link>)
            </div>
          </div>

          <div className="row">
            <div className="col-md-2">
              <strong>Vote Points:</strong>
            </div>
            <div className="col-md-10">
            { vote || 0 }
            &nbsp;
              (<Link to="/vote">
                Vote
              </Link>)
            </div>
          </div>

          <div className="row">
            <div className="col-md-2">
              <strong>Email:</strong>
            </div>
            <div className="col-md-10">
            { email }
            </div>
          </div>
        </div>
      </div>
    )
  }
}