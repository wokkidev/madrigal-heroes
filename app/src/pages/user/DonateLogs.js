import React, { Component } from "react";

import Transactions from '../../components/Transactions.component';

export default class UserDonateLogs extends Component {
  render() {
    return (
      <div className="DonateLogs">
        <h3 className="py-3 px-3 mb-0 mh-header">Transactions</h3>
        <div className="mh-text-container px-3 py-3">
          <Transactions />
        </div>
      </div>
    )
  }
}