import React, { Component } from "react";
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import ReCAPTCHA from "react-google-recaptcha";

import AuthService from "../../services/auth.service";
import UserService from "../../services/user.service";

import { Message } from 'rsuite';

const recaptchaRef = React.createRef();

export default class ChangeEmail extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      successMessage: '',
      displaySuccess: false,
      currentUser: {},
      captcha: ''
    }
  }

  async componentDidMount() {
    this._isMounted = true;

    let currentUser = await AuthService.getCurrentUser();
    this.setState({
      currentUser: currentUser
    });

    if (!this.state.currentUser) {
      this.props.history.push('/');
      window.location.reload();
    }
  }

  render() {
    const currentUser = this.state.currentUser || {};

    return (
      <div className="change-email">
        <h3 className="py-3 px-3 mb-0 mh-header">Change email</h3>
        <div className="mh-text-container px-3 py-3">
          {this.state.displaySuccess &&
            <Message type="success" description={this.state.successMessage} />
          }

          <Formik
            initialValues={{
              email: currentUser.email
            }}
            validationSchema={Yup.object().shape({
              email: Yup.string().email().required('Email is required')
            })}
            onSubmit={({ email }, { setStatus, setSubmitting }) => {
              setStatus();

              if (!this.state.captcha) {
                setStatus('Captcha must be completed to change email')
                return setSubmitting(false);
              }

              if (email === currentUser.email) {
                return setStatus('New email cannot be same as your old email');
              }

              UserService.changeEmail(email).then(
                response => {
                  if (this._isMounted) {
                    this.setState({
                      successMessage: 'Email changed!',
                      displaySuccess: true
                    })
                  }
                  setSubmitting(false);
                },
                error => {
                  const resMessage =
                    (error.response &&
                      error.response.data &&
                      error.response.data.message) ||
                    error.message ||
                    error.toString();
                  console.error(resMessage);
                  setStatus(resMessage);
                  setSubmitting(false);
                });

              setTimeout(() => {
                setSubmitting(false);
              }, 1000)
            }}
          >
            {({ errors, status, touched, isSubmitting }) => (
              <Form>
                {status &&
                  <Message type="error" description={status} />
                }
                <div className="form-group">
                  <label htmlFor="email">New email</label>
                  <Field name="email" type="email" className={'form-control' + (errors.email && touched.email ? ' is-invalid' : '')} />
                  <ErrorMessage name="email" component="div" className="invalid-feedback" />
                </div>
                <div className="form-group">
                  <ReCAPTCHA
                    ref={recaptchaRef}
                    sitekey={process.env.REACT_APP_CAPTCHA_KEY}
                    onChange={(value) => { if(this._isMounted) this.setState({ captcha: value }) }}
                  />
                </div>
                <div className="form-group">
                  <button type="submit" className="btn btn-primary" disabled={isSubmitting}>Update</button>
                  {isSubmitting &&
                    <div className="spinner-border text-primary" role="status"><span className="sr-only">Loading...</span></div>
                  }
                </div>
              </Form>
            )}
          </Formik>
        </div>
      </div>
    )
  }
}