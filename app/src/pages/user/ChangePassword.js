import React, { Component } from "react";
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import ReCAPTCHA from "react-google-recaptcha";

import AuthService from "../../services/auth.service";
import UserService from "../../services/user.service";

import { Message } from 'rsuite';

const recaptchaRef = React.createRef();

export default class ChangePassword extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      currentUser: {},
      successMessage: '',
      displaySuccess: false,
      oldPwHidden: true,
      newPwHidden: true,
      captcha: ''
    }
    this.toggleShowOld = this.toggleShowOld.bind(this);
    this.toggleShowNew = this.toggleShowNew.bind(this);
  }

  async componentDidMount() {
    this._isMounted = true;

    let currentUser = await AuthService.getCurrentUser();
    this.setState({
      currentUser: currentUser
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  toggleShowOld() {
    if (this._isMounted) this.setState({ oldPwHidden: !this.state.oldPwHidden });
  }

  toggleShowNew() {
    if (this._isMounted) this.setState({ newPwHidden: !this.state.newPwHidden });
  }

  render() {
    return (
      <div className="change-password">
        <h3 className="py-3 px-3 mb-0 mh-header">Change password</h3>
        <div className="mh-text-container px-3 py-3">
          {this.state.displaySuccess &&
            <Message type="success" description={this.state.successMessage} />
          }

          <Formik
            initialValues={{
              oldPassword: '',
              password: ''
            }}
            validationSchema={Yup.object().shape({
              oldPassword: Yup.string().required('Current password is required'),
              password: Yup.string().min(4).max(16).required('New password is required')
            })}
            onSubmit={({ oldPassword, password }, { setStatus, setSubmitting }) => {
              setStatus();

              if (!this.state.captcha) {
                setStatus('Captcha must be completed to change password')
                return setSubmitting(false);
              }

              if (oldPassword === password) {
                setSubmitting(false);
                return setStatus('New password can not be same as your old password');
              }

              UserService.changePassword(password).then(
                response => {
                  if (this._isMounted) {
                    this.setState({
                      successMessage: 'Password changed!',
                      displaySuccess: true
                    })
                  }
                  setSubmitting(false);
                },
                error => {
                  const resMessage =
                    (error.response &&
                      error.response.data &&
                      error.response.data.message) ||
                    error.message ||
                    error.toString();
                  console.error(resMessage);
                  setStatus(resMessage);
                  setSubmitting(false);
                });

              setTimeout(() => {
                setSubmitting(false);
              }, 1000)
            }}
          >
            {({ errors, status, touched, isSubmitting }) => (
              <Form>
                {status &&
                  <Message type="error" description={status} />
                }
                <div className="form-group">
                <label htmlFor="oldPassword">Current password</label>
                  <div className="input-group mb-3">
                  <Field name="oldPassword" type={this.state.oldPwHidden ? "password" : "text"} className={'form-control' + (errors.oldPassword && touched.oldPassword ? ' is-invalid' : '')} />
                    <div className="input-group-append">
                      {this.state.oldPwHidden ?
                        <button type="button" className="btn btn-outline-secondary" onClick={this.toggleShowOld}><i className="fas fa-eye"></i></button> :
                        <button type="button" className="btn btn-outline-secondary" onClick={this.toggleShowOld}><i className="fas fa-eye-slash"></i></button>
                      }
                    </div>
                  </div>
                  <ErrorMessage name="oldPassword" component="div" className="invalid-feedback" />
                </div>
                <div className="form-group">
                  <label htmlFor="password">New password</label>
                  <div className="input-group mb-3">
                    <Field name="password" type={this.state.newPwHidden ? "password" : "text"} className={'form-control' + (errors.password && touched.password ? ' is-invalid' : '')} />
                    <div className="input-group-append">
                      {this.state.newPwHidden ?
                        <button type="button" className="btn btn-outline-secondary" onClick={this.toggleShowNew}><i className="fas fa-eye"></i></button> :
                        <button type="button" className="btn btn-outline-secondary" onClick={this.toggleShowNew}><i className="fas fa-eye-slash"></i></button>
                      }
                    </div>
                  </div>
                  <ErrorMessage name="password" component="div" className="invalid-feedback" />
                </div>
                <div className="form-group">
                  <ReCAPTCHA
                    ref={recaptchaRef}
                    sitekey={process.env.REACT_APP_CAPTCHA_KEY}
                    onChange={(value) => { if(this._isMounted) this.setState({ captcha: value }) }}
                  />
                </div>
                <div className="form-group">
                  <button type="submit" className="btn btn-primary" disabled={isSubmitting}>Update</button>
                  {isSubmitting &&
                    <div className="spinner-border text-primary" role="status"><span className="sr-only">Loading...</span></div>
                  }
                </div>
              </Form>
            )}
          </Formik>
        </div>
      </div>
    )
  }
}