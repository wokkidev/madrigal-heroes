import React, { Component } from "react";

import AuthService from "../../services/auth.service";
import UserService from "../../services/user.service";

import { Message } from 'rsuite';

import { convertJob } from '../../Helpers';

import { toHHMMSS } from '../../Helpers';

export default class MyCharacters extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      successMessage: '',
      displaySuccess: false,
      currentUser: {},
      characters: [],
      loadingCharacters: true,
      displayError: false,
      error: ''
    }
  }

  async componentDidMount() {
    this._isMounted = true;

    const currentUser = await AuthService.getCurrentUser();

    this.setState({
      currentUser: currentUser
    });

    UserService.getCharacters(this.state.currentUser.account).then(
      characters => {
        if (this._isMounted) {
          this.setState({
            characters: characters.data.data,
            loadingCharacters: false
          })
        }
      },
      error => {
        console.error(error);
        if (this._isMounted) {
          this.setState({
            error: 'Failed to load characters',
            displayError: true
          });
        }
      }
    )
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    const { characters, displayError, error } = this.state;

    return (
      <div className="my-characters">
        <h3 className="py-3 px-3 mb-0 mh-header">My characters</h3>
        <div className="mh-text-container px-3 py-3">
          {displayError &&
            <Message type="error" description={error} />
          }

          <table className="table">
            <thead>
              <tr>
                <th>Name</th>
                <th>Job</th>
                <th>Level</th>
                <th>Guild</th>
                <th>Playtime</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              {characters.map((character, index) => {
                return (
                  <tr key={index}>
                    <td>{ character.mSzName }</td>
                    <td>{ convertJob(character.mNJob) }</td>
                    <td>{ character.mNLevel }</td>
                    <td>{ character.guildMember ? character.guildMember.guild.mSzGuild : '' }</td>
                    <td>{ toHHMMSS(character.totalPlayTime) }</td>
                    <td className={character.multiServer === 1 ? 'text-success' : 'text-danger'}>{ character.multiServer === 1 ? 'Online' : 'Offline' }</td>
                  </tr>
                )
              })}
            </tbody>
          </table>
          {this.state.loadingCharacters &&
            <div className="text-center"><div className="spinner-border text-primary" role="status"><span className="sr-only">Loading...</span></div></div>
          }
        </div>
      </div>
    )
  }
}