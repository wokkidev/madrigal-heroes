import React, { Component } from "react";
import moment from 'moment';

import UserService from "../../services/user.service";

import TablePagination from "rsuite/lib/Table/TablePagination";

import { Table } from 'rsuite';

const { Column, HeaderCell, Cell } = Table;

export default class UserVoteLogs extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      displayError: false,
      errorMessage: '',
      logs: [],
      displayLength: 10,
      loading: true,
      page: 1
    };

    this.handleChangePage = this.handleChangePage.bind(this);
    this.handleChangeLength = this.handleChangeLength.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;

    UserService.getLogs('vote').then(
      res => {
        res.data.data.forEach(log => { log.createdAt = moment(log.createdAt).format("MM/DD/YYYY HH:mm"); log.site = this.siteName(log.site); return log; });
        if (this._isMounted) {
          this.setState({
            logs: res.data.data,
            loading: false
          });
        }
      },
      err => {
        console.error(err);
      }
    )
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  handleChangePage(dataKey) {
    if (this._isMounted) {
      this.setState({
        page: dataKey
      });
    }
  }
  handleChangeLength(dataKey) {
    if (this._isMounted) {
      this.setState({
        page: 1,
        displayLength: dataKey
      });
    }
  }
  getData() {
    const { displayLength, page, logs } = this.state;

    return logs.filter((v, i) => {
      const start = displayLength * (page - 1);
      const end = start + displayLength;
      return i >= start && i < end;
    });
  }

  siteName = (id) => {
    let site = '';
    if (id === 0) site = 'gtop100';
    else if (id === 1) site = 'xtremetop100';
    else site = 'Unknown';

    return site;
  }

  render() {
    const data = this.getData();
    const { loading, displayLength, page } = this.state;

    return (
      <div className="Profile">
        <h3 className="py-3 px-3 mb-0 mh-header">Vote logs</h3>
        <div className="mh-text-container px-3 py-3">
          <Table height={displayLength === 10 ? 500 : 1000} data={data} loading={loading}>
            <Column width={150}>
              <HeaderCell>Date</HeaderCell>
              <Cell dataKey="createdAt" />
            </Column>

            <Column width={500}>
              <HeaderCell>Message</HeaderCell>
              <Cell dataKey="message" />
            </Column>
          </Table>

          <TablePagination
            lengthMenu={[
              {
                value: 10,
                label: 10
              },
              {
                value: 20,
                label: 20
              }
            ]}
            activePage={page}
            displayLength={displayLength}
            total={data.length}
            onChangePage={this.handleChangePage}
            onChangeLength={this.handleChangeLength}
          />
        </div>
      </div>
    )
  }
}