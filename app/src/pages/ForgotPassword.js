import React from 'react';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { Message } from 'rsuite';
import authService from '../services/auth.service';


export default class ForgotPassword extends React.Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      displaySuccess: false,
      successMessage: ''
    }
  }

  componentDidMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    return (
      <div className="ForgotPassword">
        <h3 className="py-3 px-3 mb-0 mh-header">Forgot password</h3>
				<div className="mh-text-container px-3 py-3">
          <p>Please enter your account name or email to begin recovering your account password.</p>
          <br />
          <Formik
            initialValues={{
              account: ''
            }}
            validationSchema={Yup.object().shape({
              account: Yup.string().required('Account or email is required')
            })}
            onSubmit={({ account }, { setStatus, setSubmitting }) => {
              setStatus();
              authService.forgotPassword(account).then(
                () => {
                  setSubmitting(false);
                  if (this._isMounted) {
                    this.setState({
                      displaySuccess: true,
                      successMessage: 'If the given account name or email was valid, you should receive an email with password reset instructions.'
                    });
                  }
                },
                error => {
                  const resMessage =
                    (error.response &&
                      error.response.data &&
                      error.response.data.message) ||
                    error.message ||
                    error.toString();
                  setStatus(resMessage);
                  setSubmitting(false);
                }
              );
            }}
          >
            {({ errors, status, touched, isSubmitting }) => (
              <Form>
                {status &&
                  <Message type="error" description={status} />
                }
                <div className="form-group">
                  <label htmlFor="account">Account or email</label>
                  <Field name="account" type="text" className={'form-control' + (errors.account && touched.account ? ' is-invalid' : '')} />
                  <ErrorMessage name="account" component="div" className="invalid-feedback" />
                </div>
                <div className="form-group">
                  <button type="submit" className="btn btn-sm btn-primary" disabled={isSubmitting}>
                    {isSubmitting &&
                      <div className="spinner-border spinner-border-sm mr-1" role="status"><span className="sr-only">Loading...</span></div>
                    }
                    Recover account</button>
                </div>
              </Form>
            )}
          </Formik>
        </div>
      </div>
    )
  }
}