import React from 'react';

import SocketContext from '../socketContext';
import AuthService from "../services/auth.service";
import { Message } from 'rsuite';

const gtop100VoteURL = 'https://gtop100.com/topsites/Flyff/sitedetails/Madrigal-Heroes-97860'; // The URL should not include ?vote=1 or &pingUsername, as those get added later on
// const xtremetop100VoteUrl = '';

/**
 * The vote page does not have xtremetop100 implemented, but that is easy to add.
 * 
 * Simply duplicate the openVote() function and rename it to something like openVote2()
 * Change the gtop100VoteURL in the window.open() function to xtremetop100VoteUrl. Play around with the width and height if necessary.
 * 
 * Inside our constructor, duplicate copy
 * this.openVote = this.openVote.bind(this);
 * and change it to
 * this.openVote2 = this.openVote2.bind(this);
 * 
 * Then in the render function next to our gtop100 voting image, add
 * <img src="URL_TO_XTREMETOP100_IMAGE" border="0" alt="Flyff Private Server" onClick={this.openVote2} />
 */

class Vote extends React.Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      currentUser: {},
      isVoting: false,
      displaySuccess: false,
      successMessage: '',
      displayWarning: false,
      warningMessage: '',
      displayError: false,
      errorMessage: '',
      socketId: undefined,
      loading: true,
      username: ''
    }

    this.openVote = this.openVote.bind(this);
    this.handleUsernameChange = this.handleUsernameChange.bind(this);
  }

  async componentDidMount() {
    this._isMounted = true;

    let currentUser = await AuthService.getCurrentUser();
    this.setState({
      currentUser: currentUser
    });

    if (this.props.socket.id && this._isMounted) {
      this.setState({
        socketId: this.props.socket.id,
        loading: false
      });
    }
    else {
      if (this._isMounted) {
        this.setState({
          loading: false,
          displayWarning: true,
          warningMessage: 'Connecting to socket failed. You can still vote and receive rewards, but progress wont be shown on this page.'
        });
      }
    }

    this.props.socket
      .on('connect', () => {
        if (this._isMounted) {
          this.setState({
            socketId: this.props.socket.id,
            loading: false,
            displayWarning: false,
            warningMessage: ''
          });
        }
      })
      .on('voteSuccess', vote => {
        //console.log('Vote success', vote);
        if (this._isMounted) {
          this.setState({
            isVoting: false,
            displaySuccess: true,
            successMessage: vote.message,
            displayWarning: false,
            warningMessage: '',
            displayError: false,
            errorMessage: ''
          });
        }
      })
      .on('voteError', error => {
        //console.log('Vote error', error);
        if (this._isMounted) {
          this.setState({
            isVoting: false,
            displaySuccess: false,
            successMessage: '',
            displayWarning: false,
            warningMessage: '',
            displayError: true,
            errorMessage: error.message
          })
        }
      })
      .on('connect_failed', () => {
        //console.log('Socket connect failed');
        if (this._isMounted) {
          this.setState({
            loading: false,
            displayWarning: true,
            warningMessage: 'Connecting to socket failed. You can still vote and receive rewards, but progress wont be shown on this page.'
          });
        }
      });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  handleUsernameChange(e) {
    this.setState({
      username: e.target.value
    })
  };

  openVote() {
    const user = this.state.currentUser ? this.state.currentUser.account : this.state.username.toLowerCase();

    if (this._isMounted) {
      this.setState({
        isVoting: true
      });
      window.open(
        gtop100VoteURL + '?vote=1&pingUsername=' + user,
        'Vote',
        'scrollbars=no,resizable=yes,status=no,location=no,toolbar=no,menubar=no,width=469,height=775,left=100,top=100'
      );
      setTimeout(() => {
        if (this._isMounted) {
          this.setState({
            displayWarning: true,
            warningMessage: 'Getting response from site is taking longer than normal. If the status does not update within couple minutes you may need to refresh the site and attempt voting again. If this issue continues, please report the issue to our Discord.'
          });
        }
      }, 120000);
    }
  }

  render() {
    const { isVoting, displaySuccess, successMessage, displayWarning, warningMessage, displayError, errorMessage, loading, currentUser } = this.state;
    
    return (
      <div className="Vote">
        <h3 className="py-3 px-3 mb-0 mh-header">Vote</h3>
        <div className="mh-text-container px-3 py-3">
          {displaySuccess &&
            <Message className="mb-3" type="success" description={successMessage} />
          }
          {displayWarning &&
            <Message className="mb-3" type="warning" description={warningMessage} />
          }
          {displayError &&
            <Message className="mb-3" type="error" description={errorMessage} />
          }
          {loading &&
            <div className="spinner-border spinner-border-sm text-primary" role="status"><span className="sr-only">Loading...</span></div>
          }
          {isVoting && !loading ?
            <Message type="success" description={<div><div className="spinner-border spinner-border-sm" role="status"><span className="sr-only">Loading...</span></div> Waiting for vote site response. This usually takes from few seconds up to 2 minutes.</div>} /> :
            <div>
              {!currentUser &&
                <div className="mb-3">
                  <label for="username">Account name</label>
                  <input type="text" className="form-control" id="username" name="username" onChange={this.handleUsernameChange} />
                </div>
              }
              <p>If you don't see the vote buttons and you have ad blocking extensions such as uBlock or AdBlock, try disabling them.</p>
              <img src="https://gtop100.com/images/votebutton.jpg" border="0" alt="Flyff Private Server" onClick={this.openVote} />
            </div>
          }
        </div>
      </div>
    )
  }
}

const VoteWithSocket = props => (
  <SocketContext.Consumer>
    {socket => <Vote {...props} socket={socket} />}
  </SocketContext.Consumer>
);

export default VoteWithSocket;