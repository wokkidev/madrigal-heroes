import React, { Component } from "react";
import update from 'immutability-helper';

import SocketContext from '../socketContext';

import ArticleService from '../services/article.service';
import ArticleView from '../components/Article.component';

import { Message } from 'rsuite';

class Home extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);


    this.state = {
      articles: [],
      displayError: false,
      errorMessage: '',
      loading: true,
      mounted: true
    }
  }

  componentDidMount() {
    this._isMounted = true;
    ArticleService.getArticles().then(
      articles => {
        if (this._isMounted) {
          this.setState({
            articles: articles.data,
            loading: false
          });
        }
      },
      error => {
        console.error(error);
        if (this._isMounted) {
          this.setState({
            displayError: true,
            errorMessage: 'An error occurred while attempting to load news articles. Try again later.'
          })
        }
      }
    )

    this.props.socket
      .on('articleUpdate', article => {
        //console.log('Article update', article);
        let articleIndex = this.state.articles.findIndex(x => x.uuid === article.uuid);
        if (this._isMounted) this.setState(update(this.state, { articles: { [articleIndex]: { $set: article } } }));
      })
      .on('newArticle', article => {
        //console.log('New article', article);
        if (this._isMounted) this.setState(update(this.state, { articles: { $unshift: [article] } }));
      })
      .on('deleteArticle', article => {
        //console.log('Delete article', article);
        if (this._isMounted) {
          this.setState({
            articles: this.state.articles.filter(a => a.uuid !== article.uuid)
          });
        }

      });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    const { articles, displayError, errorMessage } = this.state;

    return (
      <div className="Home">
        {articles.map((article, index) => {
          return (
            <ArticleView data={article} key={index} />
          )
        })}

        {displayError &&
          <Message type="error" description={ errorMessage } />
        }
        {articles.length === 0 && !this.state.loading &&
          <Message type="info" description="No articles to fetch" />
        }
        {this.state.loading &&
          <div className="text-center"><div className="spinner-border text-primary" role="status"><span className="sr-only">Loading...</span></div></div>
        }
      </div>
    )
  }
}

const HomeWithSocket = props => (
  <SocketContext.Consumer>
    {socket => <Home {...props} socket={socket} />}
  </SocketContext.Consumer>
);

export default HomeWithSocket;