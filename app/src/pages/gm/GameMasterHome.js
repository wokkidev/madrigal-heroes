import React, { Component } from "react";

import UserService from "../../services/user.service";

export default class GameMaster extends Component {
  _isMounted = false;

  componentDidMount() {
    this._isMounted = true;

    UserService.getGMData().then(
      response => {
        if (response.status !== 200) {
          this.props.history.push('/');
          window.location.reload();
        }
        console.log(response);
      },
      error => {
        console.error(error);
        this.props.history.push('/');
        window.location.reload();
      }
    );
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    return (
      <div className="GameMaster">
        <h3 className="py-3 px-3 mb-0 mh-header">Game Master</h3>
        <div className="mh-text-container px-3 py-3">
          <p>This page should at all times be only available to those with authority L or Z</p>
        </div>
      </div>
    )
  }
}