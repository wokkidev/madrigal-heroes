import React, { Component } from "react";
import AuthService from "../services/auth.service";

export default class Profile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentUser: {}
    };
  }

  async componentDidMount() {
    let currentUser = await AuthService.getCurrentUser();
    this.setState({
      currentUser: currentUser
    });
  }

  render() {
    const { currentUser } = this.state;

    return (
      <div className="Account">
      <h3 className="py-3 px-3 mb-0 mh-header">Account</h3>
      <div className="mh-text-container px-3 py-3">
        <p>Welcome, { currentUser.account }</p>
        <p>Email: { currentUser.email }</p>
        <p>Authority: { currentUser.authority }</p>
      </div>
    </div>
    );
  }
}