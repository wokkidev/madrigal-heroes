import React from 'react';

import WokkiImg from '../assets/staff/wokki.png';

import { Message } from 'rsuite';

export default class Team extends React.Component {
  render() {
    return (
      <div className="Team">
        <h3 className="py-3 px-3 mb-0 mh-header">Madrigal Heroes Team</h3>
        <div className="mh-text-container px-3 py-3">
          <Message type="warning" description="Madrigal Heroes staff will never ask for your password or any other personal details. If you encounter someone impersonating Madrigal Heroes staff, please report this to our discord immediately." />

          <h4 className="mt-3">Administrative Team</h4>
          <h4 className="mt-3">Moderation Team</h4>

          <h4 className="mt-3">Website developed by</h4>
          <div className="media text-muted pt-3">
            <img src={WokkiImg} alt="" className="mr-2 rounded" />
            <p className="media-body pb-3 mb-0 small 1h-125 border-bottom border-gray">
              <strong className="d-block text-gray-dark">Wokki</strong>
              Web Developer ─ <a href="http://forum.ragezone.com/members/516640.html" target="_blank" rel="noopener noreferrer">RageZone</a> ─ Discord Wokki#0001
            </p>
          </div>
        </div>
      </div>
    )
  }
}