import React from "react";
import { Alert, Message } from 'rsuite';

import SettingService from '../services/setting.service';

export default class Download extends React.Component {
	_isMounted = false;

	constructor(props) {
		super(props);

		this.state = {
			settings: [],
			loading: true,
			mounted: true,
			downloadLinks: []
		}
	}

	componentDidMount() {
		this._isMounted = true;
		SettingService.getSettings().then(
      settings => {
				if (this._isMounted) {
          let downloadLinks = settings.data.data.find(setting => setting.name === 'download').value;
          downloadLinks = downloadLinks.split(';').map(link => {
            let linkData = {};
            linkData.url = link.split('|')[0];
            linkData.text = link.split('|')[1];
            return linkData;
          });

					this.setState({
						settings: settings.data.data,
						downloadLinks: downloadLinks,
						loading: false
					});
				}
      },
      error => {
				Alert.error(error, 5000);
      }
    )
	}

	componentWillUnmount() {
		this._isMounted = false;
	}

  render() {
    const { loading, downloadLinks } = this.state;

    return (
      <div className="Download">
				<h3 className="py-3 px-3 mb-0 mh-header">Download</h3>
				<div className="mh-text-container px-3 py-3">
					{loading ?
						<div className="spinner-border text-primary" role="status"><span className="sr-only">Loading...</span></div> :
						<div>
              {downloadLinks && downloadLinks.map((link, index) => (
                <a type="button" href={link.url} target="_blank" rel="noopener noreferrer" className="btn btn-primary mr-1" key={index}><i className="fas fa-download"></i> {link.text}</a>
              ))}
							{downloadLinks === '#' && !downloadLinks &&
								<Message type="error" description="No valid download links" />
							}
						</div>
					}

					<div className="table-responsive">
						<table className="table mt-3">
							<thead>
								<tr>
									<td></td>
									<th>Minimum requirements</th>
									<th>Recommended requirements</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th>CPU</th>
									<td>Pentium III 800MHZ</td>
									<td>Pentium III 1.2GHz</td>
								</tr>
								<tr>
									<th>RAM</th>
									<td>512MB</td>
									<td>1GB</td>
								</tr>
								<tr>
									<th>Video Card</th>
									<td>128MB</td>
									<td>256MB</td>
								</tr>
								<tr>
									<th>Operating System</th>
									<td>Windows XP, Vista, 7, 8</td>
									<td>Windows XP, Vista, 7, 8</td>
								</tr>
								<tr>
									<th>Disk Space</th>
									<td>3.5GB</td>
									<td>4GB</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
    	</div>
    )
  }
}