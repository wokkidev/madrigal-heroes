import React from "react";

import { Steps } from 'rsuite';
import AuthService from "../services/auth.service";

export default class DonateCancel extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentUser: {},
      step: 1
    }
  }

  async componentDidMount() {
    let currentUser = await AuthService.getCurrentUser();
    this.setState({
      currentUser: currentUser
    });

    if (this.state.currentUser) {
      console.log(this.state.currentUser.account);
    }
  }

  render() {
    return (
      <div className="Donate">
      <h3 className="py-3 px-3 mb-0 mh-header">Donate</h3>
      <div className="mh-text-container px-3 py-3">
        <Steps current={this.state.step} currentStatus="error">
          <Steps.Item title="Details" />
          <Steps.Item title="Donate" />
          <Steps.Item title="Complete" />
        </Steps>
        <hr />
        <p>Donation was cancelled.</p>
      </div>
    </div>
    )
  }
}