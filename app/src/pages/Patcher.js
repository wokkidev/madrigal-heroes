import React from 'react';
import '../Patcher.css';
import ArticleService from '../services/article.service';
import { Message } from 'rsuite';

export default class Patcher extends React.Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      articles: [],
      displayError: false,
      errorMessage: ''
    }
  }

  componentDidMount() {
    this._isMounted = true;

    ArticleService.getArticles().then(
      articles => {
        if (this._isMounted) {
          this.setState({
            articles: articles.data,
            loading: false
          });
        }
      },
      error => {
        console.error(error);
        if (this._isMounted) {
          this.setState({
            displayError: true,
            errorMessage: 'An error occurred while attempting to load news articles. Try again later.'
          })
        }
      }
    )
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    const { displayError, errorMessage, articles } = this.state;

    return (
      <div className="Patcher" style={{ height: window.innerHeight + 'px' }}>
        {displayError &&
          <Message type="error" description={ errorMessage } />
        }
        <table className="table table-borderless">
          <thead>
            <tr>
              <th><h4>{'// Latest news'}</h4></th>
            </tr>
          </thead>
          <tbody>
            {articles.map((article, index) => (
            <tr key={index}>
                <td><a href={`/article/${article.uuid}`}>{article.title}</a></td>
            </tr>
            ))}
          </tbody>
        </table>
      </div>
    )
  }
}