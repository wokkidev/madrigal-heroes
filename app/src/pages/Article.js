import React, { Component } from "react";
import update from 'immutability-helper';

import SocketContext from '../socketContext';

import ArticleService from '../services/article.service';
import ArticleView from '../components/Article.component';

import { Message } from 'rsuite';

class Article extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      article: {},
      displayError: false,
      errorMessage: '',
      loading: true,
      deleted: false
    }
  }

  componentDidMount() {
    this._isMounted = true;
    const uuid = this.props.match.params.uuid;
    
    if (!uuid && this._isMounted) {
      this.setState({
        errorMessage: 'Invalid uuid. Make sure your link is valid.',
        displayError: true
      })
    }
    else {
      ArticleService.getArticle(uuid).then(
        article => {
          if (article.data && this._isMounted) {
            this.setState({
              article: article.data,
              loading: false
            });
          }
          else {
            if (this._isMounted) {
              this.setState({
                errorMessage: 'No article found',
                displayError: true,
                loading: false
              });
            }
          }
        },
        error => {
          console.error(error);
          if (this._isMounted) {
            this.setState({
              displayError: true,
              errorMessage: 'An error occurred while attempting to load news articles. Try again later.'
            });
          }
        }
      )
    }

    this.props.socket
      .on('articleUpdate', article => {
        //console.log('Article update', article);
        this.setState(update(this.state, { article: { $set: article } }));
      })
      .on('deleteArticle', article => {
        //console.log('Delete article', article);
        if (article.uuid === this.state.article.uuid) {
          this.setState({
            deleted: true
          });
        }
      });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    const { deleted, loading, displayError, errorMessage, article } = this.state;

    return (
      <div className="Home">
        {displayError &&
          <Message type="error" description={errorMessage} />
        }
        {loading &&
          <div className="text-center"><div className="spinner-border text-primary" role="status"><span className="sr-only">Loading...</span></div></div>
        }
        {!loading && !deleted && !displayError &&
          <ArticleView data={article} />
        }
        {deleted &&
          <Message type="info" description="Article you were viewing has been removed" />
        }
      </div>
    )
  }
}

const ArticleWithSocket = props => (
  <SocketContext.Consumer>
    {socket => <Article {...props} socket={socket} />}
  </SocketContext.Consumer>
);

export default ArticleWithSocket;