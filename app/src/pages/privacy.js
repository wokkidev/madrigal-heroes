import React from 'react';

export default class Privacy extends React.Component {
  render() {
    return (
      <div className="Privacy">
        <h3 className="py-3 px-3 mb-0 mh-header">Privacy Policy</h3>
        <div className="mh-text-container px-3 py-3">
          <p>Information we collect about our users</p>
          <ul>
            <li>IP address</li>
            <li>Machine address</li>
            <li>Username</li>
            <li>Password</li>
            <li>Email address</li>
          </ul>
        </div>
      </div>
    )
  }
}