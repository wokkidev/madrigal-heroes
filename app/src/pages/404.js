import React from 'react';

export default class Page404 extends React.Component {
  render() {
    return (
      <div className="error404">
        <h3 className="py-3 px-3 mb-0 mh-header">404</h3>
        <div className="mh-text-container px-3 py-3">
          <p>Sorry, nothing to see here.</p>
        </div>
      </div>
    )
  }
}