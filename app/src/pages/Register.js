import React, { Component } from 'react';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import ReCAPTCHA from "react-google-recaptcha";

import AuthService from "../services/auth.service";

import { Message } from 'rsuite';

const recaptchaRef = React.createRef();

export default class Register extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);
    this.state = {
      displayError: false,
      errorMessage: '',
      displaySuccess: false,
      successMessage: '',
      captcha: ''
    }
  }

  componentDidMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    return (
      <div className="Register">
        <h3 className="py-3 px-3 mb-0 mh-header">Register</h3>
        <div className="mh-text-container px-3 py-3">
          {this.state.displaySuccess &&
            <Message type="success" description={this.state.successMessage} />
          }

          <Formik
            initialValues={{
              username: '',
              password: '',
              email: ''
            }}
            validationSchema={Yup.object().shape({
              username: Yup.string().min(4).max(12).required('Username is required'),
              password: Yup.string().min(4).max(16).required('Password is required'),
              email: Yup.string().email().required('Email is required')
            })}
            onSubmit={({ username, password, email }, { setStatus, setSubmitting }) => {
              setStatus();
              if (this._isMounted) {
                this.setState({
                  successMessage: '',
                  displaySuccess: false,
                  errorMessage: '',
                  displayError: false
                });
              }

              if (!this.state.captcha) {
                setStatus('Captcha must be completed to register')
                return setSubmitting(false);
              }

              AuthService.register(username, email,password).then(
                response => {
                  if (this._isMounted) {
                    this.setState({
                      successMessage: `Account ${response.data.data.username} created!`,
                      displaySuccess: true
                    });
                  }
                  //recaptchaRef.reset();
                  setSubmitting(false);
                },
                error => {
                  const resMessage =
                    (error.response &&
                      error.response.data &&
                      error.response.data.message) ||
                    error.message ||
                    error.toString();
                  setStatus(resMessage);
                  //recaptchaRef.reset();
                  setSubmitting(false);
                });
            }}
          >
            {({ errors, status, touched, isSubmitting }) => (
              <Form>
                {status &&
                  <Message type="error" description={status} />
                }
                <div className="form-group">
                  <label htmlFor="username">Username</label>
                  <Field name="username" type="text" className={'form-control' + (errors.username && touched.username ? ' is-invalid' : '')} />
                  <ErrorMessage name="username" component="div" className="invalid-feedback" />
                </div>
                <div className="form-group">
                  <label htmlFor="password">Password</label>
                  <Field name="password" type="password" className={'form-control' + (errors.password && touched.password ? ' is-invalid' : '')} />
                  <ErrorMessage name="password" component="div" className="invalid-feedback" />
                </div>
                <div className="form-group">
                  <label htmlFor="email">Email</label>
                  <Field name="email" type="email" className={'form-control' + (errors.email && touched.email ? ' is-invalid' : '')} />
                  <ErrorMessage name="email" component="div" className="invalid-feedback" />
                </div>
                <div className="form-group">
                  <ReCAPTCHA
                    ref={recaptchaRef}
                    sitekey={process.env.REACT_APP_CAPTCHA_KEY}
                    onChange={(value) => { if(this._isMounted) this.setState({ captcha: value }) }}
                  />
                </div>
                <div className="form-group">
                  <button type="submit" className="btn btn-primary" disabled={isSubmitting}>
                    {isSubmitting &&
                      <div className="spinner-border spinner-border-sm mr-1" role="status"><span className="sr-only">Loading...</span></div>
                    }
                    Register
                  </button>
                </div>
              </Form>
            )}
          </Formik>
        </div>
      </div>
    )
  }
}