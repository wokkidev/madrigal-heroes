import React from "react";
import axios from 'axios';

import { convertRank, convertJob, toHHMMSS } from '../Helpers';

class Ranking extends React.Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      characters: [],
      loading: true,
      error: '',
      displayError: false
    };
  }

  componentDidMount() {
    this._isMounted = true;
    axios.get(`${process.env.REACT_APP_API_ENDPOINT}characters`).then(
      response => {
        if (response.status === 200 && this._isMounted) {
          this.setState({
            characters: response.data.data,
            loading: false
          });
        }
        else {
          if (this._isMounted) {
            this.setState({
              displayError: true,
              error: 'Something went wrong. We were unable to load characters. Try again later.'
            });
          }
        }
      },
      error => {
        if (this._isMounted) {
          this.setState({
            displayError: true,
            error: error
          });
        }
      }
    );
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    const { characters } = this.state;

    return (
      <div className="Register">
        <h3 className="py-3 px-3 mb-0 mh-header">Ranking</h3>
        <div className="mh-text-container px-3 py-3">
          {this.state.displayError &&
            <div className="alert alert-danger" role="alert">{ this.state.error }</div>
          }

          {this.state.loading ?
            <div className="text-center"><div className="spinner-border text-primary" role="status"><span className="sr-only">Loading...</span></div></div> :
            <div className="table-responsive">
              <table className="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Class</th>
                    <th>Name</th>
                    <th>Level</th>
                    <th>Guild</th>
                    <th>Playtime</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  {characters.map(( character, index ) => {
                    return (
                      <tr key={index}>
                        <td>{ convertRank(index + 1) }</td>
                        <td>{ convertJob(character.mNJob, character.mNLevel) }</td>
                        <td>{ character.mSzName }</td>
                        <td>{ character.mNLevel }</td>
                        <td>{ character.guildMember ? character.guildMember.guild.mSzGuild : '' }</td>
                        <td>{ toHHMMSS(character.totalPlayTime) }</td>
                        <td className={character.multiServer === 1 ? 'text-success' : 'text-danger'}>{ character.multiServer === 1 ? 'Online' : 'Offline' }</td>
                      </tr>
                    )
                  })}
                </tbody>
              </table>
            </div>
          }
        </div>
      </div>
    )
  }
}

export default Ranking;