import React, { Component } from 'react';
//import { Link } from "react-router-dom";
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import AuthService from "../services/auth.service";

export default class Login extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      currentUser: {}
    }
  }

  async componentDidMount() {
    let currentUser = await AuthService.getCurrentUser();
    this.setState({
      currentUser: currentUser
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    const { currentUser } = this.state;

    return (
      <div className="Login">
        {currentUser &&
          <div className="alert alert-success" role="alert">Welcome, { currentUser.account }!</div>
        }
        {!currentUser &&
        <Formik
          initialValues={{
            username: '',
            password: ''
          }}
          validationSchema={Yup.object().shape({
            username: Yup.string().required('Username is required'),
            password: Yup.string().required('Password is required')
          })}
          onSubmit={({ username, password }, { setStatus, setSubmitting }) => {
            setStatus();
            AuthService.login(username, password).then(
              () => {
                //this.props.history.push('/user');
                window.location.reload();
              },
              error => {
                const resMessage =
                  (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                  error.message ||
                  error.toString();
                setStatus(resMessage);
                setSubmitting(false);
              });
          }}
        >
          {({ errors, status, touched, isSubmitting }) => (
            <Form>
              {status &&
                <div className={'alert alert-danger'}>{ status }</div>
              }
              <div className="form-group">
                <label htmlFor="username">Username</label>
                <Field name="username" type="text" className={'form-control' + (errors.username && touched.username ? ' is-invalid' : '')} />
                <ErrorMessage name="username" component="div" className="invalid-feedback" />
              </div>
              <div className="form-group">
                <label htmlFor="password">Password</label>
                <Field name="password" type="password" className={'form-control' + (errors.password && touched.password ? ' is-invalid' : '')} />
                <ErrorMessage name="password" component="div" className="invalid-feedback" />
              </div>
              <div className="form-group">
                {/*<Link to="/forgotpassword" className="btn btn-sm btn-link">Forgot password?</Link>*/}
                <button type="submit" className="btn btn-primary" disabled={isSubmitting}>
                  {isSubmitting &&
                    <div className="spinner-border spinner-border-sm mr-1" role="status"><span className="sr-only">Loading...</span></div>
                  }
                  Login
                </button>
              </div>
            </Form>
          )}
        </Formik>
        }
      </div>
    )
  }
}