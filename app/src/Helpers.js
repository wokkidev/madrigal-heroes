import React from "react";
import rank1 from './assets/1st.png';
import rank2 from './assets/2nd.png';
import rank3 from './assets/3rd.png';

//import heroFullImage from '../assets/hero.png';
import heroImage from './assets/h.png';
import master1Image from './assets/m1.png';
import master2Image from './assets/m2.png';
import master3Image from './assets/m3.png';
import master4Image from './assets/m4.png';
import master5Image from './assets/m5.png';
import master6Image from './assets/m6.png';

/**
 * isLink
 * 
 * Fetch the image from items folder if the string is not a link or use the normal value if the string is a link
 * @param {*} value 
 */
export function isLink(value) {
  if (value.startsWith('http://') || value.startsWith('https://')) {
    return value;
  }
  else {
    return process.env.PUBLIC_URL + '/items/' + value;
  }
}

/**
 * Convert Rank
 * 
 * Return rank images for places 1-3 and numbers for 4+
 * @param {*} rank 
 */
export function convertRank(rank) {
  if (rank === 1) return (<img src={rank1} alt="Rank 1" />);
  else if (rank === 2) return (<img src={rank2} alt="Rank 2" />);
  else if (rank === 3) return (<img src={rank3} alt="Rank 3" />);
  else return rank;
}

/**
 * Convert Job
 * 
 * Convert FlyFF job ID's to job names. Also include master and hero icons
 * @param {*} id 
 * @param {*} level 
 */
export function convertJob(id, level) {
  let job;
  switch(id) {
    case 0:
      job = 'Vagrant';
      break;
    case 1:
      job = 'Mercenary';
      break;
    case 2:
      job = 'Acrobat';
      break;
    case 3:
      job = 'Assist';
      break;
    case 4:
      job = 'Magician';
      break;
    case 5:
      job = 'Puppeteer';
      break;
    case 6:
      job = 'Knight';
      break;
    case 7:
      job = 'Blade';
      break;
    case 8:
      job = 'Jester';
      break;
    case 9:
      job = 'Ranger';
      break;
    case 10:
      job = 'Ringmaster';
      break;
    case 11:
      job = 'Billposter';
      break;
    case 12:
      job = 'Psykeeper';
      break;
    case 13:
      job = 'Elementor';
      break;
    case 14:
      job = 'Gatekeeper';
      break;
    case 15:
      job = 'Doppler';
      break;
    case 16:
      if (level >= 60 && level < 70) job = <span><img src={master1Image} alt="Master" /> Knight</span>
      else if (level >= 70 && level < 80) job = <span><img src={master2Image} alt="Master" /> Knight</span>
      else if (level >= 80 && level < 90) job = <span><img src={master3Image} alt="Master" /> Knight</span>
      else if (level >= 90 && level < 100) job = <span><img src={master4Image} alt="Master" /> Knight</span>
      else if (level >= 100 && level < 110) job = <span><img src={master5Image} alt="Master" /> Knight</span>
      else if (level >= 110 && level <= 120) job = <span><img src={master6Image} alt="Master" /> Knight</span>
      else job = 'Master Knight';
      break;
    case 17:
      if (level >= 60 && level < 70) job = <span><img src={master1Image} alt="Master" /> Blade</span>
      else if (level >= 70 && level < 80) job = <span><img src={master2Image} alt="Master" /> Blade</span>
      else if (level >= 80 && level < 90) job = <span><img src={master3Image} alt="Master" /> Blade</span>
      else if (level >= 90 && level < 100) job = <span><img src={master4Image} alt="Master" /> Blade</span>
      else if (level >= 100 && level < 110) job = <span><img src={master5Image} alt="Master" /> Blade</span>
      else if (level >= 110 && level <= 120) job = <span><img src={master6Image} alt="Master" /> Blade</span>
      else job = 'Master Blade';
      break;
    case 18:
      if (level >= 60 && level < 70) job = <span><img src={master1Image} alt="Master" /> Jester</span>
      else if (level >= 70 && level < 80) job = <span><img src={master2Image} alt="Master" /> Jester</span>
      else if (level >= 80 && level < 90) job = <span><img src={master3Image} alt="Master" /> Jester</span>
      else if (level >= 90 && level < 100) job = <span><img src={master4Image} alt="Master" /> Jester</span>
      else if (level >= 100 && level < 110) job = <span><img src={master5Image} alt="Master" /> Jester</span>
      else if (level >= 110 && level <= 120) job = <span><img src={master6Image} alt="Master" /> Jester</span>
      else job = 'Master Jester';
      break;
    case 19:
      if (level >= 60 && level < 70) job = <span><img src={master1Image} alt="Master" /> Ranger</span>
      else if (level >= 70 && level < 80) job = <span><img src={master2Image} alt="Master" /> Ranger</span>
      else if (level >= 80 && level < 90) job = <span><img src={master3Image} alt="Master" /> Ranger</span>
      else if (level >= 90 && level < 100) job = <span><img src={master4Image} alt="Master" /> Ranger</span>
      else if (level >= 100 && level < 110) job = <span><img src={master5Image} alt="Master" /> Ranger</span>
      else if (level >= 110 && level <= 120) job = <span><img src={master6Image} alt="Master" /> Ranger</span>
      else job = 'Master Ranger';
      break;
    case 20:
      if (level >= 60 && level < 70) job = <span><img src={master1Image} alt="Master" /> Ringmaster</span>
      else if (level >= 70 && level < 80) job = <span><img src={master2Image} alt="Master" /> Ringmaster</span>
      else if (level >= 80 && level < 90) job = <span><img src={master3Image} alt="Master" /> Ringmaster</span>
      else if (level >= 90 && level < 100) job = <span><img src={master4Image} alt="Master" /> Ringmaster</span>
      else if (level >= 100 && level < 110) job = <span><img src={master5Image} alt="Master" /> Ringmaster</span>
      else if (level >= 110 && level <= 120) job = <span><img src={master6Image} alt="Master" /> Ringmaster</span>
      else job = 'Master Ringmaster';
      break;
    case 21:
      if (level >= 60 && level < 70) job = <span><img src={master1Image} alt="Master" /> Billposter</span>
      else if (level >= 70 && level < 80) job = <span><img src={master2Image} alt="Master" /> Billposter</span>
      else if (level >= 80 && level < 90) job = <span><img src={master3Image} alt="Master" /> Billposter</span>
      else if (level >= 90 && level < 100) job = <span><img src={master4Image} alt="Master" /> Billposter</span>
      else if (level >= 100 && level < 110) job = <span><img src={master5Image} alt="Master" /> Billposter</span>
      else if (level >= 110 && level <= 120) job = <span><img src={master6Image} alt="Master" /> Billposter</span>
      else job = 'Master Billposter';
      break;
    case 22:
      if (level >= 60 && level < 70) job = <span><img src={master1Image} alt="Master" /> Psykeeper</span>
      else if (level >= 70 && level < 80) job = <span><img src={master2Image} alt="Master" /> Psykeeper</span>
      else if (level >= 80 && level < 90) job = <span><img src={master3Image} alt="Master" /> Psykeeper</span>
      else if (level >= 90 && level < 100) job = <span><img src={master4Image} alt="Master" /> Psykeeper</span>
      else if (level >= 100 && level < 110) job = <span><img src={master5Image} alt="Master" /> Psykeeper</span>
      else if (level >= 110 && level <= 120) job = <span><img src={master6Image} alt="Master" /> Psykeeper</span>
      else job = 'Master Psykeeper';
      break;
    case 23:
      if (level >= 60 && level < 70) job = <span><img src={master1Image} alt="Master" /> Elementor</span>
      else if (level >= 70 && level < 80) job = <span><img src={master2Image} alt="Master" /> Elementor</span>
      else if (level >= 80 && level < 90) job = <span><img src={master3Image} alt="Master" /> Elementor</span>
      else if (level >= 90 && level < 100) job = <span><img src={master4Image} alt="Master" /> Elementor</span>
      else if (level >= 100 && level < 110) job = <span><img src={master5Image} alt="Master" /> Elementor</span>
      else if (level >= 110 && level <= 120) job = <span><img src={master6Image} alt="Master" /> Elementor</span>
      else job = 'Master Elementor';
      break;
    case 24:
      job = <span><img src={heroImage} alt="Hero" /> Knight</span>
      break;
    case 25:
      job = <span><img src={heroImage} alt="Hero" /> Blade</span>
      break;
    case 26:
      job = <span><img src={heroImage} alt="Hero" /> Jester</span>
      break;
    case 27:
      job = <span><img src={heroImage} alt="Hero" /> Ranger</span>
      break;
    case 28:
      job = <span><img src={heroImage} alt="Hero" /> Ringmaster</span>
      break;
    case 29:
      job = <span><img src={heroImage} alt="Hero" /> Billposter</span>
      break;
    case 30:
      job = <span><img src={heroImage} alt="Hero" /> Psykeeper</span>
      break;
    case 31:
      job = <span><img src={heroImage} alt="Hero" /> Elementor</span>
      break;
    default:
      job = 'Unknown';
      break;
  }

  return job;
}

/** 
 * Seconds to hours, minutes, seconds
 * @param {*} time 
 */
export function toHHMMSS(time) {
  let sec_num = parseInt(time, 10); // don't forget the second param
  let hours   = Math.floor(sec_num / 3600);
  let minutes = Math.floor((sec_num - (hours * 3600)) / 60);
  let seconds = sec_num - (hours * 3600) - (minutes * 60);

  if (hours   < 10) {hours   = "0"+hours;}
  if (minutes < 10) {minutes = "0"+minutes;}
  if (seconds < 10) {seconds = "0"+seconds;}
  return hours+':'+minutes+':'+seconds;
}

/**
 * Get Element
 * 
 * Convert FlyFF element ID to a text
 * @param {*} element 
 */
export function getElement(element) {
  let el;
  switch(element) {
    case 1:
      el = 'Fire';
      break;
    case 2:
      el = 'Water';
      break;
    case 3:
      el = 'Electric';
      break;
    case 4:
      el = 'Wind';
      break;
    case 5:
      el = 'Earth';
      break;
    default:
      el = 'Unknown';
      break;
  }

  return el;
}