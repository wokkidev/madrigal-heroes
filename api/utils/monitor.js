import net from 'net';

/**
 * Monitor server status
 * @param host Host IP or domain 
 * @param port Host port
 * @param interval Time between pings in ms
 */
export default class Monitor {
  constructor(host, port, interval) {
    this.host = host;
    this.port = port;
    this.interval = interval || 300000;
    this.dispatcher = new Dispatcher();
  }

  start() {
    setInterval(async () => {
      const status = await this.ping(this.host, this.port);
      if (!status) this.dispatcher.dispatch('status', false);
      else this.dispatcher.dispatch('status', true);
    }, this.interval);
  }

  ping(host = this.host, port = this.port) {
    return new Promise((resolve, reject) => {
      const socket = new net.Socket();
      socket.setTimeout(2500);
      socket.on('connect', () => {
        resolve(true);
        socket.destroy();
      }).on('error', (e) => {
        resolve(false);
      }).on('timeout', (e) => {
        resolve(false);
      }).connect(port, host);
    });
  }

  on(eventName, callback) {
    this.dispatcher.on(eventName, callback);
  }
}

class Dispatcher {
  constructor() {
    this.events = {};
  }

  dispatch(eventName, data) {
    const event = this.events[eventName];
    if (event) {
      event.fire(data);
    }
  }

  on(eventName, callback) {
    let event = this.events[eventName];
    if (!event) {
      event = new DispatcherEvent(eventName);
      this.events[eventName] = event;
    }
    event.registerCallback(callback);
  }

  off(eventName, callback) {
    const event = this.events[eventName];
    if (event && event.callbacks.indexOf(callback) > -1) {
      event.unregisterCallback(callback);
      if (event.callbacks.length === 0) {
        delete this.events[eventName];
      }
    }
  }
}

class DispatcherEvent {
  constructor(eventName) {
    this.eventName = eventName;
    this.callbacks = [];
  }

  registerCallback(callback) {
    this.callbacks.push(callback);
  }

  unregisterCallback(callback) {
    const index = this.callbacks.indexOf(callback);
    if (index > -1) {
      this.callbacks.splice(index, 1);
    }
  }

  fire(data) {
    const callbacks = this.callbacks.slice(0);
    callbacks.forEach((callback) => {
      callback(data);
    });
  }
}