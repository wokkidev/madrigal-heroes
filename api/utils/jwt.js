import jwt from 'jsonwebtoken';
import models from '../database/models';

const verifyToken = (req, res, next) => {
  //const token = req.headers["x-access-token"];
  const token = req.cookies.token;

  if (!token) {
    return res.status(403).send({
      message: 'No token provided'
    });
  }

  jwt.verify(token, process.env.SECRET, (err, decoded) => {
    if (err) {
      return res.status(401).send({
        message: 'Unauthorized'
      });
    }
    req.account = decoded.sub;
    req.authority = decoded.authority;
    next();
  });
}

const isAdmin = (req, res, next) => {
  models.accountTbl.findByPk(req.account, {
    attributes: ['account'],
    include: [
      {
        model: models.accountTblDetail,
        as: 'detail',
        attributes: ['mChLoginAuthority']
      }
    ]
  }).then(user => {
    if (user.detail.mChLoginAuthority === 'Z') {
      next();
      return;
    }
    else {
      res.status(403).send({
        message: 'Unauthorized'
      });
    }
    return;
  });
}

const isGameMaster = (req, res, next) => {
  models.accountTbl.findByPk(req.account, {
    attributes: ['account'],
    include: [
      {
        model: models.accountTblDetail,
        as: 'detail',
        attributes: ['mChLoginAuthority']
      }
    ]
  }).then(user => {
    if (user.detail.mChLoginAuthority === 'L' || user.detail.mChLoginAuthority === 'Z') {
      next();
      return;
    }
    else {
      res.status(403).send({
        message: 'Unauthorized'
      });
    }
    return;
  });
}

const authJwt = {
  verifyToken: verifyToken,
  isAdmin: isAdmin,
  isGameMaster: isGameMaster
};
export default authJwt;