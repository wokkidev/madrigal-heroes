'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.addColumn('ACCOUNT_TBL', 'vote', {
          type: Sequelize.DataTypes.INTEGER,
          defaultValue: 0
        })
      ]);
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.removeColumn('ACCOUNT_TBL', 'vote', { transaction: t }),
      ]);
    });
  }
};
