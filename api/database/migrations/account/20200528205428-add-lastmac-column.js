'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn('ACCOUNT_TBL_DETAIL', 'lastmac', {
        type: Sequelize.DataTypes.STRING(40),
        defaultValue: null
      })
    ])
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.removeColumn('ACCOUNT_TBL_DETAIL', 'lastmac', { transaction: t }),
      ]);
    });
  }
};
