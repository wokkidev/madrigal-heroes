'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Settings', [
      {
        uuid: 'e4eeb65b-354e-43bc-8373-2883d19c039c',
        name: 'download',
        value: '#',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        uuid: 'f26bb10b-4218-436b-9fc8-a3712504ea59',
        name: 'discord',
        value: '#',
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Settings', null, {});
  }
};
