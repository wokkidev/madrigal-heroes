require('dotenv').config();

module.exports = {
  "development": {
    "databases": {
      "Account": {
        "username": process.env.DB_USER,
        "password": process.env.DB_PASS,
        "database": "ACCOUNT_DBF",
        "host": process.env.DB_HOST,
        "dialect": "mssql",
        "operatorsAliases": 0,
        "logging": false
      },
      "Character": {
        "username": process.env.DB_USER,
        "password": process.env.DB_PASS,
        "database": "CHARACTER_01_DBF",
        "host": process.env.DB_HOST,
        "dialect": "mssql",
        "operatorsAliases": 0,
        "logging": false
      },
      "Logging": {
        "username": process.env.DB_USER,
        "password": process.env.DB_PASS,
        "database": "LOGGING_01_DBF",
        "host": process.env.DB_HOST,
        "dialect": "mssql",
        "operatorsAliases": 0,
        "logging": false
      },
      "Itemdb": {
        "username": process.env.DB_USER,
        "password": process.env.DB_PASS,
        "database": "ITEM_DBF",
        "host": process.env.DB_HOST,
        "dialect": "mssql",
        "operatorsAliases": 0,
        "logging": false
      },
      "Web": {
        "username": process.env.DB_USER,
        "password": process.env.DB_PASS,
        "database": "WEB_DBF",
        "host": process.env.DB_HOST,
        "dialect": "mssql",
        "operatorsAliases": 0,
        "logging": false
      }
    }
  },
  "test": {
    "databases": {
      "Account": {
        "username": process.env.DB_USER,
        "password": process.env.DB_PASS,
        "database": "ACCOUNT_DBF",
        "host": process.env.DB_HOST,
        "dialect": "mssql",
        "operatorsAliases": 0
      },
      "Character": {
        "username": process.env.DB_USER,
        "password": process.env.DB_PASS,
        "database": "CHARACTER_01_DBF",
        "host": process.env.DB_HOST,
        "dialect": "mssql",
        "operatorsAliases": 0
      },
      "Logging": {
        "username": process.env.DB_USER,
        "password": process.env.DB_PASS,
        "database": "LOGGING_01_DBF",
        "host": process.env.DB_HOST,
        "dialect": "mssql",
        "operatorsAliases": 0
      },
      "Itemdb": {
        "username": process.env.DB_USER,
        "password": process.env.DB_PASS,
        "database": "ITEM_DBF",
        "host": process.env.DB_HOST,
        "dialect": "mssql",
        "operatorsAliases": 0,
        "logging": false
      },
      "Web": {
        "username": process.env.DB_USER,
        "password": process.env.DB_PASS,
        "database": "WEB_DBF",
        "host": process.env.DB_HOST,
        "dialect": "mssql",
        "operatorsAliases": 0
      }
    }
  },
  "production": {
    "databases": {
      "Account": {
        "username": process.env.DB_USER,
        "password": process.env.DB_PASS,
        "database": "ACCOUNT_DBF",
        "host": process.env.DB_HOST,
        "dialect": "mssql",
        "operatorsAliases": 0
      },
      "Character": {
        "username": process.env.DB_USER,
        "password": process.env.DB_PASS,
        "database": "CHARACTER_01_DBF",
        "host": process.env.DB_HOST,
        "dialect": "mssql",
        "operatorsAliases": 0
      },
      "Logging": {
        "username": process.env.DB_USER,
        "password": process.env.DB_PASS,
        "database": "LOGGING_01_DBF",
        "host": process.env.DB_HOST,
        "dialect": "mssql",
        "operatorsAliases": 0
      },
      "Itemdb": {
        "username": process.env.DB_USER,
        "password": process.env.DB_PASS,
        "database": "ITEM_DBF",
        "host": process.env.DB_HOST,
        "dialect": "mssql",
        "operatorsAliases": 0,
        "logging": false
      },
      "Web": {
        "username": process.env.DB_USER,
        "password": process.env.DB_PASS,
        "database": "WEB_DBF",
        "host": process.env.DB_HOST,
        "dialect": "mssql",
        "operatorsAliases": 0
      }
    }
  },
  "Web": {
    "username": process.env.DB_USER,
    "password": process.env.DB_PASS,
    "database": "WEB_DBF",
    "host": process.env.DB_HOST,
    "dialect": "mssql",
    "operatorsAliases": 0
  },
  "Account": {
    "username": process.env.DB_USER,
    "password": process.env.DB_PASS,
    "database": "ACCOUNT_DBF",
    "host": process.env.DB_HOST,
    "dialect": "mssql",
    "operatorsAliases": 0,
    "logging": false
  }
}