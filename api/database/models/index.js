'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(__filename);
const env = process.env.NODE_ENV || 'development';
const config = require(`${__dirname}/../config/config.js`)[env];
const db = {};

const databases = Object.keys(config.databases);
for(let i = 0; i < databases.length; ++i) {
  let database = databases[i];
  let dbPath = config.databases[database];
  db[database] = new Sequelize(dbPath.database, dbPath.username, dbPath.password, dbPath);
}

fs
  .readdirSync(__dirname + '/account')
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    const model = db.Account.import(path.join(__dirname + '/account', file));
    db[model.name] = model;
  });

fs
  .readdirSync(__dirname + '/character')
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    const model = db.Character.import(path.join(__dirname + '/character', file));
    db[model.name] = model;
  });

fs
  .readdirSync(__dirname + '/logging')
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    const model = db.Logging.import(path.join(__dirname + '/logging', file));
    db[model.name] = model;
  });

fs
  .readdirSync(__dirname + '/item')
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    const model = db.Itemdb.import(path.join(__dirname + '/item', file));
    db[model.name] = model;
  });

fs
  .readdirSync(__dirname + '/web')
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    const model = db.Web.import(path.join(__dirname + '/web', file));
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.Sequelize = Sequelize;

module.exports = db;