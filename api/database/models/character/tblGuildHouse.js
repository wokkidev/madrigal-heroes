/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblGuildHouse', {
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true,
			field: 'serverindex'
		},
		mIdGuild: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true,
			field: 'm_idGuild'
		},
		dwWorldId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwWorldID'
		},
		tKeepTime: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'tKeepTime'
		},
		mSzGuild: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_szGuild'
		}
	}, {
		tableName: 'tblGuildHouse'
	});
};
