/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblMessenger', {
		idPlayer: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'idPlayer'
		},
		idFriend: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'idFriend'
		},
		bBlock: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'bBlock'
		},
		chUse: {
			type: DataTypes.CHAR,
			allowNull: false,
			defaultValue: 'T',
			field: 'chUse'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		}
	}, {
		tableName: 'tblMessenger'
	});
};
