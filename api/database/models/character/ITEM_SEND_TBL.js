/* jshint indent: 1 */
import Sequelize from 'sequelize';

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('itemSendTbl', {
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true,
			field: 'serverindex'
		},
		itemName: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'Item_Name'
		},
		itemCount: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: 1,
			field: 'Item_count'
		},
		mNAbilityOption: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '000000',
			field: 'm_nAbilityOption'
		},
		mNNo: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
			field: 'm_nNo'
		},
		endTime: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'End_Time'
		},
		mBItemResist: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '000000',
			field: 'm_bItemResist'
		},
		mNResistAbilityOption: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '000000',
			field: 'm_nResistAbilityOption'
		},
		mBCharged: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: 0,
			field: 'm_bCharged'
		},
		idSender: {
			type: DataTypes.CHAR,
			allowNull: true,
			defaultValue: '0000000',
			field: 'idSender'
		},
		nPiercedSize: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: 0,
			field: 'nPiercedSize'
		},
		adwItemId0: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: 0,
			field: 'adwItemId0'
		},
		adwItemId1: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: 0,
			field: 'adwItemId1'
		},
		adwItemId2: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: 0,
			field: 'adwItemId2'
		},
		adwItemId3: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: 0,
			field: 'adwItemId3'
		},
		mDwKeepTime: {
			type: DataTypes.BIGINT,
			allowNull: false,
			defaultValue: 0,
			field: 'm_dwKeepTime'
		},
		itemFlag: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: 0,
			field: 'ItemFlag'
		},
		receiveDt: {
			type: DataTypes.DATEONLY,
			allowNull: false,
			defaultValue: Sequelize.NOW,
			field: 'ReceiveDt'
		},
		provideDt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'ProvideDt'
		},
		nRandomOptItemId: {
			type: DataTypes.BIGINT,
			allowNull: true,
			defaultValue: 0,
			field: 'nRandomOptItemId'
		},
		itemBillingNo: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'ItemBillingNo'
		},
		adwItemId4: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: 0,
			field: 'adwItemId4'
		},
		adwItemId5: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: 0,
			field: 'adwItemId5'
		},
		adwItemId6: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: 0,
			field: 'adwItemId6'
		},
		adwItemId7: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: 0,
			field: 'adwItemId7'
		},
		adwItemId8: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: 0,
			field: 'adwItemId8'
		},
		adwItemId9: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: 0,
			field: 'adwItemId9'
		},
		nUmPiercedSize: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: 0,
			field: 'nUMPiercedSize'
		},
		adwUmItemId0: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: 0,
			field: 'adwUMItemId0'
		},
		adwUmItemId1: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: 0,
			field: 'adwUMItemId1'
		},
		adwUmItemId2: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: 0,
			field: 'adwUMItemId2'
		},
		adwUmItemId3: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: 0,
			field: 'adwUMItemId3'
		},
		adwUmItemId4: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: 0,
			field: 'adwUMItemId4'
		}
	}, {
		timestamps: false,
		tableName: 'ITEM_SEND_TBL'
	});
};
