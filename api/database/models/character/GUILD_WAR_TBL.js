/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('guildWarTbl', {
		mIdGuild: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'm_idGuild'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mIdWar: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_idWar'
		},
		fIdGuild: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'f_idGuild'
		},
		mNDeath: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nDeath'
		},
		mNSurrender: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nSurrender'
		},
		mNCount: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nCount'
		},
		mNAbsent: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nAbsent'
		},
		fNDeath: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'f_nDeath'
		},
		fNSurrender: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'f_nSurrender'
		},
		fNCount: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'f_nCount'
		},
		fNAbsent: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'f_nAbsent'
		},
		state: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'State'
		},
		startTime: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'StartTime'
		}
	}, {
		tableName: 'GUILD_WAR_TBL'
	});
};
