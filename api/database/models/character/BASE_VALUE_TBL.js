/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('baseValueTbl', {
		gNSex: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'g_nSex'
		},
		mVScaleX: {
			type: DataTypes.REAL,
			allowNull: true,
			field: 'm_vScale_x'
		},
		mDwMotion: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_dwMotion'
		},
		mFAngle: {
			type: DataTypes.REAL,
			allowNull: true,
			field: 'm_fAngle'
		},
		mNHitPoint: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nHitPoint'
		},
		mNManaPoint: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nManaPoint'
		},
		mNFatiguePoint: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nFatiguePoint'
		},
		mDwRideItemIdx: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_dwRideItemIdx'
		},
		mDwGold: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_dwGold'
		},
		mNJob: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nJob'
		},
		mPActMover: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_pActMover'
		},
		mNStr: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nStr'
		},
		mNSta: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nSta'
		},
		mNDex: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nDex'
		},
		mNInt: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nInt'
		},
		mNLevel: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nLevel'
		},
		mNExp1: {
			type: DataTypes.BIGINT,
			allowNull: true,
			field: 'm_nExp1'
		},
		mNExp2: {
			type: DataTypes.BIGINT,
			allowNull: true,
			field: 'm_nExp2'
		},
		mAJobSkill: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_aJobSkill'
		},
		mALicenseSkill: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_aLicenseSkill'
		},
		mAJobLv: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_aJobLv'
		},
		mDwExpertLv: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_dwExpertLv'
		},
		mIdMarkingWorld: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_idMarkingWorld'
		},
		mVMarkingPosX: {
			type: DataTypes.REAL,
			allowNull: true,
			field: 'm_vMarkingPos_x'
		},
		mVMarkingPosY: {
			type: DataTypes.REAL,
			allowNull: true,
			field: 'm_vMarkingPos_y'
		},
		mVMarkingPosZ: {
			type: DataTypes.REAL,
			allowNull: true,
			field: 'm_vMarkingPos_z'
		},
		mNRemainGp: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nRemainGP'
		},
		mNRemainLp: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nRemainLP'
		},
		mNFlightLv: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nFlightLv'
		},
		mNFxp: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nFxp'
		},
		mNTxp: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nTxp'
		},
		mLpQuestCntArray: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_lpQuestCntArray'
		},
		mChAuthority: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_chAuthority'
		},
		mDwMode: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_dwMode'
		},
		blockby: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'blockby'
		},
		totalPlayTime: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'TotalPlayTime'
		},
		isblock: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'isblock'
		},
		mInventory: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_Inventory'
		},
		mApIndex: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_apIndex'
		},
		mAdwEquipment: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_adwEquipment'
		},
		mASlotApplet: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_aSlotApplet'
		},
		mASlotItem: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_aSlotItem'
		},
		mASlotQueue: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_aSlotQueue'
		},
		mSkillBar: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_SkillBar'
		},
		mDwObjIndex: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_dwObjIndex'
		},
		mCard: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_Card'
		},
		mCube: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_Cube'
		},
		mApIndexCard: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_apIndex_Card'
		},
		mDwObjIndexCard: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_dwObjIndex_Card'
		},
		mApIndexCube: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_apIndex_Cube'
		},
		mDwObjIndexCube: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_dwObjIndex_Cube'
		},
		mIdparty: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_idparty'
		},
		mNNumKill: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nNumKill'
		},
		mIdMuerderer: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_idMuerderer'
		},
		mNSlaughter: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nSlaughter'
		},
		mNFame: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nFame'
		},
		mNDeathExp: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nDeathExp'
		},
		mNDeathLevel: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nDeathLevel'
		},
		mDwFlyTime: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_dwFlyTime'
		},
		mNMessengerState: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nMessengerState'
		},
		mBank: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_Bank'
		},
		mApIndexBank: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_apIndex_Bank'
		},
		mDwObjIndexBank: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_dwObjIndex_Bank'
		},
		mDwGoldBank: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_dwGoldBank'
		}
	}, {
		tableName: 'BASE_VALUE_TBL'
	});
};
