/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('taskbarTbl', {
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mASlotApplet: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_aSlotApplet'
		},
		mASlotQueue: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_aSlotQueue'
		},
		mSkillBar: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_SkillBar'
		}
	}, {
		tableName: 'TASKBAR_TBL'
	});
};
