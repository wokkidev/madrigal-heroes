/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	const tblCombatJoinGuild = sequelize.define('tblCombatJoinGuild', {
		combatId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			references: {
				model: 'tblCombatInfo',
				key: 'serverindex'
			},
			field: 'CombatID'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true,
			references: {
				model: 'tblCombatInfo',
				key: 'serverindex'
			},
			field: 'serverindex'
		},
		guildId: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true,
			field: 'GuildID'
		},
		status: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'Status'
		},
		applyDt: {
			type: DataTypes.DATE,
			allowNull: false,
			field: 'ApplyDt'
		},
		combatFee: {
			type: DataTypes.BIGINT,
			allowNull: false,
			field: 'CombatFee'
		},
		returnCombatFee: {
			type: DataTypes.BIGINT,
			allowNull: false,
			field: 'ReturnCombatFee'
		},
		reward: {
			type: DataTypes.BIGINT,
			allowNull: false,
			field: 'Reward'
		},
		point: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'Point'
		},
		rewardDt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'RewardDt'
		},
		cancelDt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'CancelDt'
		},
		straightWin: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'StraightWin'
		},
		seq: {
			type: DataTypes.BIGINT,
			allowNull: false,
			field: 'SEQ'
		}
	}, {
		timestamps: false,
		tableName: 'tblCombatJoinGuild'
	});
	tblCombatJoinGuild.associate = function(models) {
		tblCombatJoinGuild.hasMany(models.guildTbl, {
			foreignKey: 'guildId',
			targetKey: 'mIdGuild',
			as: 'guild'
		})
	}
	return tblCombatJoinGuild;
};
