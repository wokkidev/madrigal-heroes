/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblEventBoardProvide', {
		account: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'account'
		},
		regdate: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: '(getdate())',
			field: 'regdate'
		}
	}, {
		tableName: 'tblEvent_Board_Provide'
	});
};
