/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('taxInfoTbl', {
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'serverindex'
		},
		nTimes: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nTimes'
		},
		nContinent: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nContinent'
		},
		dwId: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'dwID'
		},
		dwNextId: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'dwNextID'
		},
		bSetTaxRate: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'bSetTaxRate'
		},
		changeTime: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'change_time'
		},
		saveTime: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'save_time'
		}
	}, {
		tableName: 'TAX_INFO_TBL'
	});
};
