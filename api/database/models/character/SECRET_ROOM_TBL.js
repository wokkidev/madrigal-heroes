/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('secretRoomTbl', {
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'serverindex'
		},
		nTimes: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nTimes'
		},
		nContinent: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nContinent'
		},
		mIdGuild: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idGuild'
		},
		nPenya: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nPenya'
		},
		chState: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'chState'
		},
		sDate: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 's_date'
		},
		dwWorldId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwWorldID'
		},
		nWarState: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nWarState'
		},
		nKillCount: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nKillCount'
		}
	}, {
		tableName: 'SECRET_ROOM_TBL'
	});
};
