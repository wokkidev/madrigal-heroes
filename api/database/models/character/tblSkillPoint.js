/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblSkillPoint', {
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		playerId: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'PlayerID'
		},
		skillId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'SkillID'
		},
		skillLv: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'SkillLv'
		},
		skillPosition: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'SkillPosition'
		}
	}, {
		tableName: 'tblSkillPoint'
	});
};
