/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('guildCombat1To1BattleTbl', {
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		combatId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'combatID'
		},
		mIdWorld: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'm_idWorld'
		},
		startTime: {
			type: DataTypes.DATE,
			allowNull: false,
			field: 'Start_Time'
		},
		endTime: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'End_Time'
		},
		mIdGuild1st: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'm_idGuild_1st'
		},
		mIdGuild2nd: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'm_idGuild_2nd'
		},
		state: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'State'
		},
		seq: {
			type: DataTypes.BIGINT,
			allowNull: false,
			field: 'SEQ'
		}
	}, {
		tableName: 'GUILD_COMBAT_1TO1_BATTLE_TBL'
	});
};
