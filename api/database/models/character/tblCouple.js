/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblCouple', {
		cid: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
			field: 'cid'
		},
		nServer: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			field: 'nServer'
		},
		nExperience: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nExperience'
		},
		addDate: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: '(getdate())',
			field: 'add_Date'
		}
	}, {
		tableName: 'tblCouple'
	});
};
