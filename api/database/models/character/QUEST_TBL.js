/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('questTbl', {
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true,
			field: 'serverindex'
		},
		mWId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			field: 'm_wId'
		},
		mNState: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nState'
		},
		mWTime: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_wTime'
		},
		mNKillNpcNum0: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nKillNPCNum_0'
		},
		mNKillNpcNum1: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nKillNPCNum_1'
		},
		mBPatrol: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_bPatrol'
		},
		mBDialog: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_bDialog'
		},
		mBReserve3: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_bReserve3'
		},
		mBReserve4: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_bReserve4'
		},
		mBReserve5: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_bReserve5'
		},
		mBReserve6: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_bReserve6'
		},
		mBReserve7: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_bReserve7'
		},
		mBReserve8: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_bReserve8'
		}
	}, {
		tableName: 'QUEST_TBL'
	});
};
