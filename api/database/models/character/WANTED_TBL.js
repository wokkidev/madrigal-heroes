/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('wantedTbl', {
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		penya: {
			type: DataTypes.BIGINT,
			allowNull: true,
			field: 'penya'
		},
		szMsg: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'szMsg'
		},
		sDate: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 's_date'
		},
		createTime: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'CreateTime'
		}
	}, {
		tableName: 'WANTED_TBL'
	});
};
