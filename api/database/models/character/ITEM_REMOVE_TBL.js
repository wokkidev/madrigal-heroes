/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('itemRemoveTbl', {
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		itemName: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'Item_Name'
		},
		mNAbilityOption: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'm_nAbilityOption'
		},
		itemCount: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((1))',
			field: 'Item_count'
		},
		state: {
			type: DataTypes.CHAR,
			allowNull: false,
			defaultValue: 'I',
			field: 'State'
		},
		mNNo: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'm_nNo'
		},
		endTime: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'End_Time'
		},
		mBItemResist: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'm_bItemResist'
		},
		mNResistAbilityOption: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'm_nResistAbilityOption'
		},
		itemFlag: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'ItemFlag'
		},
		receiveDt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: '(getdate())',
			field: 'ReceiveDt'
		},
		deleteDt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'DeleteDt'
		},
		requestUser: {
			type: DataTypes.STRING,
			allowNull: false,
			defaultValue: '((0))',
			field: 'RequestUser'
		},
		randomOption: {
			type: DataTypes.BIGINT,
			allowNull: true,
			defaultValue: '((0))',
			field: 'RandomOption'
		}
	}, {
		tableName: 'ITEM_REMOVE_TBL'
	});
};
