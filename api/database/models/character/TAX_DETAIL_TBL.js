/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('taxDetailTbl', {
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'serverindex'
		},
		nTimes: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nTimes'
		},
		nContinent: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nContinent'
		},
		nTaxKind: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nTaxKind'
		},
		nTaxRate: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nTaxRate'
		},
		nTaxCount: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nTaxCount'
		},
		nTaxGold: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nTaxGold'
		},
		nTaxPerin: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nTaxPerin'
		},
		nNextTaxRate: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nNextTaxRate'
		}
	}, {
		tableName: 'TAX_DETAIL_TBL'
	});
};
