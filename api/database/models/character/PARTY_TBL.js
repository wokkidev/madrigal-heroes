/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('partyTbl', {
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		partyname: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'partyname'
		}
	}, {
		tableName: 'PARTY_TBL'
	});
};
