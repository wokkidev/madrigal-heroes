/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblQuiz', {
		mNIndex: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'm_nIndex'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mNQuizType: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'm_nQuizType'
		},
		mNAnswer: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nAnswer'
		},
		mChState: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'm_chState'
		},
		mSzQuestion: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_szQuestion'
		},
		sDate: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: '(getdate())',
			field: 's_date'
		},
		mItem: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_Item'
		},
		mItemCount: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_ItemCount'
		}
	}, {
		tableName: 'tblQuiz'
	});
};
