/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblPocketExt', {
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		idPlayer: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'idPlayer'
		},
		nPocket: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'nPocket'
		},
		szExt: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'szExt'
		},
		szPiercing: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'szPiercing'
		},
		szPet: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'szPet'
		}
	}, {
		tableName: 'tblPocketExt'
	});
};
