/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('viwItemInfo', {
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mInventory: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_Inventory'
		},
		mBank: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'm_Bank'
		},
		szItem0: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'szItem0'
		},
		szItem1: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'szItem1'
		},
		szItem2: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'szItem2'
		}
	}, {
		tableName: 'viw_Item_Info'
	});
};
