/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblHousing', {
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'serverindex'
		},
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		itemIndex: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'ItemIndex'
		},
		tKeepTime: {
			type: DataTypes.BIGINT,
			allowNull: true,
			field: 'tKeepTime'
		},
		bSetup: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'bSetup'
		},
		xPos: {
			type: DataTypes.FLOAT,
			allowNull: true,
			defaultValue: '((0))',
			field: 'x_Pos'
		},
		yPos: {
			type: DataTypes.FLOAT,
			allowNull: true,
			defaultValue: '((0))',
			field: 'y_Pos'
		},
		zPos: {
			type: DataTypes.FLOAT,
			allowNull: true,
			defaultValue: '((0))',
			field: 'z_Pos'
		},
		fAngle: {
			type: DataTypes.FLOAT,
			allowNull: true,
			defaultValue: '((0))',
			field: 'fAngle'
		},
		startTime: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: '(getdate())',
			field: 'Start_Time'
		},
		endTime: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'End_Time'
		}
	}, {
		tableName: 'tblHousing'
	});
};
