/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('guildBankExtTbl', {
		mIdGuild: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true,
			field: 'm_idGuild'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true,
			field: 'serverindex'
		},
		mExtGuildBank: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_extGuildBank'
		},
		mGuildBankPiercing: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_GuildBankPiercing'
		},
		szGuildBankPet: {
			type: DataTypes.STRING,
			allowNull: true,
			defaultValue: '$',
			field: 'szGuildBankPet'
		}
	}, {
		tableName: 'GUILD_BANK_EXT_TBL'
	});
};
