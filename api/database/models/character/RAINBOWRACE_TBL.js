/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('rainbowraceTbl', {
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'serverindex'
		},
		nTimes: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nTimes'
		},
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		chState: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'chState'
		},
		nRanking: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nRanking'
		},
		sDate: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 's_date'
		}
	}, {
		tableName: 'RAINBOWRACE_TBL'
	});
};
