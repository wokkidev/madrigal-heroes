/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tagTbl', {
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		fIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'f_idPlayer'
		},
		mMessage: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_Message'
		},
		state: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'State'
		},
		createTime: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: '(getdate())',
			field: 'CreateTime'
		}
	}, {
		tableName: 'TAG_TBL'
	});
};
