/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('petfilterBlacklist', {
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mIdPlayer: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'm_idPlayer'
		},
		itemId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'itemID'
		}
	}, {
		tableName: 'PETFILTER_BLACKLIST'
	});
};
