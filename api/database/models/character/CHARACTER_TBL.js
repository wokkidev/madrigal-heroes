/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	const characterTbl = sequelize.define('characterTbl', {
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer',
			primaryKey: true
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		account: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'account'
		},
		mSzName: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_szName'
		},
		playerslot: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'playerslot'
		},
		dwWorldId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwWorldID'
		},
		mDwIndex: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_dwIndex'
		},
		mVScaleX: {
			type: DataTypes.REAL,
			allowNull: true,
			field: 'm_vScale_x'
		},
		mDwMotion: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_dwMotion'
		},
		mVPosX: {
			type: DataTypes.REAL,
			allowNull: true,
			field: 'm_vPos_x'
		},
		mVPosY: {
			type: DataTypes.REAL,
			allowNull: true,
			field: 'm_vPos_y'
		},
		mVPosZ: {
			type: DataTypes.REAL,
			allowNull: true,
			field: 'm_vPos_z'
		},
		mFAngle: {
			type: DataTypes.REAL,
			allowNull: true,
			field: 'm_fAngle'
		},
		mSzCharacterKey: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_szCharacterKey'
		},
		mNHitPoint: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nHitPoint'
		},
		mNManaPoint: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nManaPoint'
		},
		mNFatiguePoint: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nFatiguePoint'
		},
		mNFuel: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((-1))',
			field: 'm_nFuel'
		},
		mDwSkinSet: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_dwSkinSet'
		},
		mDwHairMesh: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_dwHairMesh'
		},
		mDwHairColor: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_dwHairColor'
		},
		mDwHeadMesh: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_dwHeadMesh'
		},
		mDwSex: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_dwSex'
		},
		mDwRideItemIdx: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_dwRideItemIdx'
		},
		mDwGold: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_dwGold'
		},
		mNJob: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nJob'
		},
		mPActMover: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_pActMover'
		},
		mNStr: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nStr'
		},
		mNSta: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nSta'
		},
		mNDex: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nDex'
		},
		mNInt: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nInt'
		},
		mNLevel: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nLevel'
		},
		mNMaximumLevel: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nMaximumLevel'
		},
		mNExp1: {
			type: DataTypes.BIGINT,
			allowNull: true,
			field: 'm_nExp1'
		},
		mNExp2: {
			type: DataTypes.BIGINT,
			allowNull: true,
			field: 'm_nExp2'
		},
		mAJobSkill: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_aJobSkill'
		},
		mALicenseSkill: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_aLicenseSkill'
		},
		mAJobLv: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_aJobLv'
		},
		mDwExpertLv: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_dwExpertLv'
		},
		mIdMarkingWorld: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_idMarkingWorld'
		},
		mVMarkingPosX: {
			type: DataTypes.REAL,
			allowNull: true,
			field: 'm_vMarkingPos_x'
		},
		mVMarkingPosY: {
			type: DataTypes.REAL,
			allowNull: true,
			field: 'm_vMarkingPos_y'
		},
		mVMarkingPosZ: {
			type: DataTypes.REAL,
			allowNull: true,
			field: 'm_vMarkingPos_z'
		},
		mNRemainGp: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nRemainGP'
		},
		mNRemainLp: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nRemainLP'
		},
		mNFlightLv: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nFlightLv'
		},
		mNFxp: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nFxp'
		},
		mNTxp: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nTxp'
		},
		mLpQuestCntArray: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_lpQuestCntArray'
		},
		mChAuthority: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_chAuthority'
		},
		mDwMode: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_dwMode'
		},
		mIdparty: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_idparty'
		},
		mIdCompany: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idCompany'
		},
		mIdMuerderer: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_idMuerderer'
		},
		mNFame: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nFame'
		},
		mNDeathExp: {
			type: DataTypes.BIGINT,
			allowNull: true,
			field: 'm_nDeathExp'
		},
		mNDeathLevel: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nDeathLevel'
		},
		mDwFlyTime: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_dwFlyTime'
		},
		mNMessengerState: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nMessengerState'
		},
		blockby: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'blockby'
		},
		totalPlayTime: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'TotalPlayTime'
		},
		isblock: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'isblock'
		},
		endTime: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'End_Time'
		},
		blockTime: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'BlockTime'
		},
		createTime: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'CreateTime'
		},
		mTmAccFuel: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_tmAccFuel'
		},
		mTGuildMember: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_tGuildMember'
		},
		mDwSkillPoint: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_dwSkillPoint'
		},
		mACompleteQuest: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_aCompleteQuest'
		},
		mDwReturnWorldId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_dwReturnWorldID'
		},
		mVReturnPosX: {
			type: DataTypes.REAL,
			allowNull: true,
			field: 'm_vReturnPos_x'
		},
		mVReturnPosY: {
			type: DataTypes.REAL,
			allowNull: true,
			field: 'm_vReturnPos_y'
		},
		mVReturnPosZ: {
			type: DataTypes.REAL,
			allowNull: true,
			field: 'm_vReturnPos_z'
		},
		multiServer: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'MultiServer'
		},
		mSkillPoint: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_SkillPoint'
		},
		mSkillLv: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_SkillLv'
		},
		mSkillExp: {
			type: DataTypes.BIGINT,
			allowNull: true,
			field: 'm_SkillExp'
		},
		dwEventFlag: {
			type: DataTypes.BIGINT,
			allowNull: true,
			defaultValue: '((0))',
			field: 'dwEventFlag'
		},
		dwEventTime: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'dwEventTime'
		},
		dwEventElapsed: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'dwEventElapsed'
		},
		pkValue: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'PKValue'
		},
		pkPropensity: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'PKPropensity'
		},
		pkExp: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'PKExp'
		},
		angelExp: {
			type: DataTypes.BIGINT,
			allowNull: true,
			defaultValue: '((0))',
			field: 'AngelExp'
		},
		angelLevel: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'AngelLevel'
		},
		finalLevelDt: {
			type: "SMALLDATETIME",
			allowNull: false,
			defaultValue: '2000-01-01',
			field: 'FinalLevelDt'
		},
		mDwPetId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((-1))',
			field: 'm_dwPetId'
		},
		mNExpLog: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'm_nExpLog'
		},
		mNAngelExpLog: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'm_nAngelExpLog'
		},
		mNCoupon: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'm_nCoupon'
		},
		mNHonor: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((-1))',
			field: 'm_nHonor'
		},
		mNLayer: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'm_nLayer'
		},
		mNCampusPoint: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'm_nCampusPoint'
		},
		idCampus: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'idCampus'
		},
		mACheckedQuest: {
			type: DataTypes.STRING,
			allowNull: false,
			defaultValue: '$',
			field: 'm_aCheckedQuest'
		},
		mNPetFilter: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nPetFilter'
		}
	}, {
		timestamps: false,
		tableName: 'CHARACTER_TBL'
	});
	characterTbl.associate = function(models) {
		characterTbl.belongsTo(models.guildMemberTbl, {
			foreignKey: 'mIdPlayer',
			as: 'guildMember'
		});
		characterTbl.belongsTo(models.inventoryTbl, {
			foreignKey: 'mIdPlayer',
			as: 'inventory'
		});
	};
	return characterTbl;
};
