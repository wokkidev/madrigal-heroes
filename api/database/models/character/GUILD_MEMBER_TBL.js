/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	const guildMemberTbl = sequelize.define('guildMemberTbl', {
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer',
			primaryKey: true
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mIdGuild: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'm_idGuild'
		},
		mSzAlias: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_szAlias'
		},
		mNWin: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nWin'
		},
		mNLose: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nLose'
		},
		mNSurrender: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nSurrender'
		},
		mNMemberLv: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nMemberLv'
		},
		mNGiveGold: {
			type: DataTypes.BIGINT,
			allowNull: true,
			field: 'm_nGiveGold'
		},
		mNGivePxp: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nGivePxp'
		},
		mIdWar: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_idWar'
		},
		mIdVote: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_idVote'
		},
		isuse: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'isuse'
		},
		createTime: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'CreateTime'
		},
		mNClass: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nClass'
		}
	}, {
		timestamps: false,
		tableName: 'GUILD_MEMBER_TBL'
	});
	guildMemberTbl.associate = function(models) {
		guildMemberTbl.belongsTo(models.characterTbl, {
			foreignKey: 'mIdPlayer',
			as: 'player'
		});
		guildMemberTbl.belongsTo(models.guildTbl, {
			foreignKey: 'mIdGuild',
			as: 'guild'
		})
	}
	return guildMemberTbl;
};
