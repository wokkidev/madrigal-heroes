/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblHousingVisit', {
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'serverindex'
		},
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		fIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'f_idPlayer'
		}
	}, {
		tableName: 'tblHousing_Visit'
	});
};
