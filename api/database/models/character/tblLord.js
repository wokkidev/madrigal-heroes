/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	const tblLord = sequelize.define('tblLord', {
		nServer: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'nServer',
			primaryKey: true
		},
		idElection: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'idElection'
		},
		idLord: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'idLord'
		},
		sDate: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: '(getdate())',
			field: 's_date'
		}
	}, {
		timestamps: false,
		tableName: 'tblLord'
	});
	return tblLord;
};
