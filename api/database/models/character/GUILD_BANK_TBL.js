/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('guildBankTbl', {
		mIdGuild: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true,
			field: 'm_idGuild'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true,
			field: 'serverindex'
		},
		mApIndex: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_apIndex'
		},
		mDwObjIndex: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_dwObjIndex'
		},
		mGuildBank: {
			type: DataTypes.TEXT,
			allowNull: true,
			field: 'm_GuildBank'
		}
	}, {
		tableName: 'GUILD_BANK_TBL'
	});
};
