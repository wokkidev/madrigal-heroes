/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('characterTblDel', {
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'serverindex'
		},
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		mSzName: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_szName'
		},
		account: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'account'
		},
		mNLevel: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nLevel'
		},
		mNJob: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nJob'
		},
		createTime: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'CreateTime'
		},
		deldate: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'deldate'
		}
	}, {
		tableName: 'CHARACTER_TBL_DEL'
	});
};
