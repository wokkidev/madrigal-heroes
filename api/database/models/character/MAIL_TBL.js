/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('mailTbl', {
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true,
			field: 'serverindex'
		},
		nMail: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			field: 'nMail'
		},
		idReceiver: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'idReceiver'
		},
		idSender: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'idSender'
		},
		nGold: {
			type: DataTypes.BIGINT,
			allowNull: false,
			field: 'nGold'
		},
		tmCreate: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'tmCreate'
		},
		byRead: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'byRead'
		},
		szTitle: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'szTitle'
		},
		szText: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'szText'
		},
		dwItemId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'dwItemId'
		},
		nItemNum: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'nItemNum'
		},
		nRepairNumber: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'nRepairNumber'
		},
		nHitPoint: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'nHitPoint'
		},
		nMaxHitPoint: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'nMaxHitPoint'
		},
		nMaterial: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'nMaterial'
		},
		byFlag: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'byFlag'
		},
		dwSerialNumber: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'dwSerialNumber'
		},
		nOption: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'nOption'
		},
		bItemResist: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'bItemResist'
		},
		nResistAbilityOption: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'nResistAbilityOption'
		},
		idGuild: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'idGuild'
		},
		nResistSmItemId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'nResistSMItemId'
		},
		bCharged: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'bCharged'
		},
		dwKeepTime: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'dwKeepTime'
		},
		nRandomOptItemId: {
			type: DataTypes.BIGINT,
			allowNull: true,
			field: 'nRandomOptItemId'
		},
		nPiercedSize: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'nPiercedSize'
		},
		dwItemId1: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'dwItemId1'
		},
		dwItemId2: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'dwItemId2'
		},
		dwItemId3: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'dwItemId3'
		},
		dwItemId4: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'dwItemId4'
		},
		sendDt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'SendDt'
		},
		readDt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'ReadDt'
		},
		getGoldDt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'GetGoldDt'
		},
		deleteDt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'DeleteDt'
		},
		itemFlag: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'ItemFlag'
		},
		itemReceiveDt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'ItemReceiveDt'
		},
		goldFlag: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'GoldFlag'
		},
		bPet: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'bPet'
		},
		nKind: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'nKind'
		},
		nLevel: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'nLevel'
		},
		dwExp: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'dwExp'
		},
		wEnergy: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'wEnergy'
		},
		wLife: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'wLife'
		},
		anAvailLevelD: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'anAvailLevel_D'
		},
		anAvailLevelC: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'anAvailLevel_C'
		},
		anAvailLevelB: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'anAvailLevel_B'
		},
		anAvailLevelA: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'anAvailLevel_A'
		},
		anAvailLevelS: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'anAvailLevel_S'
		},
		dwItemId5: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'dwItemId5'
		},
		dwItemId6: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'dwItemId6'
		},
		dwItemId7: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'dwItemId7'
		},
		dwItemId8: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'dwItemId8'
		},
		dwItemId9: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'dwItemId9'
		},
		dwItemId10: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'dwItemId10'
		},
		dwItemId11: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'dwItemId11'
		},
		dwItemId12: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'dwItemId12'
		},
		dwItemId13: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'dwItemId13'
		},
		dwItemId14: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'dwItemId14'
		},
		dwItemId15: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'dwItemId15'
		},
		nPiercedSize2: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'nPiercedSize2'
		},
		szPetName: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'szPetName'
		}
	}, {
		tableName: 'MAIL_TBL'
	});
};
