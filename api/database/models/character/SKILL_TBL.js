/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('skillTbl', {
		index: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'Index'
		},
		szName: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'szName'
		},
		job: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'job'
		}
	}, {
		tableName: 'SKILL_TBL'
	});
};
