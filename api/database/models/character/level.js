/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('level', {
		col001: {
			type: DataTypes.FLOAT,
			allowNull: true,
			field: 'Col001'
		},
		col002: {
			type: DataTypes.FLOAT,
			allowNull: true,
			field: 'Col002'
		}
	}, {
		tableName: 'level'
	});
};
