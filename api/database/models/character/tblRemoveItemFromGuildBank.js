/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblRemoveItemFromGuildBank', {
		nNum: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'nNum'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		idGuild: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'idGuild'
		},
		itemIndex: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'ItemIndex'
		},
		itemSerialNum: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'ItemSerialNum'
		},
		itemCnt: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'ItemCnt'
		},
		deleteRequestCnt: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'DeleteRequestCnt'
		},
		deleteCnt: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'DeleteCnt'
		},
		itemFlag: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'ItemFlag'
		},
		registerDt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: '(getdate())',
			field: 'RegisterDt'
		},
		deleteDt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'DeleteDt'
		}
	}, {
		tableName: 'tblRemoveItemFromGuildBank'
	});
};
