/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	const inventoryTbl = sequelize.define('inventoryTbl', {
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer',
			primaryKey: true
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mInventory: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_Inventory'
		},
		mApIndex: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_apIndex'
		},
		mAdwEquipment: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_adwEquipment'
		},
		mDwObjIndex: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_dwObjIndex'
		}
	}, {
		timestamps: false,
		tableName: 'INVENTORY_TBL'
	});
	inventoryTbl.associate = function(models) {
		inventoryTbl.belongsTo(models.characterTbl, {
			foreignKey: 'mIdPlayer',
			as: 'player'
		});
	}
	return inventoryTbl;
};
