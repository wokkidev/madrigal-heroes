/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblElection', {
		sWeek: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 's_week'
		},
		chrcount: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'chrcount'
		}
	}, {
		tableName: 'tblElection'
	});
};
