/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('guildCombat1To1TenderTbl', {
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		combatId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'combatID'
		},
		mIdGuild: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'm_idGuild'
		},
		mNPenya: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'm_nPenya'
		},
		sDate: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: '(getdate())',
			field: 's_date'
		},
		state: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'State'
		},
		seq: {
			type: DataTypes.BIGINT,
			allowNull: false,
			field: 'SEQ'
		}
	}, {
		tableName: 'GUILD_COMBAT_1TO1_TENDER_TBL'
	});
};
