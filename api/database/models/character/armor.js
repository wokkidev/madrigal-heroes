/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('armor', {
		armor: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'armor'
		},
		rid: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'rid'
		}
	}, {
		tableName: 'armor'
	});
};
