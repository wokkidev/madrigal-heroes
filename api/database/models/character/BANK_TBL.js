/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('bankTbl', {
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mBankPw: {
			type: DataTypes.CHAR,
			allowNull: false,
			defaultValue: '0000',
			field: 'm_BankPw'
		},
		mBank: {
			type: DataTypes.STRING,
			allowNull: false,
			defaultValue: '$',
			field: 'm_Bank'
		},
		mApIndexBank: {
			type: DataTypes.STRING,
			allowNull: false,
			defaultValue: '$',
			field: 'm_apIndex_Bank'
		},
		mDwObjIndexBank: {
			type: DataTypes.STRING,
			allowNull: false,
			defaultValue: '$',
			field: 'm_dwObjIndex_Bank'
		},
		mDwGoldBank: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'm_dwGoldBank'
		}
	}, {
		tableName: 'BANK_TBL'
	});
};
