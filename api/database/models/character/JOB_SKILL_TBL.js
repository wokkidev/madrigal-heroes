/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('jobSkillTbl', {
		mNJob: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'm_nJob'
		},
		skillId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'SkillID'
		},
		skillPosition: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'SkillPosition'
		}
	}, {
		tableName: 'JOB_SKILL_TBL'
	});
};
