/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblLordEvent', {
		nServer: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'nServer'
		},
		idPlayer: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'idPlayer'
		},
		nTick: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'nTick'
		},
		fEFactor: {
			type: DataTypes.FLOAT,
			allowNull: false,
			defaultValue: '((0.0))',
			field: 'fEFactor'
		},
		fIFactor: {
			type: DataTypes.FLOAT,
			allowNull: false,
			defaultValue: '((0.0))',
			field: 'fIFactor'
		},
		sDate: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: '(getdate())',
			field: 's_date'
		}
	}, {
		tableName: 'tblLordEvent'
	});
};
