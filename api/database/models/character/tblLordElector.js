/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblLordElector', {
		nServer: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'nServer'
		},
		idElection: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'idElection'
		},
		idElector: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'idElector'
		},
		sDate: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: '(getdate())',
			field: 's_date'
		}
	}, {
		tableName: 'tblLordElector'
	});
};
