/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblAccountList', {
		account: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'account'
		},
		rid: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'rid'
		}
	}, {
		tableName: 'tblAccountList'
	});
};
