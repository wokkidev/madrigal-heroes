/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblRestPoint', {
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true,
			field: 'serverindex'
		},
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true,
			field: 'm_idPlayer'
		},
		mNRestPoint: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nRestPoint'
		},
		mLogOutTime: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_LogOutTime'
		}
	}, {
		tableName: 'tblRestPoint'
	});
};
