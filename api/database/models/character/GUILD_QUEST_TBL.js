/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('guildQuestTbl', {
		mIdGuild: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true,
			field: 'm_idGuild'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true,
			field: 'serverindex'
		},
		nId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'n_Id'
		},
		nState: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nState'
		},
		sDate: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 's_date'
		},
		eDate: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'e_date'
		}
	}, {
		tableName: 'GUILD_QUEST_TBL'
	});
};
