/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('guildCombat1To1BattlePersonTbl', {
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		combatId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'combatID'
		},
		mIdGuild: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'm_idGuild'
		},
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'm_idPlayer'
		},
		mNSeq: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nSeq'
		},
		startTime: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'Start_Time'
		},
		endTime: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'End_Time'
		},
		state: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'State'
		}
	}, {
		tableName: 'GUILD_COMBAT_1TO1_BATTLE_PERSON_TBL'
	});
};
