/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblCouplePlayerDeleted', {
		nServer: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nServer'
		},
		idPlayer: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'idPlayer'
		},
		cid: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'cid'
		}
	}, {
		tableName: 'tblCouplePlayer_deleted'
	});
};
