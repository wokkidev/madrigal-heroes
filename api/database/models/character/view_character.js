/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('viewCharacter', {
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mInventory: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_Inventory'
		},
		mBank: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'm_Bank'
		},
		szItem: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'szItem'
		}
	}, {
		tableName: 'view_character'
	});
};
