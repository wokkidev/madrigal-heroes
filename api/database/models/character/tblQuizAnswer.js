/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblQuizAnswer', {
		mNIndex: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			field: 'm_nIndex'
		},
		mAnswer1: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_Answer1'
		},
		mAnswer2: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_Answer2'
		},
		mAnswer3: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_Answer3'
		},
		mAnswer4: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_Answer4'
		}
	}, {
		tableName: 'tblQuizAnswer'
	});
};
