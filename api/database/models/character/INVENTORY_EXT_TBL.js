/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('inventoryExtTbl', {
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mExtInventory: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_extInventory'
		},
		mInventoryPiercing: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_InventoryPiercing'
		},
		szInventoryPet: {
			type: DataTypes.STRING,
			allowNull: true,
			defaultValue: '$',
			field: 'szInventoryPet'
		}
	}, {
		tableName: 'INVENTORY_EXT_TBL'
	});
};
