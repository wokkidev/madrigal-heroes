/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tbValentineEvent', {
		prodate: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'Prodate'
		},
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'm_idPlayer'
		},
		regdate: {
			type: DataTypes.DATE,
			allowNull: false,
			field: 'regdate'
		}
	}, {
		tableName: 'TB_VALENTINE_EVENT'
	});
};
