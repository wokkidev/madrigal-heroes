/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('skillinfluenceTbl', {
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		skillInfluence: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'SkillInfluence'
		}
	}, {
		tableName: 'SKILLINFLUENCE_TBL'
	});
};
