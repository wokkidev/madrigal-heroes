/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	const guildTbl = sequelize.define('guildTbl', {
		mIdGuild: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'm_idGuild',
			primaryKey: true
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		lv1: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'Lv_1'
		},
		lv2: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'Lv_2'
		},
		lv3: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'Lv_3'
		},
		lv4: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'Lv_4'
		},
		pay0: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'Pay_0'
		},
		pay1: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'Pay_1'
		},
		pay2: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'Pay_2'
		},
		pay3: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'Pay_3'
		},
		pay4: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'Pay_4'
		},
		mSzGuild: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_szGuild'
		},
		mNLevel: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nLevel'
		},
		mNGuildGold: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nGuildGold'
		},
		mNGuildPxp: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nGuildPxp'
		},
		mNWin: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nWin'
		},
		mNLose: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nLose'
		},
		mNSurrender: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nSurrender'
		},
		mNWinPoint: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nWinPoint'
		},
		mDwLogo: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_dwLogo'
		},
		mSzNotice: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_szNotice'
		},
		isuse: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'isuse'
		},
		createTime: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'CreateTime'
		}
	}, {
		timestamps: false,
		tableName: 'GUILD_TBL'
	});
	guildTbl.associate = function(models) {
		guildTbl.hasMany(models.tblCombatJoinGuild, {
			foreignKey: 'mIdGuild',
			targetKey: 'guildId',
			as: 'combats'
		});
		guildTbl.hasMany(models.guildMemberTbl, {
			foreignKey: 'mIdGuild',
			as: 'member'
		});
	}
	return guildTbl;
};
