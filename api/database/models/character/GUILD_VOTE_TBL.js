/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('guildVoteTbl', {
		mIdGuild: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true,
			field: 'm_idGuild'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true,
			field: 'serverindex'
		},
		mIdVote: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_idVote'
		},
		mCbStatus: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_cbStatus'
		},
		mSzTitle: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_szTitle'
		},
		mSzQuestion: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_szQuestion'
		},
		mSzString1: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_szString1'
		},
		mSzString2: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_szString2'
		},
		mSzString3: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_szString3'
		},
		mSzString4: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_szString4'
		},
		mCbCount1: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_cbCount1'
		},
		mCbCount2: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_cbCount2'
		},
		mCbCount3: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_cbCount3'
		},
		mCbCount4: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_cbCount4'
		},
		createTime: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'CreateTime'
		}
	}, {
		tableName: 'GUILD_VOTE_TBL'
	});
};
