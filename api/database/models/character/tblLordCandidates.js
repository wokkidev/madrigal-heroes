/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblLordCandidates', {
		nServer: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'nServer'
		},
		idElection: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'idElection'
		},
		idPlayer: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'idPlayer'
		},
		iDeposit: {
			type: DataTypes.BIGINT,
			allowNull: false,
			defaultValue: '((0))',
			field: 'iDeposit'
		},
		nVote: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'nVote'
		},
		szPledge: {
			type: DataTypes.STRING,
			allowNull: false,
			defaultValue: '',
			field: 'szPledge'
		},
		isLeftOut: {
			type: DataTypes.CHAR,
			allowNull: false,
			defaultValue: 'F',
			field: 'IsLeftOut'
		},
		sDate: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: '(getdate())',
			field: 's_date'
		},
		tCreate: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'tCreate'
		}
	}, {
		tableName: 'tblLordCandidates'
	});
};
