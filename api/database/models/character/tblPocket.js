/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblPocket', {
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		idPlayer: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'idPlayer'
		},
		nPocket: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'nPocket'
		},
		szItem: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'szItem'
		},
		szIndex: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'szIndex'
		},
		szObjIndex: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'szObjIndex'
		},
		bExpired: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'bExpired'
		},
		tExpirationDate: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'tExpirationDate'
		}
	}, {
		tableName: 'tblPocket'
	});
};
