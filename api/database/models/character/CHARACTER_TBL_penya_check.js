/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('characterTblPenyaCheck', {
		account: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'account'
		},
		mSzName: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_szName'
		},
		mDwGold: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_dwGold'
		},
		sDate: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 's_date'
		},
		checkSec: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'check_sec'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'serverindex'
		}
	}, {
		tableName: 'CHARACTER_TBL_penya_check'
	});
};
