/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('characterTblValidityCheck', {
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'serverindex'
		},
		account: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'account'
		},
		mSzName: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_szName'
		},
		totalPlayTime: {
			type: DataTypes.BIGINT,
			allowNull: true,
			field: 'TotalPlayTime'
		},
		mDwGold: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_dwGold'
		},
		mNLevel: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nLevel'
		},
		mNJob: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nJob'
		},
		sumAbility: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'sum_ability'
		},
		createTime: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'CreateTime'
		},
		regdate: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: '(getdate())',
			field: 'regdate'
		}
	}, {
		tableName: 'CHARACTER_TBL_validity_check'
	});
};
