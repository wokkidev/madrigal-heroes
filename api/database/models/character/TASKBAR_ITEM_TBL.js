/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('taskbarItemTbl', {
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mASlotItem: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_aSlotItem'
		}
	}, {
		tableName: 'TASKBAR_ITEM_TBL'
	});
};
