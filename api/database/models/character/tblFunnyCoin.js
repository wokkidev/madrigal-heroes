/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblFunnyCoin', {
		fcid: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
			field: 'fcid'
		},
		account: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'account'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'm_idPlayer'
		},
		itemName: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'Item_Name'
		},
		itemCash: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'Item_Cash'
		},
		itemUniqueNo: {
			type: DataTypes.BIGINT,
			allowNull: false,
			field: 'Item_UniqueNo'
		},
		sDate: {
			type: DataTypes.DATE,
			allowNull: false,
			field: 's_date'
		},
		fid: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'fid'
		},
		itemFlag: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'ItemFlag'
		},
		fDate: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'f_date'
		}
	}, {
		tableName: 'tblFunnyCoin'
	});
};
