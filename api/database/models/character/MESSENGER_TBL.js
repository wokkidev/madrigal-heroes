/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('messengerTbl', {
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		fIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'f_idPlayer'
		},
		mNJob: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nJob'
		},
		mDwSex: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_dwSex'
		},
		state: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'State'
		},
		createTime: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'CreateTime'
		},
		mDwState: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_dwState'
		}
	}, {
		tableName: 'MESSENGER_TBL'
	});
};
