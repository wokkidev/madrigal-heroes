/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblCampusMember', {
		idCampus: {
			type: DataTypes.INTEGER,
			allowNull: false,
			references: {
				model: 'tblCampus',
				key: 'serverindex'
			},
			field: 'idCampus'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			references: {
				model: 'tblCampus',
				key: 'serverindex'
			},
			field: 'serverindex'
		},
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true,
			field: 'm_idPlayer'
		},
		nMemberLv: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nMemberLv'
		},
		createTime: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: '(getdate())',
			field: 'CreateTime'
		}
	}, {
		tableName: 'tblCampusMember'
	});
};
