/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblCombatJoinPlayer', {
		combatId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			references: {
				model: 'tblCombatJoinGuild',
				key: 'serverindex'
			},
			field: 'CombatID'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true,
			references: {
				model: 'tblCombatJoinGuild',
				key: 'serverindex'
			},
			field: 'serverindex'
		},
		guildId: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true,
			references: {
				model: 'tblCombatJoinGuild',
				key: 'serverindex'
			},
			field: 'GuildID'
		},
		playerId: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true,
			field: 'PlayerID'
		},
		status: {
			type: DataTypes.STRING,
			allowNull: false,
			defaultValue: '30',
			field: 'Status'
		},
		point: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'Point'
		},
		reward: {
			type: DataTypes.BIGINT,
			allowNull: false,
			defaultValue: '((0))',
			field: 'Reward'
		},
		rewardDt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'RewardDt'
		}
	}, {
		timestamps: false,
		tableName: 'tblCombatJoinPlayer'
	});
};
