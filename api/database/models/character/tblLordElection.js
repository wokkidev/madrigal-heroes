/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblLordElection', {
		nServer: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'nServer'
		},
		idElection: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'idElection'
		},
		eState: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'eState'
		},
		sDate: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: '(getdate())',
			field: 's_date'
		},
		szBegin: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'szBegin'
		}
	}, {
		tableName: 'tblLordElection'
	});
};
