/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('bilingItemTbl', {
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mDwSmTime: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_dwSMTime'
		}
	}, {
		tableName: 'BILING_ITEM_TBL'
	});
};
