/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblCouplePlayer', {
		cid: {
			type: DataTypes.INTEGER,
			allowNull: false,
			references: {
				model: 'tblCouple',
				key: 'nServer'
			},
			field: 'cid'
		},
		nServer: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'tblCouple',
				key: 'nServer'
			},
			field: 'nServer'
		},
		idPlayer: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'idPlayer'
		}
	}, {
		tableName: 'tblCouplePlayer'
	});
};
