/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblLordSkill', {
		nServer: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'nServer'
		},
		nSkill: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'nSkill'
		},
		nTick: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'nTick'
		},
		sDate: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: '(getdate())',
			field: 's_date'
		}
	}, {
		tableName: 'tblLordSkill'
	});
};
