/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblCampus', {
		idCampus: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			field: 'idCampus'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true,
			field: 'serverindex'
		},
		createTime: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: '(getdate())',
			field: 'CreateTime'
		}
	}, {
		tableName: 'tblCampus'
	});
};
