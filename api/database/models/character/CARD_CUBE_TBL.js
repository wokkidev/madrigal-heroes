/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('cardCubeTbl', {
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mCard: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_Card'
		},
		mCube: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_Cube'
		},
		mApIndexCard: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_apIndex_Card'
		},
		mDwObjIndexCard: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_dwObjIndex_Card'
		},
		mApIndexCube: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_apIndex_Cube'
		},
		mDwObjIndexCube: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_dwObjIndex_Cube'
		}
	}, {
		tableName: 'CARD_CUBE_TBL'
	});
};
