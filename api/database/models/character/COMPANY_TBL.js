/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('companyTbl', {
		mIdCompany: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'm_idCompany'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mSzCompany: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_szCompany'
		},
		mLeaderid: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_leaderid'
		},
		isuse: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'isuse'
		}
	}, {
		tableName: 'COMPANY_TBL'
	});
};
