/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('secretRoomMemberTbl', {
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'serverindex'
		},
		nTimes: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nTimes'
		},
		nContinent: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nContinent'
		},
		mIdGuild: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idGuild'
		},
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		}
	}, {
		tableName: 'SECRET_ROOM_MEMBER_TBL'
	});
};
