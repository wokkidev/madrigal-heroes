/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblCoupleDeleted', {
		cid: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'cid'
		},
		nServer: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nServer'
		},
		nExperience: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nExperience'
		},
		addDate: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'add_Date'
		},
		delDate: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: '(getdate())',
			field: 'del_Date'
		}
	}, {
		tableName: 'tblCouple_deleted'
	});
};
