/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblLogoutPenyaDiffLog', {
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'serverindex'
		},
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		mDwGoldOld: {
			type: DataTypes.BIGINT,
			allowNull: true,
			field: 'm_dwGold_old'
		},
		regdateOld: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'regdate_old'
		},
		mDwGoldNow: {
			type: DataTypes.BIGINT,
			allowNull: true,
			field: 'm_dwGold_now'
		},
		regdateNow: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: '(getdate())',
			field: 'regdate_now'
		},
		seq: {
			type: DataTypes.BIGINT,
			allowNull: false,
			field: 'SEQ'
		}
	}, {
		tableName: 'tblLogout_Penya_Diff_Log'
	});
};
