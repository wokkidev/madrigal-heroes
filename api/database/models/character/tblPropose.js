/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblPropose', {
		nServer: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nServer'
		},
		idProposer: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'idProposer'
		},
		tPropose: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'tPropose'
		}
	}, {
		tableName: 'tblPropose'
	});
};
