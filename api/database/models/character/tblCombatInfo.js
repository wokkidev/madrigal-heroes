/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblCombatInfo', {
		combatId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			field: 'CombatID'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true,
			field: 'serverindex'
		},
		status: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'Status'
		},
		startDt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'StartDt'
		},
		endDt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'EndDt'
		},
		comment: {
			type: DataTypes.STRING,
			allowNull: false,
			defaultValue: '',
			field: 'Comment'
		},
		seq: {
			type: DataTypes.BIGINT,
			allowNull: false,
			field: 'SEQ'
		}
	}, {
		timestamps: false,
		tableName: 'tblCombatInfo'
	});
};
