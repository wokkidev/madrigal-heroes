/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('evidance', {
		account: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'account'
		},
		gamecode: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'gamecode'
		},
		tester: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'tester'
		},
		mChLoginAuthority: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'm_chLoginAuthority'
		},
		regdate: {
			type: DataTypes.DATE,
			allowNull: false,
			field: 'regdate'
		},
		blockTime: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'BlockTime'
		},
		endTime: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'EndTime'
		},
		webTime: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'WebTime'
		},
		isuse: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'isuse'
		},
		secession: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'secession'
		}
	}, {
		tableName: 'EVIDANCE'
	});
};
