/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('viwCharacterItem', {
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mSzName: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_szName'
		},
		mNLevel: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nLevel'
		},
		mInventory: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_Inventory'
		},
		mExtInventory: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_extInventory'
		},
		mBank: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'm_Bank'
		},
		mExtBank: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_extBank'
		}
	}, {
		tableName: 'viwCharacterItem'
	});
};
