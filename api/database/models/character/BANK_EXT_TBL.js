/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('bankExtTbl', {
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mExtBank: {
			type: DataTypes.STRING,
			allowNull: true,
			defaultValue: '$',
			field: 'm_extBank'
		},
		mBankPiercing: {
			type: DataTypes.STRING,
			allowNull: true,
			defaultValue: '$',
			field: 'm_BankPiercing'
		},
		szBankPet: {
			type: DataTypes.STRING,
			allowNull: true,
			defaultValue: '$',
			field: 'szBankPet'
		}
	}, {
		tableName: 'BANK_EXT_TBL'
	});
};
