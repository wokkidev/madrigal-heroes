/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('itemTbl', {
		index: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'Index',
			primaryKey: true
		},
		szName: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'szName'
		},
		dwPackMax: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwPackMax'
		},
		dwItemKind1: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwItemKind1'
		},
		dwItemKind2: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwItemKind2'
		},
		dwItemKind3: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwItemKind3'
		},
		dwItemJob: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwItemJob'
		},
		dwItemSex: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwItemSex'
		},
		dwCost: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwCost'
		},
		dwHanded: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwHanded'
		},
		dwFlag: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwFlag'
		},
		dwParts: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwParts'
		},
		dwPartsub: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwPartsub'
		},
		dwExclusive: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwExclusive'
		},
		dwItemLv: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwItemLV'
		},
		dwItemRare: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwItemRare'
		},
		dwShopAble: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwShopAble'
		},
		bCharged: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'bCharged'
		},
		dwAbilityMin: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwAbilityMin'
		},
		dwAbilityMax: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwAbilityMax'
		},
		eItemType: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'eItemType'
		},
		dwWeaponType: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwWeaponType'
		},
		nAdjHitRate: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nAdjHitRate'
		},
		fAttackSpeed: {
			type: DataTypes.REAL,
			allowNull: true,
			field: 'fAttackSpeed'
		},
		dwAttackRange: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwAttackRange'
		},
		nProbability: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nProbability'
		},
		dwDestParam1: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwDestParam1'
		},
		dwDestParam2: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwDestParam2'
		},
		dwDestParam3: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwDestParam3'
		},
		nAdjParamVal1: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nAdjParamVal1'
		},
		nAdjParamVal2: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nAdjParamVal2'
		},
		nAdjParamVal3: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nAdjParamVal3'
		},
		dwChgParamVal1: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwChgParamVal1'
		},
		dwChgParamVal2: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwChgParamVal2'
		},
		dwChgParamVal3: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwChgParamVal3'
		},
		nDestData11: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nDestData11'
		},
		nDestData12: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nDestData12'
		},
		nDestData13: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nDestData13'
		},
		dwReqMp: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwReqMp'
		},
		dwReqFp: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwReqFp'
		},
		dwReqDisLv: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwReqDisLV'
		},
		dwCircleTime: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwCircleTime'
		},
		dwSkillTime: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwSkillTime'
		},
		dwReferStat1: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwReferStat1'
		},
		dwReferStat2: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwReferStat2'
		},
		dwReferTarget1: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwReferTarget1'
		},
		dwReferTarget2: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwReferTarget2'
		},
		dwReferValue1: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwReferValue1'
		},
		dwReferValue2: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwReferValue2'
		},
		dwSkillType: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwSkillType'
		},
		nItemResistElecricity: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nItemResistElecricity'
		},
		nItemResistFire: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nItemResistFire'
		},
		nItemResistWind: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nItemResistWind'
		},
		nItemResistWater: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nItemResistWater'
		},
		nItemResistEarth: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nItemResistEarth'
		},
		dwExp: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwExp'
		},
		fFlightSpeed: {
			type: DataTypes.REAL,
			allowNull: true,
			field: 'fFlightSpeed'
		},
		dwFlightLimit: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwFlightLimit'
		},
		dwFFuelReMax: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwFFuelReMax'
		},
		dwAFuelReMax: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwAFuelReMax'
		},
		dwLimitLevel1: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwLimitLevel1'
		},
		szIcon: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'szIcon'
		},
		szCommand: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'szCommand'
		}
	}, {
		timestamps: false,
		tableName: 'ITEM_TBL'
	});
};
