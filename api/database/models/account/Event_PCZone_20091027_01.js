/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('eventPcZone2009102701', {
		seq: {
			type: DataTypes.BIGINT,
			allowNull: false,
			field: 'SEQ'
		},
		idNo1: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'id_no1'
		},
		idNo2: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'id_no2'
		},
		account: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'account'
		},
		sDate: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 's_date'
		},
		regIp: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'RegIP'
		},
		giftItem: {
			type: DataTypes.BOOLEAN,
			allowNull: false,
			field: 'Gift_Item'
		},
		giftDate: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'Gift_Date'
		}
	}, {
		tableName: 'Event_PCZone_20091027_01'
	});
};
