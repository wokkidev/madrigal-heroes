/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('pcZoneIp', {
		ipid: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'IPID'
		},
		pcZoneId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'PCZoneID'
		},
		ipFrom1: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'IPFrom1'
		},
		ipFrom2: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'IPFrom2'
		},
		ipFrom3: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'IPFrom3'
		},
		ipFrom4: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'IPFrom4'
		},
		ipTo4: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'IPTo4'
		},
		isUse: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'IsUse'
		},
		regDate: {
			type: DataTypes.DATE,
			allowNull: false,
			field: 'RegDate'
		},
		operId: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'OperID'
		},
		operDate: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'OperDate'
		}
	}, {
		tableName: 'PCZoneIP'
	});
};
