/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	const accountTblDetail = sequelize.define('accountTblDetail', {
		account: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'account',
			primaryKey: true
		},
		gamecode: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'gamecode'
		},
		tester: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'tester'
		},
		mChLoginAuthority: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'm_chLoginAuthority'
		},
		regdate: {
			type: DataTypes.DATE,
			allowNull: false,
			field: 'regdate'
		},
		blockTime: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'BlockTime'
		},
		endTime: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'EndTime'
		},
		webTime: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'WebTime'
		},
		isuse: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'isuse'
		},
		secession: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'secession'
		},
		email: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'email'
		},
		lastmac: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'lastmac'
		}
	}, {
		timestamps: false,
		tableName: 'ACCOUNT_TBL_DETAIL'
	});
	accountTblDetail.associate = function(models) {
		accountTblDetail.belongsTo(models.accountTbl, {
			foreignKey: 'account',
			as: 'acc'
		});
		accountTblDetail.belongsTo(models.macBanTbl, {
			foreignKey: 'lastmac',
			as: 'ban'
		});
	};
	return accountTblDetail;
};
