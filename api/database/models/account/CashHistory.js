/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('cashHistory', {
		account: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'account'
		},
		beforeCash: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'beforeCash'
		},
		afterCash: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'afterCash'
		},
		regdate: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: '(getdate())',
			field: 'regdate'
		}
	}, {
		tableName: 'CashHistory'
	});
};
