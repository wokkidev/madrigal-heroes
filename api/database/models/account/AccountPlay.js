/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	const accountPlay = sequelize.define('accountPlay', {
		account: {
			type: DataTypes.STRING,
			allowNull: false,
			defaultValue: '',
			primaryKey: true,
			field: 'Account'
		},
		playDate: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '',
			field: 'PlayDate'
		},
		playTime: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'PlayTime'
		}
	}, {
		timestamps: false,
		tableName: 'AccountPlay'
	});
	accountPlay.associate = function(models) {
		accountPlay.belongsTo(models.accountTbl, {
			foreignKey: 'account',
			as: 'acc'
		});
	};
	return accountPlay;
};
