/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	const accountTbl = sequelize.define('accountTbl', {
		account: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'account',
			primaryKey: true
		},
		password: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'password'
		},
		isuse: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'isuse'
		},
		member: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'member'
		},
		idNo1: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'id_no1'
		},
		idNo2: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'id_no2'
		},
		realname: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'realname'
		},
		reload: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'reload'
		},
		oldPassword: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'OldPassword'
		},
		tempPassword: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'TempPassword'
		},
		cash: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'cash'
		},
		vote: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'vote'
		},
	}, {
		timestamps: false,
		tableName: 'ACCOUNT_TBL'
	});
	accountTbl.associate = function(models) {
		accountTbl.belongsTo(models.accountTblDetail, {
			foreignKey: 'account',
			as: 'detail'
		});
		accountTbl.belongsTo(models.accountPlay, {
			foreignKey: 'account',
			as: 'play'
		});
	};
	return accountTbl;
};
