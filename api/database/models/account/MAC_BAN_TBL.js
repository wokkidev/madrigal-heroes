/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	const macBanTbl = sequelize.define('macBanTbl', {
		id: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'id',
			primaryKey: true
		},
		macaddr: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'macaddr'
		}
	}, {
		timestamps: false,
		tableName: 'MAC_BAN_TBL'
	});
	macBanTbl.associate = function(models) {
		macBanTbl.belongsTo(models.accountTblDetail, {
			foreignKey: 'macaddr',
			as: 'account'
		});
	}
	return macBanTbl;
};
