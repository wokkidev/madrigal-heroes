/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('pcZone', {
		pcZoneId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'PCZoneID'
		},
		pcZoneName: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'PCZoneName'
		},
		address: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'Address'
		},
		phone: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'Phone'
		},
		cpu: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'CPU'
		},
		vga: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'VGA'
		},
		ram: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'RAM'
		},
		monitor: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'Monitor'
		},
		comment: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'Comment'
		},
		grade: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'Grade'
		},
		account: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'Account'
		},
		regDate: {
			type: DataTypes.DATE,
			allowNull: false,
			field: 'RegDate'
		}
	}, {
		tableName: 'PCZone'
	});
};
