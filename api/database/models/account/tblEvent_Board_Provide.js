/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblEventBoardProvide', {
		account: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'account'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'serverindex'
		},
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		mSzName: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_szName'
		},
		regdate: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'regdate'
		}
	}, {
		tableName: 'tblEvent_Board_Provide'
	});
};
