/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblEventRecommend', {
		ofaccount: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'ofaccount'
		},
		byaccount: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'byaccount'
		},
		regdate: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'regdate'
		}
	}, {
		tableName: 'tblEventRecommend'
	});
};
