/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('logItemEventTbl', {
		mSzName: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'm_szName'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mGetszName: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_GetszName'
		},
		dwWorldId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwWorldID'
		},
		mDwGold: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_dwGold'
		},
		mNRemainGold: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nRemainGold'
		},
		itemUniqueNo: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'Item_UniqueNo'
		},
		itemName: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'Item_Name'
		},
		itemDurability: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'Item_durability'
		},
		mNAbilityOption: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nAbilityOption'
		},
		mGetdwGold: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_GetdwGold'
		},
		itemCount: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'Item_count'
		},
		state: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'State'
		},
		sDate: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 's_date'
		},
		mNSlot0: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nSlot0'
		},
		mNSlot1: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nSlot1'
		},
		mBItemResist: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_bItemResist'
		},
		mNResistAbilityOption: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nResistAbilityOption'
		},
		mBCharged: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_bCharged'
		},
		mDwKeepTime: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_dwKeepTime'
		},
		mNRandomOptItemId: {
			type: DataTypes.BIGINT,
			allowNull: true,
			field: 'm_nRandomOptItemId'
		},
		nPiercedSize: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nPiercedSize'
		},
		adwItemId0: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'adwItemId0'
		},
		adwItemId1: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'adwItemId1'
		},
		adwItemId2: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'adwItemId2'
		},
		adwItemId3: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'adwItemId3'
		},
		maxDurability: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'MaxDurability'
		},
		adwItemId4: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'adwItemId4'
		},
		nPetKind: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'nPetKind'
		},
		nPetLevel: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'nPetLevel'
		},
		dwPetExp: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'dwPetExp'
		},
		wPetEnergy: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'wPetEnergy'
		},
		wPetLife: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'wPetLife'
		},
		nPetAlD: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'nPetAL_D'
		},
		nPetAlC: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'nPetAL_C'
		},
		nPetAlB: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'nPetAL_B'
		},
		nPetAlA: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'nPetAL_A'
		},
		nPetAlS: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'nPetAL_S'
		},
		adwItemId5: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'adwItemId5'
		},
		adwItemId6: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'adwItemId6'
		},
		adwItemId7: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'adwItemId7'
		},
		adwItemId8: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'adwItemId8'
		},
		adwItemId9: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'adwItemId9'
		},
		nUmPiercedSize: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'nUMPiercedSize'
		},
		adwUmItemId0: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'adwUMItemId0'
		},
		adwUmItemId1: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'adwUMItemId1'
		},
		adwUmItemId2: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'adwUMItemId2'
		},
		adwUmItemId3: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'adwUMItemId3'
		},
		adwUmItemId4: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '((0))',
			field: 'adwUMItemId4'
		},
		seq: {
			type: DataTypes.BIGINT,
			allowNull: false,
			field: 'SEQ'
		}
	}, {
		tableName: 'LOG_ITEM_EVENT_TBL'
	});
};
