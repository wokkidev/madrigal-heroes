/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblPetLog', {
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		idPlayer: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'idPlayer'
		},
		iSerial: {
			type: DataTypes.BIGINT,
			allowNull: false,
			field: 'iSerial'
		},
		nType: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'nType'
		},
		dwData: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'dwData'
		},
		nKind: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'nKind'
		},
		nLevel: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'nLevel'
		},
		wLife: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'wLife'
		},
		wEnergy: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'wEnergy'
		},
		dwExp: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'dwExp'
		},
		nAvailParam1: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'nAvailParam1'
		},
		nAvailParam2: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'nAvailParam2'
		},
		nAvailParam3: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'nAvailParam3'
		},
		nAvailParam4: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'nAvailParam4'
		},
		nAvailParam5: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'nAvailParam5'
		},
		logDt: {
			type: DataTypes.DATE,
			allowNull: false,
			field: 'LogDt'
		},
		seq: {
			type: DataTypes.BIGINT,
			allowNull: false,
			field: 'SEQ'
		}
	}, {
		tableName: 'tblPetLog'
	});
};
