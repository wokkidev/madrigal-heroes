/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('logLoginTbl', {
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		dwWorldId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwWorldID'
		},
		startTime: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'Start_Time'
		},
		endTime: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'End_Time'
		},
		totalPlayTime: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'TotalPlayTime'
		},
		mDwGold: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_dwGold'
		},
		remoteIp: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'remoteIP'
		},
		account: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'account'
		},
		charLevel: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'CharLevel'
		},
		job: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'Job'
		},
		state: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'State'
		},
		seq: {
			type: DataTypes.BIGINT,
			allowNull: false,
			field: 'SEQ'
		}
	}, {
		tableName: 'LOG_LOGIN_TBL'
	});
};
