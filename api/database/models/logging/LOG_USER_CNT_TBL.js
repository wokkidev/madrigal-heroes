/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('logUserCntTbl', {
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex',
			primaryKey: true
		},
		sDate: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 's_date'
		},
		number: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'number'
		},
		m01: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_01'
		},
		m02: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_02'
		},
		m03: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_03'
		},
		m04: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_04'
		},
		m05: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_05'
		},
		m06: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_06'
		},
		m07: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_07'
		},
		m08: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_08'
		},
		m09: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_09'
		},
		m10: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_10'
		},
		m11: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_11'
		},
		m12: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_12'
		},
		m13: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_13'
		},
		m14: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_14'
		},
		m15: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_15'
		},
		m16: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_16'
		},
		m17: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_17'
		},
		m18: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_18'
		},
		m19: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_19'
		},
		m20: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_20'
		},
		mChannel: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_channel'
		}
	}, {
		timestamps: false,
		tableName: 'LOG_USER_CNT_TBL'
	});
};
