/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('logRespawnTbl', {
		mNid: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nid'
		},
		mNFrequency: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nFrequency'
		},
		sDate: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 's_date'
		}
	}, {
		tableName: 'LOG_RESPAWN_TBL'
	});
};
