/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblQuizAnswerLog', {
		seq: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'SEQ'
		},
		mIdQuizEvent: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'm_idQuizEvent'
		},
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mNChannel: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'm_nChannel'
		},
		mNQuizNum: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'm_nQuizNum'
		},
		mNSelect: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nSelect'
		},
		mNAnswer: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nAnswer'
		},
		sDate: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: '(getdate())',
			field: 's_date'
		}
	}, {
		tableName: 'tblQuizAnswerLog'
	});
};
