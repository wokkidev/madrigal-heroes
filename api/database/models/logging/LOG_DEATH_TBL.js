/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('logDeathTbl', {
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		dwWorldId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwWorldID'
		},
		killedMSzName: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'killed_m_szName'
		},
		mVPosX: {
			type: DataTypes.REAL,
			allowNull: true,
			field: 'm_vPos_x'
		},
		mVPosY: {
			type: DataTypes.REAL,
			allowNull: true,
			field: 'm_vPos_y'
		},
		mVPosZ: {
			type: DataTypes.REAL,
			allowNull: true,
			field: 'm_vPos_z'
		},
		mNLevel: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nLevel'
		},
		attackPower: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'attackPower'
		},
		maxHp: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'max_HP'
		},
		sDate: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 's_date'
		},
		seq: {
			type: DataTypes.BIGINT,
			allowNull: false,
			field: 'SEQ'
		}
	}, {
		tableName: 'LOG_DEATH_TBL'
	});
};
