/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblCampusPointLog', {
		seq: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'SEQ'
		},
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		chState: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'chState'
		},
		nPrevPoint: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nPrevPoint'
		},
		nCurrPoint: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nCurrPoint'
		},
		sDate: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: '(getdate())',
			field: 's_date'
		}
	}, {
		tableName: 'tblCampus_PointLog'
	});
};
