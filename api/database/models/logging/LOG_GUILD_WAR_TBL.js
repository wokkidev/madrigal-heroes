/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('logGuildWarTbl', {
		mIdGuild: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'm_idGuild'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		fIdGuild: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'f_idGuild'
		},
		mIdWar: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_idWar'
		},
		mNCurExp: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nCurExp'
		},
		mNGetExp: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nGetExp'
		},
		mNCurPenya: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nCurPenya'
		},
		mNGetPenya: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nGetPenya'
		},
		state: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'State'
		},
		sDate: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 's_date'
		},
		eDate: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'e_date'
		}
	}, {
		tableName: 'LOG_GUILD_WAR_TBL'
	});
};
