/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblQuizLog', {
		seq: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'SEQ'
		},
		mIdQuizEvent: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'm_idQuizEvent'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mNChannel: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'm_nChannel'
		},
		mNQuizType: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'm_nQuizType'
		},
		startTime: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: '(getdate())',
			field: 'Start_Time'
		},
		endTime: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'End_Time'
		},
		mNWinnerCount: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nWinnerCount'
		},
		mNQuizCount: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nQuizCount'
		}
	}, {
		tableName: 'tblQuizLog'
	});
};
