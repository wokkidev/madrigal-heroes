/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('logItemSendTbl', {
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		mNNo: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nNo'
		},
		mSzItemName: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_szItemName'
		},
		mNItemNum: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nItemNum'
		},
		sDate: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 's_date'
		},
		mBItemResist: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_bItemResist'
		},
		mNResistAbilityOption: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nResistAbilityOption'
		}
	}, {
		tableName: 'LOG_ITEM_SEND_TBL'
	});
};
