/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblGuildHouseFurnitureLog', {
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mIdGuild: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'm_idGuild'
		},
		seq: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'SEQ'
		},
		itemIndex: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'ItemIndex'
		},
		bSetup: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'bSetup'
		},
		delDate: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: '(getdate())',
			field: 'Del_date'
		},
		sDate: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 's_date'
		},
		setDate: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'set_date'
		}
	}, {
		tableName: 'tblGuildHouse_FurnitureLog'
	});
};
