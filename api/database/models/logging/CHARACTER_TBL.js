/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('characterTblLogging', {
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		account: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'account'
		},
		mSzName: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_szName'
		},
		isblock: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'isblock'
		},
		mNLevel: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nLevel'
		},
		mNJob: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nJob'
		}
	}, {
		tableName: 'CHARACTER_TBL'
	});
};
