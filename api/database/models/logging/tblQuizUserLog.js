/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblQuizUserLog', {
		seq: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'SEQ'
		},
		mIdQuizEvent: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'm_idQuizEvent'
		},
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mNChannel: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'm_nChannel'
		},
		sDate: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: '(getdate())',
			field: 's_date'
		}
	}, {
		tableName: 'tblQuizUserLog'
	});
};
