/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblCampusLog', {
		seq: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'SEQ'
		},
		mIdMaster: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'm_idMaster'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mIdPupil: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPupil'
		},
		idCampus: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'idCampus'
		},
		chState: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'chState'
		},
		sDate: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: '(getdate())',
			field: 's_date'
		}
	}, {
		tableName: 'tblCampusLog'
	});
};
