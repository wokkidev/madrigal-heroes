/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblSystemErrorLog', {
		seq: {
			type: DataTypes.BIGINT,
			allowNull: false,
			field: 'SEQ'
		},
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'serverindex'
		},
		account: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'account'
		},
		nChannel: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nChannel'
		},
		chType: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'chType'
		},
		szError: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'szError'
		},
		sDate: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: '(getdate())',
			field: 's_date'
		}
	}, {
		tableName: 'tblSystemErrorLog'
	});
};
