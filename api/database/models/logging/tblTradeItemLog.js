/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblTradeItemLog', {
		tradeId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			field: 'TradeID'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true,
			field: 'serverindex'
		},
		idPlayer: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true,
			field: 'idPlayer'
		},
		itemIndex: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			field: 'ItemIndex'
		},
		itemSerialNum: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			field: 'ItemSerialNum'
		},
		itemCnt: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'ItemCnt'
		},
		abilityOpt: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'AbilityOpt'
		},
		itemResist: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'ItemResist'
		},
		resistAbilityOpt: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: '((0))',
			field: 'ResistAbilityOpt'
		},
		randomOpt: {
			type: DataTypes.BIGINT,
			allowNull: false,
			defaultValue: '((0))',
			field: 'RandomOpt'
		}
	}, {
		tableName: 'tblTradeItemLog'
	});
};
