/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('logInsDungeonTbl', {
		seq: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
			field: 'SEQ'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mDungeonId: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'm_DungeonID'
		},
		mWorldId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'm_WorldID'
		},
		mChannel: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_channel'
		},
		mType: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_Type'
		},
		mState: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_State'
		},
		mDate: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: '(getdate())',
			field: 'm_Date'
		}
	}, {
		tableName: 'LOG_INS_DUNGEON_TBL'
	});
};
