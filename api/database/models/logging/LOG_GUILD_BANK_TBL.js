/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('logGuildBankTbl', {
		mIdGuild: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'm_idGuild'
		},
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mNGuildGold: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nGuildGold'
		},
		mGuildBank: {
			type: DataTypes.TEXT,
			allowNull: true,
			field: 'm_GuildBank'
		},
		mItem: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_Item'
		},
		state: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'State'
		},
		sDate: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 's_date'
		},
		mNAbilityOption: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nAbilityOption'
		},
		itemCount: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'Item_count'
		},
		itemUniqueNo: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'Item_UniqueNo'
		},
		seq: {
			type: DataTypes.BIGINT,
			allowNull: false,
			field: 'SEQ'
		}
	}, {
		tableName: 'LOG_GUILD_BANK_TBL'
	});
};
