/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblGuildHouseLog', {
		seq: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'SEQ'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mIdGuild: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'm_idGuild'
		},
		dwWorldId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwWorldID'
		},
		tKeepTime: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'tKeepTime'
		},
		mSzGuild: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_szGuild'
		},
		sDate: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: '(getdate())',
			field: 's_date'
		}
	}, {
		tableName: 'tblGuildHouseLog'
	});
};
