/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblChangeNameLog', {
		changeId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'ChangeID'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		startDt: {
			type: DataTypes.DATE,
			allowNull: false,
			field: 'StartDt'
		},
		endDt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: '2099-12-31',
			field: 'EndDt'
		},
		idPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'idPlayer'
		},
		charName: {
			type: DataTypes.STRING,
			allowNull: false,
			defaultValue: '',
			field: 'CharName'
		}
	}, {
		tableName: 'tblChangeNameLog'
	});
};
