/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblLogExpBox', {
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'serverindex'
		},
		idPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'idPlayer'
		},
		objid: {
			type: DataTypes.BIGINT,
			allowNull: false,
			field: 'objid'
		},
		iExp: {
			type: DataTypes.BIGINT,
			allowNull: false,
			field: 'iExp'
		},
		state: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'State'
		},
		sDate: {
			type: DataTypes.DATE,
			allowNull: false,
			field: 's_date'
		}
	}, {
		tableName: 'tblLogExpBox'
	});
};
