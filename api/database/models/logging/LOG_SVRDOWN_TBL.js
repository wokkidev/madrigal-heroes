/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('logSvrdownTbl', {
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		startTime: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'Start_Time'
		},
		endTime: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'End_Time'
		}
	}, {
		tableName: 'LOG_SVRDOWN_TBL'
	});
};
