/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('logQuestTbl', {
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		questIndex: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'Quest_Index'
		},
		state: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'State'
		},
		startTime: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'Start_Time'
		},
		endTime: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'End_Time'
		}
	}, {
		tableName: 'LOG_QUEST_TBL'
	});
};
