/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblSkillPointLog', {
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'serverindex'
		},
		playerId: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'PlayerID'
		},
		skillId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'SkillID'
		},
		skillLevel: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'SkillLevel'
		},
		skillPoint: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'SkillPoint'
		},
		holdingSkillPoint: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'HoldingSkillPoint'
		},
		totalSkillPoint: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'TotalSkillPoint'
		},
		skillExp: {
			type: DataTypes.BIGINT,
			allowNull: true,
			field: 'SkillExp'
		},
		usingBy: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'UsingBy'
		},
		actionDt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'ActionDt'
		}
	}, {
		tableName: 'tblSkillPointLog'
	});
};
