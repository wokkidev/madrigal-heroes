/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblTradeDetailLog', {
		tradeId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			field: 'TradeID'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true,
			field: 'serverindex'
		},
		idPlayer: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true,
			field: 'idPlayer'
		},
		job: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'Job'
		},
		level: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'Level'
		},
		tradeGold: {
			type: DataTypes.BIGINT,
			allowNull: false,
			defaultValue: '((0))',
			field: 'TradeGold'
		},
		playerIp: {
			type: DataTypes.STRING,
			allowNull: false,
			defaultValue: '0.0.0.0',
			field: 'PlayerIP'
		}
	}, {
		tableName: 'tblTradeDetailLog'
	});
};
