/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('logGamemasterTbl', {
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		mSzWords: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_szWords'
		},
		sDate: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 's_date'
		},
		mIdGuild: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idGuild'
		},
		seq: {
			type: DataTypes.BIGINT,
			allowNull: false,
			field: 'SEQ'
		}
	}, {
		tableName: 'LOG_GAMEMASTER_TBL'
	});
};
