/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('logGuildServiceTbl', {
		mIdGuild: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'm_idGuild'
		},
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mNLevel: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nLevel'
		},
		mGuildLv: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_GuildLv'
		},
		guildPoint: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'GuildPoint'
		},
		gold: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'Gold'
		},
		state: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'State'
		},
		sDate: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 's_date'
		}
	}, {
		tableName: 'LOG_GUILD_SERVICE_TBL'
	});
};
