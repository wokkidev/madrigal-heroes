/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblChangeNameHistoryLog', {
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		idPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'idPlayer'
		},
		oldCharName: {
			type: DataTypes.STRING,
			allowNull: false,
			defaultValue: '',
			field: 'OldCharName'
		},
		newCharName: {
			type: DataTypes.STRING,
			allowNull: false,
			defaultValue: '',
			field: 'NewCharName'
		},
		changeDt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: '(getdate())',
			field: 'ChangeDt'
		}
	}, {
		tableName: 'tblChangeNameHistoryLog'
	});
};
