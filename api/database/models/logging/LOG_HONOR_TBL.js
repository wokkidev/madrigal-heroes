/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('logHonorTbl', {
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'serverindex'
		},
		nHonor: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'nHonor'
		},
		logDt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'LogDt'
		}
	}, {
		tableName: 'LOG_HONOR_TBL'
	});
};
