/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('logUniqueTbl', {
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		dwWorldId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'dwWorldID'
		},
		mVPosX: {
			type: DataTypes.REAL,
			allowNull: true,
			field: 'm_vPos_x'
		},
		mVPosY: {
			type: DataTypes.REAL,
			allowNull: true,
			field: 'm_vPos_y'
		},
		mVPosZ: {
			type: DataTypes.REAL,
			allowNull: true,
			field: 'm_vPos_z'
		},
		itemName: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'Item_Name'
		},
		itemAddLv: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'Item_AddLv'
		},
		sDate: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 's_date'
		}
	}, {
		tableName: 'LOG_UNIQUE_TBL'
	});
};
