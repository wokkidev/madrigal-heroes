/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblQuestLog', {
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		idPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'idPlayer'
		},
		questIndex: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'QuestIndex'
		},
		state: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'State'
		},
		logDt: {
			type: DataTypes.DATE,
			allowNull: false,
			field: 'LogDt'
		},
		comment: {
			type: DataTypes.STRING,
			allowNull: false,
			defaultValue: '',
			field: 'Comment'
		},
		seq: {
			type: DataTypes.BIGINT,
			allowNull: false,
			field: 'SEQ'
		}
	}, {
		tableName: 'tblQuestLog'
	});
};
