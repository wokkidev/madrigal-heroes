/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblTradeLog', {
		tradeId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			field: 'TradeID'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			primaryKey: true,
			field: 'serverindex'
		},
		worldId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'WorldID'
		},
		tradeDt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: '(getdate())',
			field: 'TradeDt'
		}
	}, {
		tableName: 'tblTradeLog'
	});
};
