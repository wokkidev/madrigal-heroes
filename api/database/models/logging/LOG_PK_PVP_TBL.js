/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('logPkPvpTbl', {
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		mNLevel: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nLevel'
		},
		mVPosX: {
			type: DataTypes.REAL,
			allowNull: true,
			field: 'm_vPos_x'
		},
		mVPosZ: {
			type: DataTypes.REAL,
			allowNull: true,
			field: 'm_vPos_z'
		},
		killedMIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'killed_m_idPlayer'
		},
		killedMNLevel: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'killed_m_nLevel'
		},
		mGetPoint: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_GetPoint'
		},
		mNSlaughter: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nSlaughter'
		},
		mNFame: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nFame'
		},
		killedMNSlaughter: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'killed_m_nSlaughter'
		},
		killedMNFame: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'killed_m_nFame'
		},
		mKillNum: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_KillNum'
		},
		state: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'State'
		},
		sDate: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 's_date'
		},
		seq: {
			type: DataTypes.BIGINT,
			allowNull: false,
			field: 'SEQ'
		}
	}, {
		tableName: 'LOG_PK_PVP_TBL'
	});
};
