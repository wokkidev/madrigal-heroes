/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('logCharacterTbl', {
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		account: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'account'
		},
		mSzName: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'm_szName'
		},
		createTime: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'CreateTime'
		}
	}, {
		tableName: 'LOG_CHARACTER_TBL'
	});
};
