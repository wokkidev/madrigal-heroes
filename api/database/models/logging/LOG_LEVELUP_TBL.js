/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('logLevelupTbl', {
		mIdPlayer: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'm_idPlayer'
		},
		serverindex: {
			type: DataTypes.CHAR,
			allowNull: false,
			field: 'serverindex'
		},
		mNExp1: {
			type: DataTypes.BIGINT,
			allowNull: true,
			field: 'm_nExp1'
		},
		mNLevel: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nLevel'
		},
		mNJob: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nJob'
		},
		mNJobLv: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nJobLv'
		},
		mNFlightLv: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nFlightLv'
		},
		mNStr: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nStr'
		},
		mNDex: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nDex'
		},
		mNInt: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nInt'
		},
		mNSta: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nSta'
		},
		mNRemainGp: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nRemainGP'
		},
		mNRemainLp: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_nRemainLP'
		},
		mDwGold: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'm_dwGold'
		},
		totalPlayTime: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'TotalPlayTime'
		},
		state: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'State'
		},
		sDate: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 's_date'
		},
		seq: {
			type: DataTypes.BIGINT,
			allowNull: false,
			field: 'SEQ'
		}
	}, {
		tableName: 'LOG_LEVELUP_TBL'
	});
};
