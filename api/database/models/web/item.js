'use strict';
module.exports = (sequelize, DataTypes) => {
  const Item = sequelize.define('Item', {
    name: DataTypes.STRING,
    image: DataTypes.STRING,
    price: DataTypes.INTEGER,
    salePrice: DataTypes.INTEGER,
    isSale: DataTypes.BOOLEAN,
    isVote: DataTypes.BOOLEAN,
    isHidden: DataTypes.BOOLEAN,
    itemId: DataTypes.STRING,
    description: DataTypes.TEXT,
    quantity: DataTypes.INTEGER,
    categoryId: DataTypes.INTEGER,
    purchases: DataTypes.INTEGER
  }, {});
  Item.associate = function(models) {
    Item.belongsTo(models.Category, {
      foreignKey: 'categoryId',
      as: 'category'
    });
  };
  return Item;
};