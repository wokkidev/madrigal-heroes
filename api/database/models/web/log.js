'use strict';
module.exports = (sequelize, DataTypes) => {
  const Log = sequelize.define('Log', {
    type: DataTypes.STRING,
    message: DataTypes.STRING,
    account: DataTypes.STRING,
    ip: DataTypes.STRING,
    siteId: DataTypes.INTEGER
  }, {});
  Log.associate = function(models) {
    // associations can be defined here
  };
  return Log;
};