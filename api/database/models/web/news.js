'use strict';
module.exports = (sequelize, DataTypes) => {
  const News = sequelize.define('News', {
    uuid: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    title: DataTypes.STRING,
    message: DataTypes.TEXT,
    author: DataTypes.STRING
  }, {});
  News.associate = function(models) {
    // associations can be defined here
  };
  return News;
};