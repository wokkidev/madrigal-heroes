'use strict';
module.exports = (sequelize, DataTypes) => {
  const Setting = sequelize.define('Setting', {
    uuid: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    name: DataTypes.STRING,
    value: DataTypes.STRING
  }, {
    timestamps: true
  });
  Setting.associate = function(models) {
    // associations can be defined here
  };
  return Setting;
};