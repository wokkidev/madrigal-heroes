'use strict';
module.exports = (sequelize, DataTypes) => {
  const VoteLogs = sequelize.define('VoteLogs', {
    account: DataTypes.STRING,
    site: DataTypes.INTEGER,
    ip: DataTypes.STRING
  }, {});
  VoteLogs.associate = function(models) {
    // associations can be defined here
  };
  return VoteLogs;
};