import express from 'express';
import http from 'http';
import path from 'path';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import routes from '../routes';
import rateLimit from 'express-rate-limit';
import socket from 'socket.io';

import winston from 'winston';
import DailyRotateFile from 'winston-daily-rotate-file';

import Monitor from '../utils/monitor';

const limiter = rateLimit({
  windowMs: 15 * 60 * 1000, // 15 minutes,
  max: 500 // limit each IP to 500 requests per windowMs
});

const app = express();
if (process.env.NODE_ENV === 'production') app.use(limiter);
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());

const allowedIPs = [
  '127.0.0.1',
  '198.148.82.98', '198.148.82.99', // GTOP 100 IP's
  '163.158.234.41', '163.158.241.201', '77.170.39.68', '172.68.34.48' // XTREMETOP100 IP's
];

app.set('trust proxy', function (ip) {
  if (ip.includes('::ffff:')) {
    ip = ip.split(':').reverse()[0]
  }
  if (allowedIPs.includes(ip)) return true;
  return false;
});

if (process.env.NODE_ENV === 'development') app.use('/api', routes);
else app.use('/', routes);

const server = http.createServer(app);
const io = socket(server);

const monitor = new Monitor(process.env.WORLD_IP, parseInt(process.env.WORLD_PORT), parseInt(process.env.PING_INTERVAL));
monitor.start(); // Start our pinging interval
let worldStatus = monitor.ping() ? true : false;
monitor.on('status', (status) => {
  if (!status) {
    worldStatus = false;
    io.emit('worldStatus', false);
  }
  else {
    worldStatus = true;
    io.emit('worldStatus', true);
  }
});

let clients = new Map();

io
  .on('connection', (socket) => {
    socket.emit('requestIdentity');

    socket.on('identify', (data) => {
      if (data.user === undefined) {
        clients.set(socket.id, socket.id);
      }
      else {
        let client = clients.get(data.user.account);
        if (!client) clients.set(data.user.account, socket.id);
        if (data.user.authority === 'Z') socket.join('admin');
      }

      io.to('admin').emit('clients', clients);
    });

    socket.emit('worldStatus', worldStatus);

    socket.on('disconnect', (reason) => {
      const value = [...clients.entries()].filter(({ 1: v}) => v === socket.id).map(([k]) => k);
      if (value.length > 0) clients.delete(value[0]);

      io.to('admin').emit('clients', clients);
    });

    socket.on('sendMessage', (data) => {
      io.to(data.to).emit('notice', { message: data.message });
    });

    socket.on('getClients', () => {
      io.to('admin').emit('clients', clients);
    });
  });

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  transports: [
    new winston.transports.File({ filename: path.join(__dirname + '/../logs', 'error.log'), level: 'error' }),
    new winston.transports.File({ filename: path.join(__dirname + '/../logs', 'combined.log') }),
  ],
});

logger.configure({
  level: 'verbose',
  transports: [
    new DailyRotateFile({
      filename: path.join(__dirname + '/../logs', 'madrigalheroes-api-%DATE%.log'),
      datePattern: 'YYYY-MM-DD-HH',
      zippedArchive: true,
      maxSize: '20m',
      maxFiles: '14d'
    })
  ]
});

if (process.env.NODE_ENV !== 'production') {
  logger.add(new winston.transports.Console({
    format: winston.format.simple(),
  }));
}

exports.logger = logger;
exports.io = io;
exports.clients = clients;
export default server;