import server from './server';

const PORT = process.env.PORT || 3300;

server.listen(PORT, () => console.log(`Madrigal Heroes API listening on localhost:${PORT}`));