import models from '../database/models';
import Util from '../utils/Utils';
import LogService from '../services/LogService';
import { logger } from '../server';

const util = new Util();

export default class AccountContoller {
  static async getPeakCount(req, res) {
    try {
      await models.logUserCntTbl.findOne({
        order: [
          ['number', 'DESC']
        ],
        attributes: ['number']
      })
        .then(count => {
          util.setSuccess(200, 'Peak fetched', count.number);
          return util.send(res);
        })
        .catch(error => {
          console.error(error);
          util.setError(400, error);
          return util.send(res);
        });
    }
    catch(error) {
      console.error(error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  static async getVoteLogs(req, res) {
    const account = req.account;

    logger.info(`[LoggingController:getVoteLogs:${account}] Get vote logs request from IP ${req.ip || 'Unknown IP'}`);

    if (!account) {
      logger.info(`[LoggingController:getVoteLogs:${account}] Missing account name`);
      util.setError(400, 'Missing account name');
      return util.send(res);
    }

    try {
      await models.VoteLogs.findAll({
        where: {
          account: account
        },
        limit: 20
      }).then(
        logs => {
          logger.info(`[LoggingController:getVoteLogs:${account}] Logs fetched`);
          util.setSuccess(200, 'Vote logs fetched', logs);
          return util.send(res);
        }
      ).catch(
        error => {
          logger.error(`[LoggingController:getVoteLogs:${account}] Error`, error);
          util.setError(400, error);
          return util.send(res);
        }
      )
    }
    catch(error) {
      logger.error(`[LoggingController:getVoteLogs:${account}] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  static async getLogs(req, res) {
    const { type } = req.params;
    const account = req.account;

    logger.info(`[LoggingController:getLogs:${account}] Getting logs of type ${type} from IP ${req.ip || 'Unknown IP'}`);

    if (!account || !type) {
      logger.info(`[LoggingController:getLogs:${account}] Missing required values`);
      util.setError(400, 'Missing account name or log type');
      return util.send(res);
    }

    try {
      const logs = await LogService.getLogs(account, type);
      if (!logs) {
        logger.info(`[LoggingController:getLogs:${account}] No logs found`);
        util.setError(404, 'Logs not found');
        return util.send(res);
      }
      logger.info(`[LoggingController:getLogs:${account}] Logs fetched`);
      util.setSuccess(200, 'Logs fetched', logs);
      return util.send(res);
    }
    catch(error) {
      logger.error(`[LoggingController:getLogs:${account}] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  static async getAllLogsByType(req, res) {
    const { type } = req.params;
    const account = req.account;

    logger.info(`[LoggingController:getAllLogsByType:${type}] Getting all logs by type from IP ${req.ip || 'Unknown IP'}`);

    try {
      const logs = await LogService.getAllByType(type);
      if (!logs) {
        logger.info(`[LoggingController:getAllLogsByType:${type}] No logs found`);
        util.setError(404, 'Logs not found');
        return util.send(res);
      }
      logger.info(`[LoggingController:getAllLogsByType:${type}] Logs fetched`);
      util.setSuccess(200, 'Logs fetched', logs);
      return util.send(res);
    }
    catch(error) {
      logger.error(`[LoggingController:getAllLogsByType:${type}] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }
}