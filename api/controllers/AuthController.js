import models from '../database/models';
import Util from '../utils/Utils';
import crypto from 'crypto';
import jwt from 'jsonwebtoken';
import { logger } from '../server';

import AccountService from '../services/AccountService';

const util = new Util();

export default class AuthController {
  /**
   * Authenticate account
   * @param {*} req 
   * @param {*} res 
   */
  static async auth(req, res) {
    const { username, password } = req.body;
  
    logger.info(`[AuthController:auth] Login for user ${username} from IP ${req.ip || 'Unknown IP'}`);

    if (!username || !password) {
      logger.info(`[AuthController:auth] Login was missing required information`);
      util.setError(400, 'Missing username or password');
      return util.send(res);
    }

    try {
      const account = await AccountService.getAccount(username);
      if (!account) {
        logger.info(`[AuthController:auth] Account ${username} was not found`);
        util.setError(404, 'Account not found');
        return util.send(res);
      }

      if (account.detail.ban && account.detail.ban.length > 0) {
        util.setError(400, 'Account banned');
        return util.send(res);
      }

      if (account.password !== crypto.createHash('md5').update(process.env.SALT + password).digest('hex')) {
        logger.info(`[AuthController:auth] Password for ${username} was invalid`);
        util.setError(401, 'Invalid password');
        return util.send(res);
      }

      logger.info(`[AuthController:auth] Successful login to account ${username} from IP ${req.ip || 'Unknown IP'}`);

      const token = jwt.sign({ sub: account.account, authority: account.detail.mChLoginAuthority }, process.env.SECRET, {
        expiresIn: 86400
      });

      res.cookie('token', token, { httpOnly: true });
      res.cookie('loggedin', '1');

      util.setSuccess(200, 'Logged in');
      return util.send(res);
    }
    catch(error) {
      console.error(`[AuthController:auth] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  static async logout(req, res) {
    res.clearCookie('token');
    res.clearCookie('loggedin');
    util.setSuccess(200, 'Logged out');
    return util.send(res);
  }

  static async getAccount(req, res) {
    if (req.account) {
      const account = await AccountService.getAccount(req.account)
      if (!account) {
        util.setError(404, 'No account found');
        return util.send(res);
      }
      const data = {
        account: account.account,
        email: account.detail.email,
        cash: account.cash,
        vote: account.vote
      }

      util.setSuccess(200, 'Account data fetched', data);
      return util.send(res);
    }
    else {
      util.setError(404, 'No account found');
      return util.send(res);
    }
  }

  /**
   * Web shop login
   * This function is not in use currently. It should also be moved over to use the AccountService like
   * normal authentication.
   * @param {*} req 
   * @param {*} res 
   */
  static async gameLogin(req, res) {
    util.setError(400, 'This functionality is not in use');
    return util.send(res);

    const { user_id, m_idPlayer, server_index, md5, check } = req.body;

    if (!user_id, !m_idPlayer, !server_index, !md5, !check) {
      util.setError(400, 'Missing required info');
      return util.send(res);
    }

    const md5check = crypto.createHash('md5').update(user_id + m_idPlayer + server_index).digest("hex");
    if (md5 !== md5check) {
      util.setError(400, 'Invalid check');
      return util.send(res);
    }

    try {
      await models.accountTbl.findOne({
        where: {
          account: user_id
        },
        include: [
          {
            model: models.accountTblDetail,
            as: 'detail',
            attributes: ['account', 'mChLoginAuthority', 'regdate', 'isuse', 'email']
          },
          {
            model: models.accountPlay,
            as: 'play'
          }
        ]
      }).then(
        account => {
          if (account) {
            const token = jwt.sign({ sub: account.account }, process.env.SECRET, {
              expiresIn: 86400
            });
    
            const u = {
              account: account.account,
              email: account.detail.email,
              cash: account.cash,
              vote: account.vote,
              authority: account.detail.mChLoginAuthority,
              accessToken: token
            };
    
            util.setSuccess(200, 'Logged in', u);
            return util.send(res);
          }
          else {
            util.setError(404, 'Account not found');
            return util.send(res);
          }
        }
      ).catch(
        error => {
          util.setError(400, error);
          return util.send(res);
        }
      )
    }
    catch(error) {

    }
  }

  /**
   * Create account using the CreateNewAccount stored procedure
   * @param {*} req 
   * @param {*} res 
   */
  static async createAccount(req, res) {
    try {
      const { username, email, password } = req.body;
      const cash = 0;

      logger.info(`[AuthController:createAccount] Registration username ${username} email ${email}`);

      const allowed = /^[0-9a-zA-Z]+$/;
      if (!username.match(allowed)) {
        logger.info(`[AuthController:createAccount] Account creation failed. Account name can only include characters from a-z and numbers from 0-9.`);
        util.setError(422, 'Account creation failed. Account name can only include charaters from a-z and numbers from 0-9.');
        return util.send(res);
      }

      await models.Account.query('EXEC usp_CreateNewAccount :username, :password, :cash, :email', {
        replacements: {
          username: username.toLowerCase(),
          password: crypto.createHash('md5').update(process.env.SALT + password).digest('hex'),
          cash: cash,
          email: email
        }
      })
        .then((data) => {
          logger.info(`[AuthController:createAccount]`, data);
          if (Object.values(data[0][0])[0] === 1 && Object.values(data[1][0])[0]) {
            util.setSuccess(200, 'Account created', { username: username });
            return util.send(res);
          }
          else {
            logger.error(`[AuthController:createAccount] Account with the name ${username} already exists`);
            util.setError(409, 'Account creation failed. An account with that name might already exist.');
            return util.send(res);
          }
        })
        .catch(error => {
          logger.error(`[AuthController:createAccount] Error`, error);
          util.setError(400, error);
          return util.send(res);
        });
    }
    catch(error) {
      logger.error(`[AuthController:createAccount] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  /**
   * Update account password
   * @param {*} req 
   * @param {*} res 
   */
  static async updatePassword(req, res) {
    const { password } = req.body;
    const account = req.account;
    
    logger.info(`[AuthController:updatePassword] Updating password for account ${account} from IP ${req.ip || 'Unknown IP'}`);

    if (!account || !password) {
      logger.info(`[AuthController:updatePassword] Missing required values`);
      util.setError(409, 'Missing required values');
      return util.send(res);
    }

    try {
      await models.Account.query('EXEC usp_ChangePW :username, :password', {
        replacements: {
          username: account,
          password: crypto.createHash('md5').update(process.env.SALT + password).digest('hex'),
        }
      })
        .then(data => {
          logger.info(`[AuthController:updatePassword] Account ${account} password changed`, data);
          util.setError(200, 'Password changed', data);
          return util.send(res);
        })
        .catch(error => {
          logger.error(`[AuthController:updatePassword] Error`, error);
          util.setError(400, error);
          return util.send(res);
        });
    }
    catch(error) {
      logger.error(`[AuthController:updatePassword] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  /**
   * Update account email
   * @param {*} req 
   * @param {*} res 
   */
  static async updateEmail(req, res) {
    const { email } = req.body;
    const account = req.account;

    logger.info(`[AuthController:updateEmail] Updating email for account ${account} from IP ${req.ip || 'Unknown IP'}`);

    if (!account || !email) {
      logger.info(`[AuthController:updateEmail] Missing required values`);
      util.setError(409, 'Missing required values');
      return util.send(res);
    }

    try {
      await models.Account.query('EXEC usp_ChangeEmail :username, :email', {
        replacements: {
          username: account,
          email: email
        }
      })
        .then(data => {
          logger.info(`[AuthController:updateEmail] Account ${account} email changed`, data);
          util.setError(200, 'Email changed', data);
          return util.send(res);
        })
        .catch(error => {
          logger.error(`[AuthController:updateEmail] Error`, error);
          util.setError(400, error);
          return util.send(res);
        });
    }
    catch(error) {
      logger.error(`[AuthController:updateEmail] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  /**
   * Forgot password
   * 
   * This function is work in progress
   * @param {*} req 
   * @param {*} res 
   */
  static async forgotPassword(req, res) {
    const { accountOrEmail } = req.body;

    try {
      const account = await AccountService.getAccount(accountOrEmail);
      
      if (!account) {
        const emailAccount = await AccountService.getAccountByEmail(accountOrEmail);
        if (!emailAccount) {
          util.setError(400, 'No account found');
          return util.send(res);
        }
        else {
          // Send password reset link to account.email
          // account.account
        }
      }
      else {
        // Send password reset link to account.detail.email
      }
    }
    catch(error) {
      console.error(error);
      util.setError(400, error);
      return util.send(res);
    }
  }
}