import models from '../database/models';
import Util from '../utils/Utils';
import LogService from '../services/LogService';
import { logger } from '../server';
import AccountService from '../services/AccountService';

const util = new Util();

export default class AccountContoller {
  /**
   * Fetch all accounts from the ACCOUNT_TBL
   * @param {*} req 
   * @param {*} res 
   */
  static async getAccounts(req, res) {
    logger.info(`[AccountController:getAccounts] request from IP ${req.ip || 'Unknown IP'}, account ${req.account || 'Unknown account'}`);
    try {
      await models.accountTbl.findAll({
        include: [
          {
            model: models.accountTblDetail,
            as: 'detail',
            include: [
              {
                model: models.macBanTbl,
                as: 'ban'
              }
            ]
          },
          {
            model: models.accountPlay,
            as: 'play'
          }
        ]
      })
        .then(accounts => {
          logger.info(`[AccountController:getAccounts] Accounts fetched`)
          util.setSuccess(200, 'Accounts fetched', accounts);
          return util.send(res);
        })
        .catch(error => {
          logger.error(`[AccountController:getAccounts] Error`, error);
          util.setError(400, error);
          return util.send(res);
        });
    }
    catch(error) {
      logger.error(`[AccountController:getAccounts] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  /**
   * Get mac banned accounts
   * @param {*} req 
   * @param {*} res 
   */
  static async getBannedAccounts(req, res) {
    try {
      await models.macBanTbl.findAll({
        include: [
          {
            model: models.accountTblDetail,
            as: 'account'
          }
        ]
      }).then(
        accounts => {
          util.setSuccess(200, 'Banned accounts fetched', accounts);
          return util.send(res);
        }
      ).catch(
        error => {
          logger.error(`[AccountController:getBannedAccounts] Error`, error);
          util.setError(400, error);
          return util.send(res);
        }
      )
    }
    catch(error) {
      logger.error(`[AccountController:getBannedAccounts] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  /**
   * Fetch a specific account from ACCOUNT_TBL with account name
   * @param {*} req 
   * @param {*} res 
   */
  static async getAccount(req, res) {
    const { id } = req.params;

    logger.info(`[AccountController:getAccount:${id}] request from IP ${req.ip || 'Unknown IP'}, account ${req.account || 'Unknown account'}`)

    try {
      await models.accountTbl.findOne({
        where: {
          account: id
        },
        include: [
          {
            model: models.accountTblDetail,
            as: 'detail',
            include: [
              {
                model: models.macBanTbl,
                as: 'ban'
              }
            ]
          },
          {
            model: models.accountPlay,
            as: 'play'
          }
        ]
      })
        .then(account => {
          logger.info(`[AccountController:getAccount:${id}] Account fetched`);
          util.setSuccess(200, 'Account fetched', account);
          return util.send(res);
        })
        .catch(error => {
          logger.info(`[AccountController:getAccount:${id}] Error`, error);
          util.setError(400, error);
          return util.send(res);
        });
    }
    catch(error) {
      logger.info(`[AccountController:getAccount:${id}] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  static async updateAccount(req, res) {
    const { account, values } = req.body;
    const admin = req.account;

    logger.info(`[AccountController:updateAccount] Account = ${account}, Values = ${values} set by ${req.account || 'Unknown account'} from IP ${req.ip || 'Unknown IP'}`);

    const replaceWords = [
      {
        word: 'cash',
        replace: 'Madrigal Points'
      },
      {
        word: 'vote',
        replace: 'Vote Points'
      }
    ];

    if (!account || !values) {
      logger.info(`[AccountController:updateAccount] Missing required values`);
      util.setError(400, 'Missing values');
      return util.send(res);
    }
    
    try {
      await models.accountTbl.findOne({
        where: {
          account: account
        }
      }).then(
        account => {
          if (account) {
            Object.keys(values).forEach(async key => {
              account[key] = values[key];
              await LogService.log({ type: 'admin', message: `${admin} set ${account.account} ${replaceWords.find(w => w.word === key) ? replaceWords.find(w => w.word === key).replace : key} to ${values[key]}`, account: account.account });
            });
            account.save();
  
            logger.info(`[AccountController:updateAccount] Account ${account.account} saved`);
            util.setSuccess(200, 'Account saved', account);
            return util.send(res);
          }
          else {
            logger.info(`[AccountController:updateAccount] Invalid account name`);
            util.setError(404, 'Account not found');
            return util.send(res);
          }
        }
      )
    }
    catch(error) {
      logger.info(`[AccountController:updateAccount] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  /**
   * Prevent from using this function and instead use the getOnlineCount of Character Controller
   * This function currently returns the same as Logging Controllers getPeakCount does
   * @param {*} req 
   * @param {*} res 
   */
  static async getOnlineCount(req, res) {
    try {
      await models.accountTblDetail.count({
        where: {
          isuse: 'J'
        }
      })
        .then(count => {
          util.setSuccess(200, 'Online count fetched', count);
          return util.send(res);
        })
        .catch(error => {
          console.error(error);
          util.setError(400, error);
          return util.send(res);
        });
    }
    catch(error) {
      console.error(error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  /**
   * Add new mac ban
   * @param {*} req 
   * @param {*} res 
   */
  static async addMacBan(req, res) {
    const account = req.account;
    const { target } = req.body;

    logger.info(`[AccountController:addMacBan:${target}] request from IP ${req.ip || 'Unknown IP'}, account ${req.account || 'Unknown account'}`);

    try {
      const user = await AccountService.getAccount(target);
      if (!user) {
        util.setError(404, 'No account found');
        return util.send(res);
      }

      const mac = user.detail.lastmac;
      if (!mac) {
        util.setError(400, 'Acccount does not have last mac on record');
        return util.send(res);
      }

      await models.Account.query('EXEC MAC_BAN_ACC :iAccount', {
        replacements: {
          iAccount: target.toLowerCase()
        }
      }).then(
        async data => {
          await LogService.log({ type: 'admin', message: `${account} mac banned ${target}`, account: target })
          util.setSuccess(200, 'Account mac banned');
          return util.send(res);
        }
      ).catch(
        error => {
          logger.error('[AccountController:addMacBan] Error', error);
          util.setError(error);
          return util.send(res);
        }
      );
    }
    catch(error) {
      logger.error('[AccountController:addMacBan] Error', error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  /**
   * Remove mac ban
   * @param {*} req 
   * @param {*} res 
   */
  static async removeMacBan(req, res) {
    const account = req.account;
    const { target } = req.body;

    logger.info(`[AccountController:removeMacBan:${target}] request from IP ${req.ip || 'Unknown IP'}, account ${req.account || 'Unknown account'}`);

    try {
      const user = await AccountService.getAccount(target);
      if (!user) {
        util.setError(404, 'No account found');
        return util.send(res);
      }

      if (!account.detail.ban || account.detail.ban.length === 0) {
        util.setError(400, 'No bans on record for account');
        return util.send(res);
      }

      account.detail.ban[0].destroy();
      await LogService.log({ type: 'admin', message: `${account} unbanned ${target}`, account: target })
      util.setSuccess(200, 'Account unbanned');
      return util.send(res);
    }
    catch(error) {
      logger.error('[AccountController:removeMacBan] Error', error);
      util.setError(400, error);
      return util.send(res);
    }
  }
}