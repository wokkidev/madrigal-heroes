import models from '../database/models';
import StoreService from '../services/StoreService';
import AccountService from '../services/AccountService';
import CharacterService from '../services/CharacterService';
import LogService from '../services/LogService';
import Util from '../utils/Utils';
import { logger } from '../server';

const util = new Util();

export default class StoreController {
  /**
   * Get all items
   * @param {*} req 
   * @param {*} res 
   */
  static async getItems(req, res) {
    try {
      await models.Item.findAll().then(
        items => {
          util.setSuccess(200, 'Items fetched', items);
          return util.send(res);
        }
      ).catch(
        error => {
          logger.error(`[StoreController:getItems] Error`, error);
          util.setError(400, error);
          return util.send(res);
        }
      )
    }
    catch(error) {
      logger.error(`[StoreController:getItems] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  static async getItemData(req, res) {
    try {
      await models.itemTbl.findAll().then(
        items => {
          util.setSuccess(200, 'Items fetched', items);
          return util.send(res);
        }
      ).catch(
        error => {
          logger.error(`[StoreController:getItemData] Error`, error);
          util.setError(400, error);
          return util.send(res);
        }
      )
    }
    catch(error) {
      logger.error(`[StoreController:getItemData] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  /**
   * Get a single item
   * @param {*} req 
   * @param {*} res 
   */
  static async getItem(req, res) {
    const { id } = req.params;

    try {
      await StoreService.getItem(id).then(
        item => {
          if (item) {
            util.setSuccess(200, 'Item fetched', item);
            return util.send(res);
          }
          else {
            util.setError(404, 'Item not found');
            return util.send(res);
          }
        }
      ).catch(
        error => {
          logger.error(`[StoreController:getItem] Error`, error);
          util.setError(400, error);
          return util.send(res);
        }
      )
    }
    catch(error) {
      logger.error(`[StoreController:getItem] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  /**
   * Get categories. Does not return items
   * @param {*} req 
   * @param {*} res 
   */
  static async getCategories(req, res) {
    try {
      await models.Category.findAll().then(
        categories => {
          util.setSuccess(200, 'Categories fetched', categories);
          return util.send(res);
        }
      ).catch(
        error => {
          logger.error(`[StoreController:getCategories] Error`, error);
          util.setError(400, error);
          return util.send(res);
        }
      )
    }
    catch(error) {
      logger.error(`[StoreController:getCategories] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  /**
   * Get items by category
   * @param {*} req 
   * @param {*} res 
   */
  static async getItemsByCategory(req, res) {
    const { id } = req.params;

    try {
      await StoreService.getItemsByCategory(id).then(
        category => {
          util.setSuccess(200, 'Items fetched', category.items);
          return util.send(res);
        }
      ).catch(
        error => {
          logger.error(`[StoreController:getItemsByCategory] Error`, error);
          util.setError(400, error);
          return util.send(res);
        }
      )
    }
    catch(error) {
      logger.error(`[StoreController:getItemsByCategory] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  /**
   * Add item
   * @param {*} req 
   * @param {*} res 
   */
  static async addItem(req, res) {
    const { name, image, price, salePrice, isSale, isVote, itemId, description, categoryId, quantity } = req.body;
    const account = req.account;

    if (!account) {
      util.setError(401, 'Unauthorized');
      return util.send(res);
    }
    
    logger.info(`[StoreController:addItem] Adding new item by ${account} from IP ${req.ip || 'Unknown IP'}`);

    try {
      await models.Item.create({
        name: name,
        image: image,
        price: price,
        salePrice: salePrice || price,
        isSale: isSale || false,
        isVote: isVote || false,
        itemId: itemId,
        description: description,
        categoryId: categoryId,
        quantity: quantity
      }).then(
        item => {
          util.setSuccess(200, 'Item added', item);
          return util.send(res);
        }
      ).catch(
        error => {
          logger.error(`[StoreController:addItem] Error`, error);
          util.setError(400, error);
          return util.send(res);
        }
      )
    }
    catch(error) {
      logger.error(`[StoreController:addItem] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  /**
   * Update item
   * @param {*} req 
   * @param {*} res 
   */
  static async updateItem(req, res) {
    const { id, name, image, price, salePrice, isSale, isVote, itemId, description, categoryId, quantity } = req.body;
    const account = req.account;

    if (!account) {
      util.setError(401, 'Unauthorized');
      return util.send(res);
    }

    logger.info(`[StoreController:updateItem:${id}] Updating item by ${account} from IP ${req.ip || 'Unknown IP'}`);

    try {
      await StoreService.getItem(id).then(
        item => {
          if (item) {
            item.name = name;
            item.image = image;
            item.price = price;
            item.salePrice = salePrice;
            item.isSale = isSale;
            item.isVote = isVote;
            item.itemId = itemId;
            item.description = description;
            item.categoryId = categoryId;
            item.quantity = quantity;
            item.save();
            logger.info(`[StoreController:updateItem:${id}] Item updated`);
            util.setSuccess(200, 'Item updated', item);
            return util.send(res);
          }
          else {
            logger.info(`[StoreController:updateItem:${id}] Item not found`);
            util.setError(404, 'Item not found');
            return util.send(res);
          }
        }
      ).catch(
        error => {
          logger.error(`[StoreController:updateItem:${id}] Error`, error);
          util.setError(400, error);
          return util.send(res);
        }
      )
    }
    catch(error) {
      logger.error(`[StoreController:updateItem:${id}] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  /**
   * Delete item
   * @param {*} req 
   * @param {*} res 
   */
  static async deleteItem(req, res) {
    const { id } = req.body;
    const account = req.account;

    if (!account) {
      util.setError(401, 'Unauthorized');
      return util.send(res);
    }

    logger.info(`[StoreController:deleteItem:${id}] Delete item by ${account} from IP ${req.ip || 'Unknown IP'}`);

    try {
      await StoreService.getItem(id).then(
        item => {
          if (item) {
            item.destroy();
            logger.info(`[StoreController:deleteItem:${id}] Item deleted`);
            util.setSuccess(200, 'Item deleted', item);
            return util.send(res);
          }
          else {
            logger.info(`[StoreController:deleteItem:${id}] Item not found`);
            util.setError(404, 'Item not found');
            return util.send(res);
          }
        }
      ).catch(
        error => {
          logger.error(`[StoreController:deleteItem:${id}] Error`, error);
          util.setError(400, error);
          return util.send(res);
        }
      )
    }
    catch(error) {
      logger.error(`[StoreController:deleteItem:${id}] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  /**
   * Add category
   * @param {*} req 
   * @param {*} res 
   */
  static async addCategory(req, res) {
    const { name } = req.body;
    const account = req.account;

    if (!account) {
      util.setError(401, 'Unauthorized');
      return util.send(res);
    }

    logger.info(`[StoreController:addCategory] Add category by ${account} from IP ${req.ip || 'Unknown IP'}`);

    try {
      await models.Category.create({
        name: name
      }).then(
        category => {
          logger.info(`[StoreController:addCategory] Category added`);
          util.setSuccess(200, 'Category added', category);
          return util.send(res);
        }
      ).catch(
        error => {
          logger.error(`[StoreController:addCategory] Error`, error);
          util.setError(400, error);
          return util.send(res);
        }
      )
    }
    catch(error) {
      logger.error(`[StoreController:addCategory] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  /**
   * Update category
   * @param {*} req 
   * @param {*} res 
   */
  static async updateCategory(req, res) {
    const { id, name } = req.body;
    const account = req.account;

    if (!account) {
      util.setError(401, 'Unauthorized');
      return util.send(res);
    }

    logger.info(`[StoreController:updateCategory:${id}] Update category by ${account} from IP ${req.ip || 'Unknown IP'}`);

    try {
      await StoreController.getCategory(id).then(
        category => {
          if (category) {
            category.name = name;
            category.save();
            logger.info(`[StoreController:updateCategory:${id}] Category updated`);
            util.setSuccess(200, 'Category updated', category);
            return util.send(res);
          }
          else {
            logger.info(`[StoreController:updateCategory:${id}] Category not found`);
            util.setError(404, 'Category not found')
            return util.send(res);
          }
        }
      ).catch(
        error => {
          logger.error(`[StoreController:updateCategory:${id}] Error`, error);
          util.setError(400, error);
          return util.send(res);
        }
      )
    }
    catch(error) {
      logger.error(`[StoreController:updateCategory:${id}] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  /**
   * Delete category
   * @param {*} req 
   * @param {*} res 
   */
  static async deleteCategory(req, res) {
    const { id } = req.body;
    const account = req.account;

    if (!account) {
      util.setError(401, 'Unauthorized');
      return util.send(res);
    }

    logger.info(`[StoreController:deleteCategory:${id}] Delete category by ${account} from IP ${req.ip || 'Unknown IP'}`);

    try {
      await StoreService.getItemsByCategory(id).then(
        category => {
          if (category) {
            if (category.items.length > 0) {
              logger.info(`[StoreController:deleteCategory:${id}] Cannot delete a category with items in it`);
              util.setError(409, 'Cannot delete a category with items in it');
              return util.send(res);
            }
            else {
              category.destroy();
              logger.info(`[StoreController:deleteCategory:${id}] Category deleted`);
              util.setSuccess(200, 'Category deleted', category);
              return util.send(res);
            }
          }
          else {
            logger.info(`[StoreController:deleteCategory:${id}] Category not found`);
            util.setError(404, 'Category not found')
            return util.send(res);
          }
        }
      ).catch(
        error => {
          logger.error(`[StoreController:deleteCategory:${id}] Error`, error);
          util.setError(400, error);
          return util.send(res);
        }
      )
    }
    catch(error) {
      logger.error(`[StoreController:deleteCategory:${id}] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  /**
   * Purchase item
   * @param {*} req 
   * @param {*} res 
   */
  static async purchaseItem(req, res) {
    const { itemId, characterId, amount } = req.body;
    const accountName = req.account;

    // If our amount is 0 or less, we'll return an error or if amount is not an integer
    if (amount <= 0 || !Number.isInteger(amount)) {
      util.setError(400, 'Invalid amount. Amount has to an integer and equal to 1 or higher.');
      return util.send(res);
    }

    logger.info(`[StoreController:purchaseItem] Account ${accountName} purchasing item ${itemId} x ${amount} for character ${characterId}`);

    if (!itemId || !characterId || !accountName) {
      logger.info(`[StoreController:purchaseItem] Missing required values`);
      util.setError(400, 'Missing required info');
      return util.send(res);
    }

    try {
      const item = await StoreService.getItem(itemId);
      if (!item) {
        logger.info(`[StoreController:purchaseItem] Invalid item id`);
        util.setError(404, 'Invalid item id');
        return util.send(res);
      }

      const account = await AccountService.getAccount(accountName);
      if (!account) {
        logger.info(`[StoreController:purchaseItem] No account found`);
        util.setError(404, 'No account found');
        return util.send(res);
      }

      const character = await CharacterService.getCharacterById(characterId);
      if (!character) {
        logger.info(`[StoreController:purchaseItem] No character found`);
        util.setError(404, 'No character found');
        return util.send(res);
      }

      const price = item.isSale ? item.salePrice * amount : item.price * amount;

      if (item.isVote) {
        if (account.vote >= price) {
          account.vote -= price;
        }
        else {
          logger.info(`[StoreController:purchaseItem] Insufficient Vote Points (${account.vote})`);
          util.setError(400, `Insufficient Vote Points (${account.vote})`);
          return util.send(res);
        }
      }
      else {
        if (account.cash >= price) {
          account.cash -= price;
        }
        else {
          logger.info(`[StoreController:purchaseItem] Insufficient Madrigal Points (${account.cash})`);
          util.setError(400, `Insufficient Madrigal Points (${account.cash})`);
          return util.send(res);
        }
      }

      const items = item.itemId.split(",");
      let sent = [];

      // Character is offline
      if (character.multiServer === 0) {
        logger.info(`[StoreController:purchaseItem] Sending item offline`);

        items.forEach(async itm => {
          let sending = await StoreService.sendItem(character.mIdPlayer, '01', parseInt(itm), amount * item.quantity);
          sent.push(sending);
        });
  
        logger.info(`[StoreController:purchaseItem] Items sent`, sent);
  
        await account.save();
        await LogService.log({ type: 'purchase', message: `Purchased item ${item.name} x ${amount} for ${character.mSzName}. -${price} ${item.isVote ? 'Vote Points' : 'Madrigal Points'} (Now has ${item.isVote ? account.vote + 'VP' : account.cash + 'MP'})`, account: accountName });
        await StoreService.updateItemPurchases(item, amount);
        util.setSuccess(200, 'Item sent', { item: item, account: account, amount: amount });
        return util.send(res);
      }
      // Character is online
      else {
        logger.info(`[StoreController:purchaseItem] Sending item online using socket`);

        items.forEach(async itm => {
          let sending = await StoreService.createItem(character.mIdPlayer, parseInt(itm), amount * item.quantity);
          sent.push(sending);
        });

        logger.info(`[StoreController:purchaseItem] Items sent`, sent);

        await account.save();
        await LogService.log({ type: 'purchase', message: `Purchased item ${item.name} x ${amount} for ${character.mSzName}. -${price} ${item.isVote ? 'Vote Points' : 'Madrigal Points'} (Now has ${item.isVote ? account.vote + 'VP' : account.cash + 'MP'})`, account: accountName });
        await StoreService.updateItemPurchases(item, amount);
        util.setSuccess(200, 'Item sent', { item: item, account: account, amount: amount });
        return util.send(res);
      }
    }
    catch(error) {
      logger.error(`[StoreController:purchaseItem] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }
}