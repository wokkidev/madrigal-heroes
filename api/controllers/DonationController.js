import models from '../database/models';
import { io, clients, logger } from '../server';

import PayPalService from '../services/PayPalService';
import LogService from '../services/LogService';

export default class DonationController {
  static async ipn(req, res) {
    res.status(200).send('OK');
    res.send();

    const body = req.body || {};

    logger.info(`[DonationController:ipn] Received IPN request with body`, body);

    try {
      const isValidated = await PayPalService.validate(body);
      if (!isValidated) {
        logger.info(`[DonationController:ipn] Error validating IPN message`);
        return;
      }

      // IPN Message validated
      const transactionType = body.txn_type;

      // Socket client
      const client = clients.get(body.custom);

      // Donation was through our donate button
      if (transactionType === 'web_accept') {
        try {
          const account = body.custom;
          const amount = body.mc_gross;
          let cash = 0;

          if (amount >= 25 && amount < 50) {
            cash = parseInt(amount * 10) * 1.1;
          }
          else if (amount >= 50 && amount < 99) {
            cash = parseInt(amount * 10) * 1.15;
          }
          else if (amount >= 100) {
            cash = parseInt(amount * 10) * 1.2;
          }
          else {
            cash = parseInt(amount * 10);
          }

          await models.accountTbl.findOne({
            where: {
              account: account
            }
          }).then(
            async acc => {
              acc.cash = parseInt(acc.cash) + cash
              acc.save();

              await LogService.log({ type: 'donation', message: `Donation $${amount} for ${cash} Madrigal Points`, account: account });

              if (client) io.to(client).emit('donateSuccess', { message: 'Thank you for donating! Your rewards should be on their way to your account. If you don\'t see points instantly on your account, try relogging to the website.' })

              logger.info(`[DonationController:ipn] IPN request validated and donation points x${cash} awarded for ${account} ($${amount})`);
            }
          ).catch(
            error => {
              logger.error(`[DonationController:ipn] Error`, error);
              if (client) io.to(client).emit('donateError', { message: error });
            }
          )
        }
        catch(error) {
          logger.error(`[DonationController:ipn] Error`, error);
          if (client) io.to(client).emit('donateError', { message: error });
        }
      }
      // Donation was through something else, so we'll log it for further investigation if necessary
      else {
        logger.error(`[DonationController:ipn] Received IPN type ${transactionType} which is not defined`);
      }
    }
    catch(error) {
      logger.error(`[DonationController:ipn] Error`, error);
      if (client) io.to(client).emit('donateError', { message: error });
    }
  }
}