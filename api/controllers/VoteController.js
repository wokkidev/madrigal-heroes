import models from '../database/models';
import Util from '../utils/Utils';
import { io, clients, logger } from '../server';

import VoteService from '../services/VoteService';
import LogService from '../services/LogService';

const util = new Util();

export default class VoteController {
  /**
   * gtop100 pingback
   * @param {*} req 
   * @param {*} res 
   */
  static async gtop100(req, res) {
    const authorized = ["198.148.82.98","198.148.82.99"];
    let requestIp = req.headers['cf-connecting-ip'] || req.headers['x-real-ip'] || req.connection.remoteAddress;
    if (requestIp.includes('::ffff:')) {
      requestIp = requestIp.split(':').reverse()[0];
    }
    const points = process.env.VOTE_POINTS_PER_VOTE || 5;
    const { VoterIP, Successful, Reason, pingUsername } = req.body;

    logger.info(`[VoteController:gtop100] New gtop100 pingback`, req.body);

    if (!authorized.includes(requestIp)) {
      logger.info(`[VoteController:gtop100] The incoming ip ${requestIp} does not match to the authorized IP addresses`);
      util.setError(401, 'Unauthorized IP');
      return util.send(res);
    }

    try {
      const client = clients.get(pingUsername);

      logger.info(`[VoteController:gtop100]`);

      if (parseInt(Successful) !== 0) {
        if (client) io.to(client).emit('voteError', { message: 'Voting failed. Vote site returned failure. This could mean you are trying to vote multiple times from the same IP address or 24 hours has not passed since your last vote.' });
        logger.info(`[VoteController:gtop100] Vote for account ${pingUsername} failed. Site returned Success value 0. Reason: ${Reason}`);
        util.setError(400, 'Site returned failure');
        return util.send(res);
      }

      const isValidated = await VoteService.validate({ account: pingUsername, site: 1 });
      if (!isValidated) {
        logger.info(`[VoteController:gtop100] Vote validation failed`);
        if (client) io.to(client).emit('voteError', { message: 'Voting failed as 24 hours has not passed since the last vote on this account' });
        util.setError(400, 'Vote validation failed');
        return util.send(res);
      }

      await models.accountTbl.findOne({
        where: {
          account: pingUsername
        }
      }).then(
          async account => {
            if (account) {
              logger.info(`[VoteController:gtop100] Account valid. Adding vote points ${points}`);
              account.vote = parseInt(account.vote) + parseInt(points);
              account.save();

              if (client) {
                io.to(client).emit('voteSuccess', { message: `Thank you for your vote! ${points} Vote Points have been added to your account.`, amount: points });
              }

              await LogService.log({ type: 'vote', message: `Vote on gtop100. +${points} Vote Points (Now has ${account.vote}VP)`, account: pingUsername, ip: VoterIP, siteId: 1 });

              util.setSuccess(200, 'Vote success', account.vote);
              return util.send(res);
            }
            else {
              logger.error(`[VoteController:gtop100] Account not found ${pingUsername}:${client}`);
              if (client) io.to(client).emit('voteError', { message: `Account could not be found, no voting rewards could be issued. Please contact support with these details: ${pingUsername}:${client}` });
              util.setError(404, 'Account could not be found, no vote points given');
              return util.send(res);
            }
          }
        ).catch(
          error => {
            logger.error(`[VoteController:gtop100] Error`, error);
            util.setError(400, error);
            return util.send(res);
          }
        )
    }
    catch(error) {
      logger.error(`[VoteController:gtop100] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  /**
   * xtremetop100 pingback
   * @param {*} req 
   * @param {*} res 
   */
  static async xtremetop100(req, res) {
    const authorized = ["198.148.82.98", "198.148.82.99", "163.158.234.41", "163.158.241.201", "77.170.39.68", "172.68.34.48"];
    const requestIp = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    const { votingip, postback } = req.body;
    const points = process.env.VOTE_POINTS_PER_VOTE || 5;

    logger.info(`[VoteController:xtremetop100] New xtremetop100 pingback`, req.body);

    if (!authorized.includes(requestIp)) {
      logger.info(`[VoteController:xtremetop100] The incoming ip ${requestIp} does not match to the authorized IP addresses`);
      util.setError(401, 'Unauthorized IP');
      return util.send(res);
    }

    try {
      const client = clients.get(pingUsername);

      logger.info(`[VoteController:xtremetop100]`);

      const isValidated = VoteService.validate({ account: pingUsername, site: 2 });
      if (!isValidated) {
        logger.info(`[VoteController:xtremetop100] Vote validation failed`);
        if (client) io.to(client).emit('voteError', { message: 'Voting failed as 24 hours has not passed since the last vote on this account' });
        util.setError(400, 'Vote validation failed');
        return util.send(res);
      }

      await models.accountTbl.findOne({
        where: {
          account: postback
        }
      }).then(
        async account => {
          if (account) {
            logger.info(`[VoteController:xtremetop100] Account valid. Adding vote points ${points}`);
            account.vote = parseInt(account.vote) + parseInt(points);
            account.save();

            if (client) {
              io.to(client).emit('voteSuccess', { message: `Thank you for your vote! ${points} Vote Points have been added to your account.`, amount: points });
            }

            await LogService.log({ type: 'vote', message: `Vote on xtremetop100. +${points} Vote Points (Now has ${account.vote}VP)`, account: postback, ip: votingip, siteId: 2 });

            util.setSuccess(200, 'Vote success', account.vote);
            return util.send(res);
          }
          else {
            logger.error(`[VoteController:xtremetop100] Account not found ${postback}:${client}`);
            if (client) io.to(client).emit('voteError', { message: `Account could not be found, no voting rewards could be issued. Please contact support with these details: ${postback}:${client}` });
            util.setError(404, 'Account could not be found, no vote points given');
            return util.send(res);
          }
        }
      ).catch(
        error => {
          logger.error(`[VoteController:xtremetop100] Error`, error);
          console.error(error);
          util.setError(400, error);
          return util.send(res);
        }
      )
    }
    catch(error) {
      logger.error(`[VoteController:xtremetop100] Error`, error);
      console.error(error);
      util.setError(400, error);
      return util.send(res);
    }
  }
}