import models from '../database/models';
import Util from '../utils/Utils';
import { logger } from '../server';

const util = new Util();

export default class CharacterController {
  /**
   * Fetch all chracters from the database
   * @param {*} req 
   * @param {*} res 
   */
  static async getCharacters(req, res) {
    logger.info(`[CharacterController:getCharacters] Get characters request from ${req.ip || 'Unknown IP'}`);

    try {
      await models.characterTbl.findAll({
        limit: 20,
        where: {
          mChAuthority: 'F'
        },
        attributes: ['mSzName', 'mNJob', 'mNLevel', 'mDwGold', 'multiServer', 'totalPlayTime'],
        order: [
          [models.Sequelize.literal(`CASE WHEN m_nJob BETWEEN 16 AND 31 THEN 2 ELSE 1 END`), 'DESC'],
          ['mNLevel', 'DESC'],
          ['mNJob', 'DESC']
        ],
        include: [
          {
            model: models.guildMemberTbl,
            as: 'guildMember',
            attributes: ['createTime'],
            include: [
              {
                model: models.guildTbl,
                as: 'guild',
                attributes: ['mSzGuild']
              }
            ]
          }
        ]
      })
        .then(characters => {
          logger.info(`[CharacterController:getCharacters] Characters fetched`);
          util.setSuccess(200, 'Characters fetched', characters);
          return util.send(res);
        })
        .catch(error => {
          logger.error(`[CharacterController:getCharacters] Error`, error);
          util.setError(400, error);
          return util.send(res);
        });
    }
    catch(error) {
      logger.error(`[CharacterController:getCharacters] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  /**
   * Fetch character by it's name from the database
   * @param {*} req 
   * @param {*} res 
   */
  static async getCharacter(req, res) {
    const { id } = req.params;

    logger.info(`[CharacterController:getCharacter:${id}] Get character request from IP ${req.ip || 'Unknown IP'}`);

    try {
      await models.characterTbl.findOne({
        where: {
          mSzName: id
        },
        attributes: ['mSzName', 'mChAuthority', 'mNJob', 'mNLevel', 'mDwGold', 'multiServer', 'totalPlayTime'],
        include: [
          {
            model: models.guildMemberTbl,
            as: 'guildMember',
            attributes: ['createTime'],
            include: [
              {
                model: models.guildTbl,
                as: 'guild',
                attributes: ['mSzGuild']
              }
            ]
          },
          {
            model: models.inventoryTbl,
            as: 'inventory',
            attributes: ['mInventory']
          }
        ]
      })
        .then(character => {
          logger.info(`[CharacterController:getCharacter:${id}] Character fetched`);
          util.setSuccess(200, 'Character fetched', character);
          return util.send(res);
        })
        .catch(error => {
          logger.error(`[CharacterController:getCharacter:${id}] Error`, error);
          util.setError(400, error);
          return util.send(res);
        });
    }
    catch(error) {
      logger.error(`[CharacterController:getCharacter:${id}] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  static async getCharactersByAccount(req, res) {
    const { account } = req.params;

    logger.info(`[CharacterController:getCharactersByAccount:${account}] Get characters by account from IP ${req.ip || 'Unknown IP'}`);

    if (req.account === account) {
      try {
        await models.characterTbl.findAll({
          where: {
            account: account
          },
          attributes: ['mSzName', 'mNJob', 'mNLevel', 'mDwGold', 'mIdPlayer', 'multiServer', 'totalPlayTime'],
          order: [
            [models.Sequelize.literal(`CASE WHEN m_nJob BETWEEN 16 AND 31 THEN 2 ELSE 1 END`), 'DESC'],
            ['mNLevel', 'DESC'],
            ['mNJob', 'DESC']
          ],
          include: [
            {
              model: models.guildMemberTbl,
              as: 'guildMember',
              attributes: ['createTime'],
              include: [
                {
                  model: models.guildTbl,
                  as: 'guild',
                  attributes: ['mSzGuild']
                }
              ]
            }
          ]
        })
          .then(characters => {
            logger.info(`[CharacterController:getCharactersByAccount:${account}] Characters fetched`);
            util.setSuccess(200, 'Character fetched', characters);
            return util.send(res);
          })
          .catch(error => {
            logger.error(`[CharacterController:getCharactersByAccount:${account}] Error`, error);
            util.setError(400, error);
            return util.send(res);
          });
      }
      catch(error) {
        logger.error(`[CharacterController:getCharactersByAccount:${account}] Error`, error);
        util.setError(400, error);
        return util.send(res);
      }
    }
    else {
      util.setError(401, `You don't have access to this content`);
      return util.send(res);
    }
  }

  /**
   * Get online count from characters table. Checks if MultiServer does not equal to 0
   * @param {*} req 
   * @param {*} res 
   */
  static async getOnlineCount(req, res) {
    try {
      await models.characterTbl.count({
        where: {
          MultiServer: {
            [models.Sequelize.Op.not]: 0
          }
        }
      })
        .then(count => {
          util.setSuccess(200, 'Online count fetched', count);
          return util.send(res);
        })
        .catch(error => {
          console.error(error);
          util.setError(400, error);
          return util.send(res);
        });
    }
    catch(error) {
      console.error(error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  /**
   * Get latest guild siege winner
   */
  // SELECT TOP 1 m_szGuild FROM tblCombatJoinGuild JG LEFT JOIN tblCombatInfo CI ON JG.CombatID = CI.CombatID LEFT JOIN GUILD_TBL G ON JG.GuildID = G.m_idGuild WHERE CI.[Status] = '30' ORDER BY JG.CombatID DESC, Point DESC
  static async gsWinner(req, res) {
    try {
      await models.tblCombatJoinGuild.findOne({
        order: [
          ['combatId', 'DESC'],
          ['point', 'DESC']
        ],
        include: [
          {
            model: models.guildTbl,
            as: 'guild'
          }
        ]
      })
        .then(guild => {
          console.log(guild);
          util.setSuccess(200, 'Guild Siege winner fetched', guild);
          return util.send(res);
        })
        .catch(error => {
          console.error(error);
          util.setError(404, error);
          return util.send(res);
        });
    }
    catch(error) {
      console.error(error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  /**
   * Get latest guild siege MVP
   * @param {*} req 
   * @param {*} res 
   */
  // SELECT TOP 1 m_szName FROM tblCombatJoinPlayer JP LEFT JOIN  tblCombatInfo CI ON JP.CombatID = CI.CombatID LEFT JOIN CHARACTER_TBL C ON JP.PlayerID = C.m_idPlayer WHERE CI.[Status] = '30' ORDER BY JP.CombatID DESC, Point DESC
  /*static async gsMVP(req, res) {
    try {
      await models.tblCombatJoinPlayer.findOne({
        order: [
          ['combatId', 'DESC'],
          ['point', 'DESC']
        ],
        include: [
          {
            model: models.characterTbl,
            as: 'character',
            where: {
              playerId: { [models.Sequelize.Op.col]: 'models.tblCombatJoinPlayer.playerId' }
            }
          }
        ]
      })
        .then(player => {
          console.log(player);
          util.setSuccess(200, 'Guild Siege MVP fetched', player);
          return util.send(res);
        })
        .catch(error => {
          console.error(error);
          util.setError(404, error);
          return util.send(res);
        })
    }
    catch(error) {
      console.error(error);
      util.setError(400, error);
      return util.send(res);
    }
  }*/

  static async getLord(req, res) {
    try {
      await models.tblLord.findOne({
        order: [
          ['sDate', 'DESC']
        ]
      })
        .then(async lord => {
          if (!lord) {
            util.setError(404, 'No lord found');
            return util.send(res);
          }

          await models.characterTbl.findOne({
            where: {
              mIdPlayer: lord.idLord
            },
            attributes: ['mSzName']
          })
            .then(player => {
              util.setSuccess(200, 'Lord fetched', player);
              return util.send(res);
            })
            .catch(error => {
              console.error(error);
              util.setError(400, error);
              return util.send(res);
            });
        })
        .catch(error => {
          console.error(error);
          util.setError(400, error);
          return util.send(res);
        })
    }
    catch(error) {
      console.error(error);
      util.setError(400, error);
      return util.send(res);
    }
  }
}