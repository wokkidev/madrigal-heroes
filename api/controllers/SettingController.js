import models from '../database/models';
import Util from '../utils/Utils';
import { logger } from '../server';

const util = new Util();

export default class SettingController {
  /**
   * Get all settings
   * @param {*} req 
   * @param {*} res 
   */
  static async getSettings(req, res) {
    try {
      await models.Setting.findAll().then(
        settings => {
          util.setSuccess(200, 'Settings fetched', settings);
          return util.send(res);
        }
      ).catch(
        error => {
          console.error(error);
          util.setError(400, error);
          return util.send(res);
        }
      )
    }
    catch(error) {
      console.error(error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  /**
   * Get a setting
   * @param {*} req 
   * @param {*} res 
   */
  static async getSetting(req, res) {
    const { name } = req.params;

    if (!name) {
      util.setError(409, 'Missing name');
      return util.send(res);
    }

    try {
      await models.Setting.findOne({
        where: {
          name: name
        }
      }).then(
        setting => {
          util.setSuccess(200, 'Setting fetched', setting);
          return util.send(res);
        }
      ).catch(
        error => {
          console.error(error);
          util.setError(400, error);
          return util.send(res);
        }
      )
    }
    catch(error) {
      console.error(error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  /**
   * Add setting
   * @param {*} req 
   * @param {*} res 
   */
  static async addSetting(req, res) {
    const { name, value } = req.body;
    const account = req.account;

    logger.info(`[SettingController:addSetting] Adding new setting by account ${account} from IP ${req.ip || 'Unknown IP'}`);

    if (!name || !value || !account) {
      logger.info(`[SettingController:addSetting] Missing required values`);
      util.setError(409, 'Missing required values');
      return util.send(res);
    }

    try {
      await models.Setting.create({
        name: name,
        value: value
      }).then(
        setting => {
          logger.info(`[SettingController:addSetting] Setting added`, setting);
          util.setSuccess(200, 'Setting added', setting);
          return util.send(res);
        }
      ).catch(
        error => {
          logger.error(`[SettingController:addSetting] Error`, error);
          util.setError(400, error);
          return util.send(res);
        }
      )
    }
    catch(error) {
      logger.error(`[SettingController:addSetting] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  /**
   * Update setting
   * @param {*} req 
   * @param {*} res 
   */
  static async updateSetting(req, res) {
    const { uuid, value } = req.body;
    const account = req.account;

    logger.info(`[SettingController:updateSetting:${uuid}] Updating setting by account ${account} from ip ${req.ip || 'Unknown IP'}`);

    if (!uuid || !value || !account) {
      logger.info(`[SettingController:updateSetting:${uuid}] Missing required values`);
      util.setError(409, 'Missing required values');
      return util.send(res);
    }

    try {
      await models.Setting.findOne({
        where: {
          uuid: uuid
        }
      }).then(
        setting => {
          setting.value = value;
          setting.save();
          logger.info(`[SettingController:updateSetting:${uuid}] Setting updated`);
          util.setSuccess(200, 'Setting updated', setting);
          return util.send(res);
        }
      ).catch(
        error => {
          logger.error(`[SettingController:updateSetting:${uuid}] Error`, error);
          util.setError(400, error);
          return util.send(res);
        }
      )
    }
    catch(error) {
      logger.error(`[SettingController:updateSetting:${uuid}] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }
  
  /**
   * Delete setting
   * @param {*} req 
   * @param {*} res 
   */
  static async deleteSetting(req, res) {
    const { uuid } = req.body;
    const account = req.account;

    logger.info(`[SettingController:deleteSetting:${uuid}] Delete setting by account ${account} from IP ${req.ip || 'Unknown IP'}`)

    if (!uuid || !account) {
      logger.info(`[SettingController:deleteSetting:${uuid}] Missing required values`);
      util.setError(409, 'Missing required values');
      return util.send(res);
    }

    try {
      await models.Setting.findOne({
        where: {
          uuid: uuid
        }
      }).then(
        setting => {
          setting.destroy();
          logger.info(`[SettingController:deleteSetting:${uuid}] Setting deleted`);
          util.setSuccess(200, 'Setting deleted', []);
          return util.send(res);
        }
      ).catch(
        error => {
          logger.error(`[SettingController:deleteSetting:${uuid}] Error`, error);
          util.setError(400, error);
          return util.send(res);
        }
      )
    }
    catch(error) {
      logger.error(`[SettingController:deleteSetting:${uuid}] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }
}