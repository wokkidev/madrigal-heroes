import models from '../database/models';
import Util from '../utils/Utils';
import { io, logger } from '../server';
import axios from 'axios';
import moment from 'moment';

const util = new Util();

export default class NewsController {
  /**
   * Fetch all news from database
   * @param {*} req 
   * @param {*} res 
   */
  static async getNews(req, res) {
    try {
      await models.News.findAll({
        order: [
          ['createdAt', 'DESC']
        ]
      }).then(
        news => {
          util.setSuccess(200, 'News fetched', news);
          return util.send(res);
        }
      ).catch(
        error => {
          logger.error(`[NewsController:getNews] Error`, error);
          util.setError(400, error);
          return util.send(res);
        }
      )
    }
    catch(error) {
      logger.error(`[NewsController:getNews] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  /**
   * Fetch article from database by it's uuid
   * @param {*} req 
   * @param {*} res 
   */
  static async getArticle(req, res) {
    const { uuid } = req.params;

    try {
      await models.News.findByPk(uuid).then(
        article => {
          util.setSuccess(200, 'Article fetched', article);
          return util.send(res);
        }
      ).catch(
        error => {
          logger.error(`[NewsController:getArticle] Error`, error);
          util.setError(400, error);
          return util.send(res);
        }
      )
    }
    catch(error) {
      logger.error(`[NewsController:getArticle] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }
  
  /**
   * Add new article to the database
   * @param {*} req 
   * @param {*} res 
   */
  static async addArticle(req, res) {
    const { title, message } = req.body;
    const account = req.account;

    logger.info(`[NewsController:addArticle] Adding new article by ${account} from IP ${req.ip || 'Unknown IP'}`);

    if (!title || !message || !account) {
      logger.info(`[NewsController:addArticle] Missing required values`);
      util.setError(409, 'Missing required values');
      return util.send(res);
    }

    try {
      await models.News.create({ title: title, message: message, author: account }).then(
        article => {
          io.emit('newArticle', article);
          // Send webhook to Discord with a link to the article
          if (process.env.UPDATES_WEBHOOK) {
            axios.post(process.env.UPDATES_WEBHOOK, {
              content: `[@everyone] __**${article.title} ${moment(article.createdAt).format("MM/DD/YYYY")}**__\n<${process.env.ARTICLE_PATH}${article.uuid}>`
            }, { 'Content-Type': 'application/json' });
          }
          logger.info(`[NewsController:addArticle] Article created`);
          util.setSuccess(200, 'Article created', article);
          return util.send(res);
        }
      ).catch(
        error => {
          logger.error(`[NewsController:addArticle] Error`, error);
          util.setError(400, error);
          return util.send(res);
        }
      )
    }
    catch(error) {
      logger.error(`[NewsController:addArticle] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  /**
   * Update an existing article
   * @param {*} req 
   * @param {*} res 
   */
  static async updateArticle(req, res) {
    const { uuid, title, message } = req.body;
    const account = req.account;

    logger.info(`[NewsController:updateArticle:${uuid}] Updating article by account ${account} from IP ${req.ip || 'Unknown IP'}`);

    if (!uuid || !title || !message || !account) {
      logger.info(`[NewsController:updateArticle:${uuid}] Missing required values`);
      util.setError(409, 'Missing required values');
      return util.send(res);
    }

    try {
      await models.News.findOne({
        where: {
          uuid: uuid
        }
      }).then(
        article => {
          if (article) {
            article.title = title;
            article.message = message;
            article.save();
            io.emit('articleUpdate', article);
            logger.info(`[NewsController:updateArticle:${uuid}] Article updated`);
            util.setSuccess(200, 'Article updated', article);
            return util.send(res);
          }
          else {
            logger.info(`[NewsController:updateArticle:${uuid}] Article with given UUID could not be found`);
            util.setError(404, 'Article with given uuid could not be found');
            return util.send(res);
          }
        }
      )
      .catch(
        error => {
          logger.error(`[NewsController:updateArticle:${uuid}] Error`, error);
          util.setError(400, error);
          return util.send(res);
        }
      )
    }
    catch(error) {
      logger.error(`[NewsController:updateArticle:${uuid}] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  /**
   * Delete an article
   * @param {*} req 
   * @param {*} res 
   */
  static async deleteArticle(req, res) {
    const { uuid } = req.body;
    const account = req.account;

    logger.info(`[NewsController:deleteArticle:${uuid}] Deleting article by account ${account} from IP ${req.ip || 'Unknown IP'}`);

    if (!uuid || !account) {
      logger.info(`[NewsController:deleteArticle:${uuid}] Missing required values`);
      util.setError(409, 'Missing required values');
      return util.send(res);
    }

    try {
      await models.News.findOne({
        where: {
          uuid: uuid
        }
      }).then(
        article => {
          if (article) {
            article.destroy();
            io.emit('deleteArticle', article);
            logger.info(`[NewsController:deleteArticle:${uuid}] Article deleted`);
            util.setSuccess(200, 'Article deleted', []);
            return util.send(res);
          }
          else {
            logger.info(`[NewsController:deleteArticle:${uuid}] Article with given UUID could not be found`);
            util.setError(404, 'Article with given uuid could not be found');
            return util.send(res);
          }
        }
      )
      .catch(
        error => {
          logger.error(`[NewsController:deleteArticle:${uuid}] Error`, error);
          util.setError(400, error);
          return util.send(res);
        }
      )
    }
    catch(error) {
      logger.error(`[NewsController:deleteArticle:${uuid}] Error`, error);
      util.setError(400, error);
      return util.send(res);
    }
  }
}