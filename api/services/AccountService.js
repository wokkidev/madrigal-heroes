import models from '../database/models';

export default class AccountService {
  static getAccount(name) {
    return new Promise(async (resolve, reject) => {
      try {
        await models.accountTbl.findOne({
          where: {
            account: name
          },
          include: [
            {
              model: models.accountTblDetail,
              as: 'detail',
              include: [
                {
                  model: models.macBanTbl,
                  as: 'ban'
                }
              ]
            },
            {
              model: models.accountPlay,
              as: 'play'
            }
          ]
        }).then(
          account => {
            if (account) {
              resolve(account);
            }
            else {
              reject('Invalid account name or password');
            }
          }
        ).catch(
          error => {
            reject(new Error(error));
          }
        )
      }
      catch(error) {
        reject(new Error(error));
      }
    })
  }

  static getAccountByEmail(email) {
    return new Promise(async (resolve, reject) => {
      try {
        await models.accountTblDetail.findOne({
          where: {
            email: email
          }
        }).then(
          account => {
            if (account) {
              resolve(account);
            }
            else {
              reject(new Error('Account not found'));
            }
          }
        ).catch(
          error => {
            reject(new Error(error));
          }
        )
      }
      catch(error) {
        reject(new Error(error));
      }
    });
  }
  
  static createAccount(name, email, password) {

  }
}