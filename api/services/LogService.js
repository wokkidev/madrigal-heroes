import models from '../database/models';

export default class LogService {
  static log(body = {}) {
    const { type, message, account, ip, siteId } = body;

    return new Promise(async (resolve, reject) => {
      try {
        await models.Log.create({
          type: type,
          message: message,
          account: account,
          ip: ip || null,
          siteId: siteId || null
        }).then(
          log => {
            resolve(log);
          }
        ).catch(
          error => {
            reject(new Error(error));
          }
        )
      }
      catch(error) {
        reject(new Error(error));
      }
    });
  }

  static getLogs(account, type) {
    return new Promise(async (resolve, reject) => {
      try {
        await models.Log.findAll({
          where: {
            account: account,
            type: type
          },
          order: [
            ['id', 'DESC']
          ]
        }).then(
          logs => {
            resolve(logs);
          }
        ).catch(
          error => {
            reject(new Error(error));
          }
        )
      }
      catch(error) {
        reject(new Error(error));
      }
    });
  }

  static getAllByType(type) {
    return new Promise(async (resolve, reject) => {
      await models.Log.findAll({
        where: {
          type: type
        },
        order: [
          ['id', 'DESC']
        ]
      }).then(
        logs => {
          resolve(logs);
        }
      ).catch(
        error => {
          reject(new Error(error));
        }
      )
    });
  }
}