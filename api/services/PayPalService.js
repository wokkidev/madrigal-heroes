import request from 'request';
import querystring from 'querystring';
import Promise from 'bluebird';
import { logger } from '../server';

export default class PayPalService {
  /**
   * Validate PayPal IPN
   * @param {*} body 
   */
  static validate(body = {}) {
    return new Promise((resolve, reject) => {
      // Prepend 'cmd=_notify-validate' flag to the post string
      let formUrlEncodedBody = querystring.stringify(body);
      let verificationBody = `cmd=_notify-validate&${formUrlEncodedBody}`;
      let postreq = 'cmd=_notify-validate';

      // Iterate the original request payload object
      // and prepend its keys and values to the post string
      Object.keys(body).map((key) => {
        postreq = `${postreq}&${key}=${body[key]}`;
        return key;
      });

      console.log('verificationBody', verificationBody);
      console.log('PayPal URL', process.env.PAYPAL_MODE === 'live' ? 'https://ipnpb.paypal.com/cgi-bin/webscr' : 'https://ipnpb.sandbox.paypal.com/cgi-bin/webscr');

      const options = {
        url: process.env.PAYPAL_MODE === 'live' ? 'https://ipnpb.paypal.com/cgi-bin/webscr' : 'https://ipnpb.sandbox.paypal.com/cgi-bin/webscr',
        method: 'POST',
        body: verificationBody
      };

      // Make a post request to PayPal
      request(options, (error, response, resBody) => {
        if (error || response.statusCode !== 200) {
          reject(new Error(error));
          return;
        }

        console.log('Request body', resBody);

        // Validate the response from PayPal and resolve / reject the promise.
        if (resBody.substring(0, 8) === 'VERIFIED') {
          console.log(resBody.substring(0, 8));
          resolve(true);
        } else if (resBody.substring(0, 7) === 'INVALID') {
          console.log(resBody.substring(0, 7));
          reject(new Error('IPN Message is invalid.'));
        } else {
          reject(new Error('Unexpected response body.'));
        }
      });
    });
  }
}