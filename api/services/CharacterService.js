import models from '../database/models';

export default class CharacterService {
  static getCharacterById(id) {
    return new Promise(async (resolve, reject) => {
      try {
        await models.characterTbl.findOne({
          where: {
            mIdPlayer: id
          }
        }).then(
          character => {
            if (character) {
              resolve(character);
            }
            else {
              reject(new Error('No character found'));
            }
          }
        ).catch(
          error => {
            reject(new Error(error));
          }
        )
      }
      catch(error) {
        reject(new Error(error));
      }
    });
  }
}