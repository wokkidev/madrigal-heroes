import models from '../database/models';
import Promise from 'bluebird';
import moment from 'moment';

export default class VoteService {
  /**
   * Validate vote
   * @param {*} body 
   */
  static validate(body = {}) {
    return new Promise(async (resolve, reject) => {
      await models.Log.findAll({
        limit: 1,
        where: {
          account: body.account,
          siteId: body.site
        },
        order: [
          ['createdAt', 'DESC']
        ]
      }).then(
        logs => {
          if (logs) {
            if (logs.length > 0) {
              const log = logs[0];

              const now = moment();
              const voteDate = moment(log.createdAt);
              const diff = now.diff(voteDate)
              const interval = 86400000;
  
              if (diff >= interval) {
                resolve(true);
              }
              else {
                resolve(false);
              }
            }
            else {
              resolve(true);
            }
          }
          else {
            resolve(true);
          }
        }
      ).catch(
        error => {
          console.error(error);
          resolve(false);
        }
      )
    });
  }
}