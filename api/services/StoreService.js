import models from '../database/models';
import pack from 'locutus/php/misc/pack';
import net from 'net';

export default class StoreService {
  static getItem(id) {
    return new Promise(async (resolve, reject) => {
      try {
        await models.Item.findOne({
          where: {
            id: id
          }
        }).then(
          item => {
            if (item) {
              resolve(item);
            }
            else {
              reject(new Error('Item not found'));
            }
          }
        ).catch(
          error => {
            reject(new Error(error));
          }
        )
      }
      catch(error) {
        reject(new Error(error));
      }
    });
  }

  static getCategory(id) {
    return new Promise(async (resolve, reject) => {
      try {
        await models.Category.findOne({
          where: {
            id: id
          }
        }).then(
          category => {
            if (category) {
              resolve(category);
            }
            else {
              reject(new Error('Category not found'));
            }
          }
        ).catch(
          error => {
            reject(new Error(error));
          }
        )
      }
      catch(error) {
        reject(new Error(error));
      }
    });
  }

  static getItemsByCategory(id) {
    return new Promise(async (resolve, reject) => {
      try {
        await models.Category.findOne({
          where: {
            id: id
          },
          include: [
            {
              model: models.Item,
              as: 'items'
            }
          ]
        }).then(
          category => {
            if (category) {
              resolve(category);
            }
            else {
              reject(new Error('Category not found'));
            }
          }
        ).catch(
          error => {
            reject(new Error(error));
          }
        )
      }
      catch(error) {
        reject(new Error(error));
      }
    });
  }

  static sendItem(character, server, id, amount) {
    return new Promise(async (resolve, reject) => {
      try {
        await models.Character.query('EXEC uspProvideItemToCharacter :pPlayerId, :pserverindex, :pItemIndex, :pItemCnt', {
          replacements: {
            pPlayerId: character,
            pserverindex: server,
            pItemIndex: id,
            pItemCnt: amount
          }
        }).then(
          sentItem => {
            console.log(sentItem);
            resolve(true);
          }
        ).catch(
          error => {
            reject(new Error(error));
          }
        )
      }
      catch(error) {
        reject(new Error(error));
      }
    })
  }

  static sendApiCmd(ServerIndex, dwPlayerId, dwTargetId, dwParam1, dwParam2, dwParam3) {
    return new Promise((resolve, reject) => {
      try {
        const packet = Buffer.from(pack("VVVVVVVVV", ServerIndex, dwPlayerId, dwTargetId, 101, dwParam1, dwParam2, dwParam3, '0000000', '0000000'), 'binary');

        const socket = new net.Socket();
        socket.setTimeout(10000);
        socket.on('connect', () => {
          socket.write(packet, (error) => {
            if (error) {
              console.error(error);
              reject(new Error(error));
              socket.destroy();
            }
          });
        }).on('error', (e) => {
          console.error('Error', e);
          socket.destroy();
          reject(new Error(e));
        }).on('timeout', (e) => {
          console.error('Timeout', e);
          socket.destroy();
          reject(new Error(e));
        }).on('data', () => {
          socket.destroy();
          resolve(true);
        }).connect(29000, '95.217.20.36');
      }
      catch(error) {
        reject(new Error(error));
      }
    });
  }

  static createItem(dwTargetId, dwItemId, dwItemCount) {
    if (typeof dwItemId === 'string') dwItemId = parseInt(dwItemId);

    return new Promise((resolve, reject) => {
      this.sendApiCmd('01', dwTargetId, dwTargetId, dwItemId, dwItemCount, 0).then(
        data => {
          resolve(true);
        },
        error => {
          console.log('Create item error', error);
          reject(new Error(error));
        }
      ).catch(
        error => {
          console.error('Catch error', error);
          reject(new Error(error));
        }
      )
    });
  }

  static updateItemPurchases(item, amount) {
    return new Promise(async (resolve, reject) => {
      try {
        let itm = await this.getItem(item.id);
        if (!item) {
          reject(new Error('No item found'));
        }
        itm.purchases = item.purchases + amount;
        itm.save();
        resolve(itm);
      }
      catch(error) {
        reject(new Error(error));
      }
    });
  }
}