import { Router } from 'express';
import authJwt from '../utils/jwt';

import AuthController from '../controllers/AuthController';
import AccountController from '../controllers/AccountController';
import CharacterController from '../controllers/CharacterController';
import LoggingController from '../controllers/LoggingController';
import NewsController from '../controllers/NewsController';
import SettingController from '../controllers/SettingController';
import DonationController from '../controllers/DonationController';
import VoteController from '../controllers/VoteController';
import StoreController from '../controllers/StoreController';

const router = Router();

router.get('/', (req, res) => res.send('Madrigal Heroes API'));

// Authentication routes
router.post('/auth/signup', AuthController.createAccount);
router.post('/auth/login', AuthController.auth);
router.post('/auth/gamelogin', AuthController.gameLogin);
router.post('/auth/changepassword', [authJwt.verifyToken], AuthController.updatePassword);
router.post('/auth/changeemail', [authJwt.verifyToken], AuthController.updateEmail);
router.post('/auth/logout', AuthController.logout);
router.get('/auth/account', [authJwt.verifyToken], AuthController.getAccount)

// Account routes
router.get('/accounts', [authJwt.verifyToken, authJwt.isAdmin], AccountController.getAccounts);
router.get('/accounts/:id', [authJwt.verifyToken, authJwt.isAdmin], AccountController.getAccount);
router.put('/accounts', [authJwt.verifyToken, authJwt.isAdmin], AccountController.updateAccount);

// Banning
router.post('/macban/add', [authJwt.verifyToken, authJwt.isAdmin], AccountController.addMacBan);
router.post('/macban/remove', [authJwt.verifyToken, authJwt.isAdmin], AccountController.removeMacBan);
router.get('/macban', [authJwt.verifyToken, authJwt.isAdmin], AccountController.getBannedAccounts);

// Log routes
router.get('/vote/logs', [authJwt.verifyToken], LoggingController.getVoteLogs);
router.get('/logs/:type', [authJwt.verifyToken], LoggingController.getLogs);
router.get('/alllogs/:type', [authJwt.verifyToken, authJwt.isAdmin], LoggingController.getAllLogsByType);

// Character routes
router.get('/characters', CharacterController.getCharacters);
router.get('/character/:id', [authJwt.verifyToken, authJwt.isGameMaster], CharacterController.getCharacter);
router.get('/characters/:account', [authJwt.verifyToken], CharacterController.getCharactersByAccount);

// Public routes, general information
router.get('/online', CharacterController.getOnlineCount);
router.get('/peak', LoggingController.getPeakCount);
//router.get('/lord', CharacterController.getLord); // This function is unfinished. Needs test data
//router.get('/siege', CharacterController.gsWinner); // This function is unfinished. Needs test data
//router.get('/mvp', CharacterController.gsMVP); // This function is unfinished. Needs test data

// News routes
router.get('/news', NewsController.getNews);
router.get('/news/:uuid', NewsController.getArticle);
router.post('/news', [authJwt.verifyToken, authJwt.isAdmin], NewsController.addArticle);
router.put('/news', [authJwt.verifyToken, authJwt.isAdmin], NewsController.updateArticle);
router.delete('/news', [authJwt.verifyToken, authJwt.isAdmin], NewsController.deleteArticle);

// Setting routes
router.get('/setting', SettingController.getSettings);
router.get('/setting/:name', SettingController.getSetting);
router.post('/setting', [authJwt.verifyToken, authJwt.isAdmin], SettingController.addSetting);
router.put('/setting', [authJwt.verifyToken, authJwt.isAdmin], SettingController.updateSetting);
router.delete('/setting', [authJwt.verifyToken, authJwt.isAdmin], SettingController.deleteSetting);

// Donation routes
router.post('/payment/ipn', DonationController.ipn);

// Vote routes
router.post('/vote/gtop100', VoteController.gtop100);
router.post('/vote/xtremetop100', VoteController.xtremetop100);

// Store routes
router.get('/store/items', [authJwt.verifyToken], StoreController.getItems);
router.get('/store/items/:id', [authJwt.verifyToken], StoreController.getItem);
router.get('/store/categories', [authJwt.verifyToken], StoreController.getCategories);
router.get('/store/categories/:id', [authJwt.verifyToken], StoreController.getItemsByCategory);
router.post('/store/purchase', [authJwt.verifyToken], StoreController.purchaseItem);
// Store admin routes
router.post('/store/items', [authJwt.verifyToken, authJwt.isAdmin], StoreController.addItem);
router.put('/store/items', [authJwt.verifyToken, authJwt.isAdmin], StoreController.updateItem);
router.delete('/store/items', [authJwt.verifyToken, authJwt.isAdmin], StoreController.deleteItem);
router.post('/store/categories', [authJwt.verifyToken, authJwt.isAdmin], StoreController.addCategory);
router.put('/store/categories', [authJwt.verifyToken, authJwt.isAdmin], StoreController.updateCategory);
router.delete('/store/categories', [authJwt.verifyToken, authJwt.isAdmin], StoreController.deleteCategory);

router.get('/store/itemsdata', [authJwt.verifyToken, authJwt.isAdmin], StoreController.getItemData);

// Auth test routes
router.get('/test/user', [authJwt.verifyToken], (req, res) => res.status(200).json({ auth: req.authority, account: req.account }));
router.get('/test/gm', [authJwt.verifyToken, authJwt.isGameMaster], (req, res) => res.status(200).json({ auth: req.authority, account: req.account }));
router.get('/test/admin', [authJwt.verifyToken, authJwt.isAdmin], (req, res) => res.status(200).json({ auth: req.authority, account: req.account }));

export default router;