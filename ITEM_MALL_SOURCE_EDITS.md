## Editing source for instant Item Mall delivery
All credits goes to [Sedrika](https://www.elitepvpers.com/forum/flyff-pserver-guides-releases/3446616-web-shop.html). I am simply including this here in case the original thread gets removed from epvp.
The website does not use the Web Shop included in that thread, instead we use our own custom web shop. The only part that exists from Sedrika's store is the instant delivery part using sockets.

Open DbItemUpdate.cpp and search for:
```
void CDbManager::ChangeItemUpdate( CQuery* pQuery )
```

Replace the whole function with this one:
```
void CDbManager::ChangeItemUpdate( CQuery* pQuery )
{
	char szSQL[4096] = {0,};
	
	sprintf( szSQL, "ITEM_STR 'D1'" );
	
	if( FALSE == pQuery->Exec( szSQL ) )
	{
		WriteLog( szSQL );
		return;		
	}

	for( int i = 0; i < prj.m_aPropItem.GetSize(); i++ )
	{
		ItemProp* pItemProp =  prj.GetItemProp( i );
		if( pItemProp && pItemProp->dwItemKind3 != IK3_VIRTUAL && pItemProp->dwID != 0 )
		{
			CString	strItem	= pItemProp->szName;
			strItem.Replace( "'", "" );
			CString strItemCommand = pItemProp->szCommand;
			strItemCommand.Replace( "'", "" );
			sprintf( szSQL, "ITEM_STR 'I1',"
				"%d, '%s', %d, %d, %d, %d,"	// 0
				"%d, %d, %d, %d, %d, %d,"	// 1
				"%d, %d, %d, %d, %d, %d,"	// 2
				"%d, %d, %d, %d, %d, %f,"	// 3
				"%d, %d, %d, %d, %d, %d,"	// 4
				"%d, %d, %d, %d, %d, %d,"	// 5
				"%d, %d, %d, %d, %d, %d,"	// 6
				"%d, %d, %d, %d, %d, %d,"	// 7
				"%d, %d, %d, %d, %d, %d,"	// 8
				"%d, %d, %f, %d, %d, %d,"	// 9
				"%d, '%s', '%s'",			// 10

				pItemProp->dwID, strItem, pItemProp->dwPackMax, pItemProp->dwItemKind1, pItemProp->dwItemKind2, pItemProp->dwItemKind3, // 0
				pItemProp->dwItemJob, pItemProp->dwItemSex, pItemProp->dwCost, pItemProp->dwHanded, pItemProp->dwFlag, pItemProp->dwParts, // 1
				pItemProp->dwPartsub, pItemProp->dwExclusive, pItemProp->dwItemLV, pItemProp->dwItemRare, pItemProp->dwShopAble, pItemProp->bCharged, // 2
				pItemProp->dwAbilityMin, pItemProp->dwAbilityMax, pItemProp->eItemType, pItemProp->dwWeaponType, pItemProp->nAdjHitRate, pItemProp->fAttackSpeed, // 3
				pItemProp->dwAttackRange, pItemProp->nProbability, pItemProp->dwDestParam[0], pItemProp->dwDestParam[1], pItemProp->dwDestParam[2], pItemProp->nAdjParamVal[0], // 4
				pItemProp->nAdjParamVal[1], pItemProp->nAdjParamVal[2], pItemProp->dwChgParamVal[0], pItemProp->dwChgParamVal[1], pItemProp->dwChgParamVal[2], pItemProp->nDestData1[0], // 5
				pItemProp->nDestData1[1], pItemProp->nDestData1[2], pItemProp->dwReqMp, pItemProp->dwReqFp, pItemProp->dwReqDisLV, pItemProp->dwCircleTime, // 6
				pItemProp->dwSkillTime, pItemProp->dwReferStat1, pItemProp->dwReferStat2, pItemProp->dwReferTarget1, pItemProp->dwReferTarget2, pItemProp->dwReferValue1, // 7
				pItemProp->dwReferValue2, pItemProp->dwSkillType, pItemProp->nItemResistElecricity, pItemProp->nItemResistFire, pItemProp->nItemResistWind, pItemProp->nItemResistWater, // 8
				pItemProp->nItemResistEarth, pItemProp->dwExp, pItemProp->fFlightSpeed, pItemProp->dwFlightLimit, pItemProp->dwFFuelReMax, pItemProp->dwAFuelReMax, // 9
				pItemProp->dwLimitLevel1, pItemProp->szIcon, strItemCommand // 10
				);
			if( FALSE == pQuery->Exec( szSQL ) )
			{
				WriteLog( szSQL );
				return;
			}
		}	// if
	}	// for
	TRACE( "ChangeItemUpdate Complete\n" );
}
```

Open ProjectCmn.cpp and search for:
```
BOOL CProject::LoadPropItem( LPCTSTR lpszFileName, CFixedArray< ItemProp >* apObjProp )
```

Search and replace this:
```
#ifdef __CLIENT	
	_tcsncpy( prop.szIcon, scanner.token, sizeof(prop.szIcon)-1 );
	prop.szCommand[sizeof(prop.szIcon)-1] = 0;
#endif
```

with this:
```
#if defined(__CLIENT) || defined(__DBSERVER)	
	_tcsncpy( prop.szIcon, scanner.token, sizeof(prop.szIcon)-1 );
	prop.szCommand[sizeof(prop.szIcon)-1] = 0;
#endif
```

Search and replace this:
```
#ifdef __CLIENT	
	_tcsncpy( prop.szCommand, scanner.token, sizeof(prop.szCommand)-1 );
	prop.szCommand[sizeof(prop.szCommand)-1] = 0;
#endif
```

with this:
```
#if defined(__CLIENT) || defined(__DBSERVER)	
	_tcsncpy( prop.szCommand, scanner.token, sizeof(prop.szCommand)-1 );
	prop.szCommand[sizeof(prop.szCommand)-1] = 0;
#endif
```

Open ProjectCmn.h and search for:
```
#ifdef __CLIENT
    TCHAR	szIcon[64];
	TCHAR	szCommand[256];
#endif
```

and replace it with this:
```
#if defined(__CLIENT) || defined(__DBSERVER)
    TCHAR	szIcon[64];
	TCHAR	szCommand[256];
#endif
```

Open DPSrvr.cpp and search for:
```
void CDPSrvr::OnBuyingInfo( CAr & ar, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, u_long uBufSize )
```

and replace the whole function with this:
```
void CDPSrvr::OnBuyingInfo( CAr & ar, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, u_long uBufSize )
{
	BUYING_INFO2 bi2;
	ar.Read( (void*)&bi2, sizeof(BUYING_INFO2) );

	CWorld* pWorld;
	CUser* pUser	= g_UserMng.GetUser( dpidCache, dpidUser );

	/*
		100 - Save Player
		101 - Create Item
	*/
    
    if( IsValidObj( pUser )  && ( pWorld = pUser->GetWorld() ) )
    {
		if( bi2.dwParam4 != 0000000 || bi2.dwParam5 != 0000000 )
				return;
		if( bi2.dwCommand == 100 ) // Save Player
		{
			g_dpDBClient.SavePlayer( pUser, pWorld->GetID(), pUser->GetPos(), pUser->GetLayer() );
		}
		else if( bi2.dwCommand == 101 ) // Create Item
		{
			SERIALNUMBER iSerialNumber	= 0;
			CItemElem itemElem;
			itemElem.m_dwItemId        = bi2.dwParam1;
			itemElem.m_nItemNum        = (short)bi2.dwParam2;
			itemElem.m_bCharged        = TRUE;
			BYTE nId;
			bi2.dwParam3    = pUser->CreateItem( &itemElem, &nId );
			if( bi2.dwParam3 )
			{
				CItemElem* pItemElem    = pUser->m_Inventory.GetAtId( nId );
				if( pItemElem )
				{
					iSerialNumber    = pItemElem->GetSerialNumber();
					pItemElem->m_bCharged    = TRUE;
					if( bi2.dwTargetId > 0 )
						pUser->AddDefinedText( TID_EVE_REAPITEM, "\"%s\"", itemElem.GetName() );
				}
			}
			else
			{
				LogItemInfo aLogItem;
				aLogItem.Action = "S";
				aLogItem.SendName = pUser->GetName();
				aLogItem.WorldId = pUser->GetWorld()->GetID();
				aLogItem.Gold = aLogItem.Gold2 = pUser->GetGold();

				g_dpDBClient.SendQueryPostMail( pUser->m_idPlayer, 0, itemElem, 0, "WebShop", "" );
				aLogItem.RecvName = "HOMEPAGE_SHOP";
				g_DPSrvr.OnLogItem( aLogItem, &itemElem, itemElem.m_nItemNum );
			}
			g_dpDBClient.SendBuyingInfo( &bi2, iSerialNumber );
			g_dpDBClient.SavePlayer( pUser, pWorld->GetID(), pUser->GetPos(), pUser->GetLayer() );
		}
	}
}
```

Open buyinginfo.h and replace the while file with this:
```
#ifndef __CHARGEDITEM_H__
#define	__CHARGEDITEM_H__

typedef struct _BUYING_INFO
{
	DWORD	dwServerIndex;
	DWORD	dwPlayerId;
	DWORD	dwTargetId;
	DWORD	dwCommand;
	DWORD	dwParam1;
	DWORD	dwParam2;
	DWORD	dwParam3;
	DWORD	dwParam4;
	DWORD	dwParam5;

	_BUYING_INFO()
	{
		dwServerIndex	= 0;
		dwPlayerId		= 0;
		dwTargetId		= 0;
		dwCommand		= 0;
		dwParam1		= 0;
		dwParam2		= 0;
		dwParam3		= 0;
		dwParam4		= 0;
		dwParam5		= 0;
	}
}
BUYING_INFO, *PBUYING_INFO;

typedef struct _BUYING_INFO2: public _BUYING_INFO
{
	DWORD	dpid;
	DWORD	dwKey;
	_BUYING_INFO2() : _BUYING_INFO()
		{
			dpid	= 0xFFFFFFFF;
			dwKey	= 0;
		}
}
BUYING_INFO2, *PBUYING_INFO2;

typedef struct _BUYING_INFO3: public _BUYING_INFO2
{
	DWORD	dwTickCount;
	_BUYING_INFO3() : _BUYING_INFO2()
		{
//			dwTickCount		= GetTickCount();
		}
}
BUYING_INFO3, *PBUYING_INFO3;

#endif	// __CHARGEDITEM_H__
```

Open dpadbill.h and search for:
```
void	Process( void )
```

and replace the whole function with this:
```
void	Process( void )
{
	DWORD dwTickCount	= GetTickCount();
//	static DWORD adwKey[1024];
	vector<DWORD>	adwKey;
			
	int nSize	= 0;
			
	m_AddRemoveLock.Enter();
	for( map<DWORD, PBUYING_INFO3>::iterator i = m_mapPbi3.begin(); i != m_mapPbi3.end(); ++i )
	{
		PBUYING_INFO3 pbi3	= i->second;
		if( dwTickCount - pbi3->dwTickCount > SEC( 3 ) )
		{
			CDPAdbill::GetInstance()->Send( pbi3, sizeof(BUYING_INFO), pbi3->dpid );
//			adwKey[nSize++]	= pbi3->dwKey;
			adwKey.push_back( pbi3->dwKey );
		}
	}
			
//	Remove
//	for( int j = 0; j < nSize; j++ )
	for( DWORD j = 0; j < adwKey.size(); j++ )
	{
//		PBUYING_INFO3 ptr	= CBuyingInfoMng::GetInstance()->Get( adwKey[j] );
		PBUYING_INFO3 ptr	= CBuyingInfoMng::GetInstance()->Get( adwKey[j] );
		if( ptr )
		{
			BUYING_INFO2	bi2;
			DWORD sn = 0;

			bi2.dwServerIndex	= ptr->dwServerIndex;
			bi2.dwPlayerId		= ptr->dwPlayerId;
			bi2.dwTargetId		= ptr->dwTargetId;
			bi2.dwCommand		= ptr->dwCommand;
			bi2.dwParam1		= ptr->dwParam1;
			bi2.dwParam2		= ptr->dwParam2;
			bi2.dwParam3		= ptr->dwParam3;
			bi2.dwParam4		= ptr->dwParam4;
			bi2.dwParam5		= ptr->dwParam5;

			CAr ar;
			ar.Write( (void*)&bi2, sizeof(BUYING_INFO2) );
			ar << sn;

			int nBufSize;
			LPBYTE lpData	= ar.GetBuffer( &nBufSize );
			LPDB_OVERLAPPED_PLUS lpDbOverlappedPlus		= g_DbManager.m_pDbIOData->Alloc();
			memcpy( lpDbOverlappedPlus->lpBuf, lpData, nBufSize );
			lpDbOverlappedPlus->uBufSize	= nBufSize;
			lpDbOverlappedPlus->nQueryMode	= LOG_SM_ITEM;
			PostQueuedCompletionStatus( g_DbManager.m_hDbCompletionPort, 1, NULL, &lpDbOverlappedPlus->Overlapped );
		}
		Remove( adwKey[j] );
		SAFE_DELETE( ptr );
	}

	m_AddRemoveLock.Leave();

	adwKey.clear();
}
```

Open DatabaseServer.ini and add this if it's not already there:
```
ITEMUPDATE

DSN_NAME_ITEMUPDATE    "item" 
DB_ADMIN_ID_ITEMUPDATE     "item"
DB_PWD_ITEMUPDATE	""
```

Create a new entry with the following settings:
Name: item
Default Database: ITEM_DBF

As far as I know, none of what is below now is required since it affects only the in-game web box. Unfortunately, the Item Mall does not support the in-game browser so implementing this is not necessary unless you manage to change the in-game browser to use a browser that supports modern JavaScript.

Open WebBox.cpp and search for:
```
void CWebBox::Refresh_Web()
```

and replace the whole function with this:
```
void CWebBox::Refresh_Web()
{
	char address[512], postdata[WEB_STR_LEN], header[WEB_STR_LEN];
	ZeroMemory( address, 512 );
	wsprintf( address, GetAddress() );
	char out[260]	= { 0, };
	char in[260]	= { 0, };
	CString hash = "gaajgussfdbfhjq";
	sprintf( in, "%s%d%d%s", m_szUser, m_nPlayer, m_nServer, hash );
	::md5( out, in );
	wsprintf( postdata, WEB_POSTDATA, m_szUser, m_nPlayer, m_nServer, out, g_Neuz.m_szPassword );
	wsprintf( header, WEB_HEADER, lstrlen( postdata ) );
	ChangeWebAddress( address, postdata, header );
}
```

Search for:
```
bool CWebBox::Process(HWND hWnd,HINSTANCE hInstance, char* szUser, u_long nPlayer, DWORD nServer, int nLevel, int nJob, int nSex, const char* szName )
```

and replace the whole function with this:
```
bool CWebBox::Process(HWND hWnd,HINSTANCE hInstance, char* szUser, u_long nPlayer, DWORD nServer, int nLevel, int nJob, int nSex, const char* szName )
{
	char address[512], postdata[WEB_STR_LEN], header[WEB_STR_LEN];
	ZeroMemory( address, 512 );
	ZeroMemory( postdata, WEB_STR_LEN );
	ZeroMemory( header, WEB_STR_LEN );

	if( m_bStart && m_bStartWeb )
	{
		lstrcpy( m_szUser, szUser );
		m_nPlayer	= nPlayer;
		m_nServer	= nServer;
		m_nLevel	= nLevel;
		m_nJob	= nJob;
		m_nSex	= nSex;
		lstrcpy( m_szName, szName );

		D3DDEVICE->SetDialogBoxMode( TRUE );
		Start_WebBox( hWnd, hInstance, WEB_DEFAULT_X, WEB_DEFAULT_Y, NULL );

		wsprintf( address, GetAddress() );
		
		char out[260]	= { 0, };
		char in[260]	= { 0, };
		CString hash = "gjgusdbfjq";
		sprintf( in, "%s%d%d%s", m_szUser, m_nPlayer, m_nServer, hash );
			
		::md5( out, in );
		wsprintf( postdata, WEB_POSTDATA, m_szUser, m_nPlayer, m_nServer, out, g_Neuz.m_szPassword );
		
		wsprintf( header, WEB_HEADER, lstrlen( postdata ) );
		ChangeWebAddress( address, postdata, header );
		Show( TRUE );
		m_bStart	= false;
		m_bEnd	= false;
		return true;
	}
	else if( m_bEnd )
	{
		End_WebBox();
		m_bEnd	= false;
		m_bStart	= false;
		m_bStartWeb	= false;
		return false;
	}
	else if( m_bStartWeb )
	{
		if( GetAsyncKeyState( VK_F5 ) )
			Refresh_Web();
	}

	return false;
}
```

Open dpdbsrvr.cpp and search for:
```
void CDPDBSrvr::OnBuyingInfo( CAr & ar, DPID dpid, LPBYTE lpBuf, u_long uBufSize )
```

and replace the whole function with this:
```
void CDPDBSrvr::OnBuyingInfo( CAr & ar, DPID dpid, LPBYTE lpBuf, u_long uBufSize )
{
	BUYING_INFO2	bi2;
	SERIALNUMBER iSerialNumber;
	ar.Read( (void*)&bi2, sizeof(BUYING_INFO2) );
	ar >> iSerialNumber;

	CBuyingInfoMng::GetInstance()->m_AddRemoveLock.Enter();
	PBUYING_INFO3 pbi3	= CBuyingInfoMng::GetInstance()->Get( bi2.dwKey );
	if( pbi3 )
	{
		//static char lpOutputString[260]	= { 0, };
		//sprintf( lpOutputString, "ACCOUNTSERVER.EXE\t// Recv from trans\t// dwRetVal = %d", bi2.dwRetVal );
		//OutputDebugString( lpOutputString );

		CDPAdbill::GetInstance()->Send( &bi2, sizeof(BUYING_INFO), bi2.dpid );
		CBuyingInfoMng::GetInstance()->Remove( bi2.dwKey );
		safe_delete( pbi3 );
		/*
		bi2.szBxaid		// 구매 번호
		bi2.dwItemId	// 아이템 인덱스
		bi2.dwItemNum	// 아이템 개수
		bi2.dwServerIndex	// 서버 인덱스
		bi2.dwPlayerId		// 플레이어 식별자
		bi2.dwRetVal	// 지급 성공 여부, 성공 : 1, 실패 : 0
		iSerialNumber		// 시리얼 번호
		*/

		LPDB_OVERLAPPED_PLUS lpDbOverlappedPlus		= g_DbManager.m_pDbIOData->Alloc();
		memcpy( lpDbOverlappedPlus->lpBuf, (LPBYTE)lpBuf + sizeof(DWORD), uBufSize - sizeof(DWORD) );
		lpDbOverlappedPlus->uBufSize	= uBufSize - sizeof(DWORD);
		lpDbOverlappedPlus->nQueryMode	= LOG_SM_ITEM;
		PostQueuedCompletionStatus( g_DbManager.m_hDbCompletionPort, 1, NULL, &lpDbOverlappedPlus->Overlapped );
	}
	else
	{
		// 치명적 오류 : 어카운트 서버에는 지급 명령에 대한 정보가 없다.
	}
	CBuyingInfoMng::GetInstance()->m_AddRemoveLock.Leave();
	TRACE( "RECV PACKETTYPE_BUYING_INFO FROM TRANS\n" );
}
```

Open DBManager.cpp and search for:
```
void CDbManager::LogSMItem( CQuery & qryLog, LPDB_OVERLAPPED_PLUS lpDbOverlappedPlus )
```

and replace the whole function with:
```
void CDbManager::LogSMItem( CQuery & qryLog, LPDB_OVERLAPPED_PLUS lpDbOverlappedPlus )
{
	return;
}
```

Open dpadbill.cpp and search for:
```
void CDPAdbill::UserMessageHandler( LPDPMSG_GENERIC lpMsg, DWORD dwMsgSize, DPID idFrom )
```

and replace the whole function with this:
```
void CDPAdbill::UserMessageHandler( LPDPMSG_GENERIC lpMsg, DWORD dwMsgSize, DPID idFrom )
{
	PBUYING_INFO3 pbi3	= new BUYING_INFO3;
	memcpy( (void*)pbi3, lpMsg, sizeof(BUYING_INFO) );
	pbi3->dpid	= idFrom;
	pbi3->dwKey	= m_dwKey++;
	pbi3->dwTickCount	= GetTickCount();

	CBuyingInfoMng::GetInstance()->Add( pbi3 );
	g_dpDbSrvr.SendBuyingInfo( (PBUYING_INFO2)pbi3 );
}
```