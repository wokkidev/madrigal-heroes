USE [master]
GO

/****** Object:  Database [ITEM_DBF]    Script Date: 06.10.2014 15:14:52 ******/
CREATE DATABASE [ITEM_DBF]

ALTER DATABASE [ITEM_DBF] SET COMPATIBILITY_LEVEL = 110
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ITEM_DBF].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [ITEM_DBF] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [ITEM_DBF] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [ITEM_DBF] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [ITEM_DBF] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [ITEM_DBF] SET ARITHABORT OFF 
GO

ALTER DATABASE [ITEM_DBF] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [ITEM_DBF] SET AUTO_CREATE_STATISTICS ON 
GO

ALTER DATABASE [ITEM_DBF] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [ITEM_DBF] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [ITEM_DBF] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [ITEM_DBF] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [ITEM_DBF] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [ITEM_DBF] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [ITEM_DBF] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [ITEM_DBF] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [ITEM_DBF] SET  DISABLE_BROKER 
GO

ALTER DATABASE [ITEM_DBF] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [ITEM_DBF] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [ITEM_DBF] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [ITEM_DBF] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [ITEM_DBF] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [ITEM_DBF] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [ITEM_DBF] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [ITEM_DBF] SET RECOVERY FULL 
GO

ALTER DATABASE [ITEM_DBF] SET  MULTI_USER 
GO

ALTER DATABASE [ITEM_DBF] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [ITEM_DBF] SET DB_CHAINING OFF 
GO

ALTER DATABASE [ITEM_DBF] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [ITEM_DBF] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO

ALTER DATABASE [ITEM_DBF] SET  READ_WRITE 
GO

USE [ITEM_DBF]
GO

/****** Object:  Table [dbo].[ITEM_TBL]    Script Date: 06.10.2014 15:15:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ITEM_TBL](
	[Index] [int] NULL,
	[szName] [nvarchar](255) NULL,
	[dwPackMax] [int] NULL,
	[dwItemKind1] [int] NULL,
	[dwItemKind2] [int] NULL,
	[dwItemKind3] [int] NULL,
	[dwItemJob] [int] NULL,
	[dwItemSex] [int] NULL,
	[dwCost] [int] NULL,
	[dwHanded] [int] NULL,
	[dwFlag] [int] NULL,
	[dwParts] [int] NULL,
	[dwPartsub] [int] NULL,
	[dwExclusive] [int] NULL,
	[dwItemLV] [int] NULL,
	[dwItemRare] [int] NULL,
	[dwShopAble] [int] NULL,
	[bCharged] [int] NULL,
	[dwAbilityMin] [int] NULL,
	[dwAbilityMax] [int] NULL,
	[eItemType] [int] NULL,
	[dwWeaponType] [int] NULL,
	[nAdjHitRate] [int] NULL,
	[fAttackSpeed] [real] NULL,
	[dwAttackRange] [int] NULL,
	[nProbability] [int] NULL,
	[dwDestParam1] [int] NULL,
	[dwDestParam2] [int] NULL,
	[dwDestParam3] [int] NULL,
	[nAdjParamVal1] [int] NULL,
	[nAdjParamVal2] [int] NULL,
	[nAdjParamVal3] [int] NULL,
	[dwChgParamVal1] [int] NULL,
	[dwChgParamVal2] [int] NULL,
	[dwChgParamVal3] [int] NULL,
	[nDestData11] [int] NULL,
	[nDestData12] [int] NULL,
	[nDestData13] [int] NULL,
	[dwReqMp] [int] NULL,
	[dwReqFp] [int] NULL,
	[dwReqDisLV] [int] NULL,
	[dwCircleTime] [int] NULL,
	[dwSkillTime] [int] NULL,
	[dwReferStat1] [int] NULL,
	[dwReferStat2] [int] NULL,
	[dwReferTarget1] [int] NULL,
	[dwReferTarget2] [int] NULL,
	[dwReferValue1] [int] NULL,
	[dwReferValue2] [int] NULL,
	[dwSkillType] [int] NULL,
	[nItemResistElecricity] [int] NULL,
	[nItemResistFire] [int] NULL,
	[nItemResistWind] [int] NULL,
	[nItemResistWater] [int] NULL,
	[nItemResistEarth] [int] NULL,
	[dwExp] [int] NULL,
	[fFlightSpeed] [real] NULL,
	[dwFlightLimit] [int] NULL,
	[dwFFuelReMax] [int] NULL,
	[dwAFuelReMax] [int] NULL,
	[dwLimitLevel1] [int] NULL,
	[szIcon] [varchar](255) NULL,
	[szCommand] [nvarchar](255) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

USE [ITEM_DBF]
GO

/****** Object:  StoredProcedure [dbo].[ITEM_STR]    Script Date: 06.10.2014 15:15:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE    PROC [dbo].[ITEM_STR]
	@iGu CHAR(2) = 'S1',
	@Index INT = 0,
	@szName NVARCHAR(255) = '',
	@dwPackMax INT = 0,
	@dwItemKind1 INT = 0,
	@dwItemKind2 INT = 0,
	@dwItemKind3 INT = 0,
	@dwItemJob INT = 0,
	@dwItemSex INT = 0,
	@dwCost INT = 0,
	@dwHanded INT = 0,
	@dwFlag INT = 0,
	@dwParts INT = 0,
	@dwPartsub INT = 0,
	@dwExclusive INT = 0,
	@dwItemLV INT = 0,
	@dwItemRare INT = 0,
	@dwShopAble INT = 0,
	@bCharged INT = 0,
	@dwAbilityMin INT = 0,
	@dwAbilityMax INT = 0,
	@eItemType INT = 0,
	@dwWeaponType INT = 0,
	@nAdjHitRate INT = 0,
	@fAttackSpeed REAL = 0.0,
	@dwAttackRange INT = 0,
	@nProbability INT = 0,
	@dwDestParam1 INT = 0,
	@dwDestParam2 INT = 0,
	@dwDestParam3 INT = 0,
	@nAdjParamVal1 INT = 0,
	@nAdjParamVal2 INT = 0,
	@nAdjParamVal3 INT = 0,
	@dwChgParamVal1 INT = 0,
	@dwChgParamVal2 INT = 0,
	@dwChgParamVal3 INT = 0,
	@nDestData11 INT = 0,
	@nDestData12 INT = 0,
	@nDestData13 INT = 0,
	@dwReqMp INT = 0,
	@dwReqFp INT = 0,
	@dwReqDisLV INT = 0,
	@dwCircleTime INT = 0,
	@dwSkillTime INT = 0,
	@dwReferStat1 INT = 0,
	@dwReferStat2 INT = 0,
	@dwReferTarget1 INT = 0,
	@dwReferTarget2 INT = 0,
	@dwReferValue1 INT = 0,
	@dwReferValue2 INT = 0,
	@dwSkillType INT = 0,
	@nItemResistElecricity INT = 0,
	@nItemResistFire INT = 0,
	@nItemResistWind INT = 0,
	@nItemResistWater INT = 0,
	@nItemResistEarth INT = 0,
	@dwExp INT = 0,
	@fFlightSpeed REAL = 0.0,
	@dwFlightLimit INT = 0,
	@dwFFuelReMax INT = 0,
	@dwAFuelReMax INT = 0,
	@dwLimitLevel1 INT = 0,
	@szIcon VARCHAR(255) = '',
	@szCommand NVARCHAR(255) = ''
	
AS
set nocount on
IF @iGu = 'D1'  -- ??? ??
	BEGIN
				TRUNCATE TABLE ITEM_TBL
		RETURN
	END
ELSE
IF @iGu = 'I1' -- ??? Insert
	BEGIN
				INSERT ITEM_TBL
							(
								[Index],
								[szName],
								[dwPackMax],
								[dwItemKind1],
								[dwItemKind2],
								[dwItemKind3],
								[dwItemJob],
								[dwItemSex],
								[dwCost],
								[dwHanded],
								[dwFlag],
								[dwParts],
								[dwPartsub],
								[dwExclusive],
								[dwItemLV],
								[dwItemRare],
								[dwShopAble],
								[bCharged],
								[dwAbilityMin],
								[dwAbilityMax],
								[eItemType],
								[dwWeaponType],
								[nAdjHitRate],
								[fAttackSpeed],
								[dwAttackRange],
								[nProbability],
								[dwDestParam1],
								[dwDestParam2],
								[dwDestParam3],
								[nAdjParamVal1],
								[nAdjParamVal2],
								[nAdjParamVal3],
								[dwChgParamVal1],
								[dwChgParamVal2],
								[dwChgParamVal3],
								[nDestData11],
								[nDestData12],
								[nDestData13],
								[dwReqMp],
								[dwReqFp],
								[dwReqDisLV],
								[dwCircleTime],
								[dwSkillTime],
								[dwReferStat1],
								[dwReferStat2],
								[dwReferTarget1],
								[dwReferTarget2],
								[dwReferValue1],
								[dwReferValue2],
								[dwSkillType],
								[nItemResistElecricity],
								[nItemResistFire],
								[nItemResistWind],
								[nItemResistWater],
								[nItemResistEarth],
								[dwExp],
								[fFlightSpeed],
								[dwFlightLimit],
								[dwFFuelReMax],
								[dwAFuelReMax],
								[dwLimitLevel1],
								[szIcon],
								[szCommand]
							)
				VALUES
							(
								@Index,
								@szName,
								@dwPackMax,
								@dwItemKind1,
								@dwItemKind2,
								@dwItemKind3,
								@dwItemJob,
								@dwItemSex,
								@dwCost,
								@dwHanded,
								@dwFlag,
								@dwParts,
								@dwPartsub,
								@dwExclusive,
								@dwItemLV,
								@dwItemRare,
								@dwShopAble,
								@bCharged,
								@dwAbilityMin,
								@dwAbilityMax,
								@eItemType,
								@dwWeaponType,
								@nAdjHitRate,
								@fAttackSpeed,
								@dwAttackRange,
								@nProbability,
								@dwDestParam1,
								@dwDestParam2,
								@dwDestParam3,
								@nAdjParamVal1,
								@nAdjParamVal2,
								@nAdjParamVal3,
								@dwChgParamVal1,
								@dwChgParamVal2,
								@dwChgParamVal3,
								@nDestData11,
								@nDestData12,
								@nDestData13,
								@dwReqMp,
								@dwReqFp,
								@dwReqDisLV,
								@dwCircleTime,
								@dwSkillTime,
								@dwReferStat1,
								@dwReferStat2,
								@dwReferTarget1,
								@dwReferTarget2,
								@dwReferValue1,
								@dwReferValue2,
								@dwSkillType,
								@nItemResistElecricity,
								@nItemResistFire,
								@nItemResistWind,
								@nItemResistWater,
								@nItemResistEarth,
								@dwExp,
								@fFlightSpeed,
								@dwFlightLimit,
								@dwFFuelReMax,
								@dwAFuelReMax,
								@dwLimitLevel1,
								@szIcon,
								@szCommand
							)
		RETURN
	END
ELSE
IF @iGu = 'D2' -- SKILL_TBL Data ?? ???
	BEGIN
				TRUNCATE TABLE SKILL_TBL
		RETURN
	END
ELSE

IF @iGu = 'I2' -- SKILL_TBL Data Insert
	BEGIN
				INSERT SKILL_TBL
							(
								[Index], 
								[szName],
								[job]
							)
				VALUES
							(
								@Index,
								@szName,
								0
							)
		RETURN
	END
ELSE
IF @iGu = 'D3' -- MONSTER_TBL Data ?? ???
	BEGIN
				TRUNCATE TABLE MONSTER_TBL
		RETURN
	END
ELSE
IF @iGu = 'I3' -- MONSTER_TBL Data Insert
	BEGIN
				INSERT MONSTER_TBL
							(
								[Index], 
								[szName],
								[Level]
							)
				VALUES
							(
								@Index,
								@szName,
								0
							)
		RETURN
	END
ELSE
IF @iGu = 'D4' -- QUEST_TBL Data ?? ???
	BEGIN
				TRUNCATE TABLE QUEST_TBL
		RETURN
	END
ELSE
IF @iGu = 'I4' -- QUEST_TBL Data Insert
	BEGIN
				INSERT QUEST_TBL
							(
								[Index], 
								[szName]
							)
				VALUES
							(
								@Index,
								@szName
							)
		RETURN
	END
ELSE
IF @iGu = 'D5' -- JOB_TBL Data ?? ???
	BEGIN
				TRUNCATE TABLE JOB_TBL
		RETURN
	END
ELSE
IF @iGu = 'I5' -- JOB_TBL Data Insert
	BEGIN
				INSERT JOB_TBL
							(
								[Index], 
								[szName]
							)
				VALUES
							(
								@Index,
								@szName
							)
		RETURN
	END
set nocount off



GO