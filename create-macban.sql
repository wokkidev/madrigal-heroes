GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MAC_BAN_TBL](
    [id] [int] IDENTITY(1,1) NOT NULL,
    [macaddr] [varchar](40) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[MAC_BAN_ACC]
    -- Add the parameters for the stored procedure here
    @iAccount varchar(64)
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

    -- Insert statements for procedure here
    declare @lastmac as varchar(38)
    select @lastmac = (SELECT lastmac FROM ACCOUNT_TBL_DETAIL WHERE account = @iAccount)
    INSERT INTO MAC_BAN_TBL (macaddr)
                VALUES        (@lastmac)
END
GO